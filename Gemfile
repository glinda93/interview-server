source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.7.3'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails', branch: 'main'
gem 'rails', '~> 6.1.3', '>= 6.1.3.2'
# Use mysql as the database for Active Record
gem 'mysql2', '~> 0.5'
# Use Puma as the app server
gem 'puma', '~> 5.0'
# Use SCSS for stylesheets
gem 'sass-rails', '>= 6'
# Transpile app-like JavaScript. Read more: https://github.com/rails/webpacker
gem 'webpacker', '~> 5.0'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.7'
# Use Redis adapter to run Action Cable in production
gem 'redis', '~> 4.0'
# Use Active Model has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Active Storage variant
# gem 'image_processing', '~> 1.2'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.4', require: false
# Flexible authentication solution for Rails with Warden.
gem 'devise'
# JWT token authentication with devise and rails
gem 'devise-jwt'
# Rack Middleware for handling Cross-Origin Resource Sharing (CORS), which makes cross-origin AJAX possible.
gem 'rack-cors'
# Ruby One Time Password library
gem 'rotp'
# Active Storage Validations
gem 'active_storage_validations'
# A Scope & Engine based, clean, powerful, customizable and sophisticated paginator for Ruby webapps
gem 'kaminari'
# A Ruby gem for communicating with the Twilio API and generating TwiML
gem 'twilio-ruby'
# Simple gem for implementing texter classes which allow sending SMS messages
gem 'textris', git: 'git@github.com:glinda93/textris.git', branch: 'rails6'
# Rack middleware for blocking & throttling
gem 'rack-attack'
# Official repository for the aws-sdk-rails gem, which integrates the AWS SDK for Ruby with Ruby on Rails.
gem 'aws-sdk-rails'
# S3 Service (Amazon S3 and S3-compatible APIs)
gem 'aws-sdk-s3', require: false
# Ruby library for the Stripe API.
gem 'stripe'
# Simple, efficient background processing for Ruby
gem 'sidekiq'
# Scheduler / Cron for Sidekiq jobs
gem "sidekiq-cron", "~> 1.1"
# Ensure uniqueness of your Sidekiq jobs
gem 'sidekiq-unique-jobs'
# Simply builds and verifies OAuth headers
gem "simple_oauth"
# The push notification service for Ruby
gem "rpush"
# Shim to load environment variables from .env into ENV in development.
gem "dotenv-rails"
# help to kill N+1 queries and unused eager loading
gem "bullet"
# High-level image processing wrapper for libvips and ImageMagick/GraphicsMagick
gem "image_processing"
# Complete Ruby gem for Sign in with Apple. Actively maintained by rootstrap.com
gem 'apple_auth'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  # A Ruby static code analyzer and formatter, based on the community Ruby style guide.
  gem 'rubocop', require: false
  # An extension of RuboCop focused on code performance checks.
  gem 'rubocop-performance', require: false
  # A RuboCop extension focused on enforcing Rails best practices and coding conventions.
  gem 'rubocop-rails', require: false
  # Code style checking for RSpec files
  gem 'rubocop-rspec', require: false
  # RSpec for Rails 5+
  gem 'rspec-rails', '~> 5.0.0'
  # Factory Bot Rails
  gem 'factory_bot_rails'
  # A library for generating fake data such as names, addresses, and phone numbers.
  gem 'faker'
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 4.1.0'
  # Display performance information such as SQL time and flame graphs for each request in your browser.
  # Can be configured to work on production as well see: https://github.com/MiniProfiler/rack-mini-profiler/blob/master/README.md
  gem 'rack-mini-profiler', '~> 2.0'
  gem 'listen', '~> 3.3'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '>= 3.26'
  gem 'selenium-webdriver'
  # Easy installation and use of web drivers to run system tests with browsers
  gem 'webdrivers'
  # Strategies for cleaning databases in Ruby. Can be used to ensure a clean state for testing.
  gem 'database_cleaner-active_record'
  # Create temporary table-backed ActiveRecord models for use in tests
  gem 'temping'
  # A mocking library for testing stripe ruby
  gem 'stripe-ruby-mock', '~> 3.1.0.rc2', require: 'stripe_mock', git: 'git@github.com:glinda93/stripe-ruby-mock.git'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
