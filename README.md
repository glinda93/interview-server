[![Ruby Style Guide](https://img.shields.io/badge/code_style-rubocop-brightgreen.svg)](https://github.com/rubocop/rubocop)

# interview-server

Rails backend for [interview app](https://github.com/glinda93/interview-ui)

## Installation

1. Clone this repository

```console
$ git clone git@github.com:glinda93/interview-server.git
```

2. Copy `.env` file

```console
$ cp .env.example .env
```

3. Bundle install

```console
$ bundle
```

5. Setup database

```console
$ rake db:setup
```

6. Start server

```console
$ rails server
```

7. Start sidekiq

```console
$ bundle exec sidekiq -q default -q mailers
```

## Test

1. Setup database

```
$ rake db:drop RAILS_ENV=test
$ rake db:create RAILS_ENV=test
$ rake db:migrate RAILS_ENV=test
$ rake db:seed RAILS_ENV=test
```

2. Run `rspec`:

```
$ rspec
```
