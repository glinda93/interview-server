class AppearanceChannel < ApplicationCable::Channel
  EVENTS = {
    offer_received: 'OFFER_RECEIVED',
    offer_response: 'OFFER_RESPONSE',
    offer_deleted: 'OFFER_DELETED',
    offer_not_responded_ids: 'OFFER_NOT_RESPONDED_IDS',
    device_registered: 'DEVICE_REGISTERED',
    unread_message: 'UNREAD_MESSAGE',
    avg_rating_updated: 'AVG_RATING_UPDATED'
  }.freeze

  def subscribed
    current_user.appear
    stream_for current_user
    check_unread_messages
    check_offers
  end

  def unsubscribed
    current_user.disappear
  end

  def appear(data)
    current_user.appear(on: data['appearing_on'] || nil)
  end

  def register_device(data)
    raise ApplicationError, 'invalid param' unless data && data['device']

    device = Device.register_device user: current_user, device_info: data['device']
    broadcast_to current_user, {
      event: EVENTS[:device_registered],
      payload: device
    }
  end

  def check_unread_messages
    unread_message_count = ChatMessage.unread_messages(current_user).count

    broadcast_to current_user, {
      event: EVENTS[:unread_message],
      payload: unread_message_count
    }
  end

  def check_offers
    offers_received_ids = Offer.where(offerable_to: current_user)
                               .not_responded
                               .not_expired
                               .pluck(:id)

    broadcast_to current_user, {
      event: EVENTS[:offer_not_responded_ids],
      payload: offers_received_ids
    }
  end

  delegate :away, to: :current_user
end
