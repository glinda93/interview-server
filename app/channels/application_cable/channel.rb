module ApplicationCable
  class Channel < ActionCable::Channel::Base
    def self.serialize_broadcasting(object)
      if object.respond_to? :stream_name
        begin
          return object.stream_name
        rescue ArgumentError
          return super(object)
        end
      end
      super(object)
    end
  end
end
