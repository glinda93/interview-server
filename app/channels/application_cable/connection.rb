module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :current_user

    def connect
      self.current_user = find_verified_user
    end

    private

    def find_verified_user
      begin
        token = request.params['token']
        scope = request.params['scope'].to_sym
        verified_user = Warden::JWTAuth::UserDecoder.new.call(token, scope, nil)
      rescue StandardError
        reject_unauthorized_connection
      end
      reject_unauthorized_connection unless verified_user
      verified_user
    end
  end
end
