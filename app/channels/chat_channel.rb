class ChatChannel < ApplicationCable::Channel
  EVENTS = {
    new_message: 'NEW_MESSAGE',
    message_updated: 'MESSAGE_UPDATED',
    message_deleted: 'MESSAGE_DELETED',
    message_read_count: 'MESSAGE_READ_COUNT'
  }.freeze

  attr_accessor :chat_room, :chat_room_info

  def subscribed
    begin
      find_current_chat_room!
    rescue ChannelError
      return reject
    end

    current_user.appear on: chat_room
    stream_for chat_room
  end

  def unsubscribed
    current_user.disappear on: chat_room if chat_room.present?
  end

  def read_all
    find_chat_room_info
    unread_message_count = chat_room_info.unread_message_count
    chat_room_info.last_read_at = Time.now.utc
    chat_room_info.save!
    AppearanceChannel.broadcast_to current_user, {
      event: EVENTS[:message_read_count],
      payload: unread_message_count
    }
  end

  def create_message(data)
    ChatMessage.create(
      chat_room: chat_room,
      authorable: current_user,
      message: data['message']
    )
  end

  def delete_message(data)
    id = data['id']
    chat_message = ChatMessage.find_by(id: id, authorable: current_user)
    chat_message&.destroy!
  end

  protected

  def find_current_chat_room!
    chat_room = ChatRoom.find_by id: params[:chat_room_id]
    raise ChannelError, "ChatRoom not found" if chat_room.blank?
    raise ChannelError, "User not allowed in ChatRoom" unless chat_room.chattables.include? current_user

    self.chat_room = chat_room
  end

  def find_chat_room_info
    self.chat_room_info = ChatRoomInfo.find_or_create_by chat_room: @chat_room, chattable: current_user
  end
end
