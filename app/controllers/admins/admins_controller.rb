module Admins
  class AdminsController < ApplicationController
    include AuthenticateUser
    include AdminsHelper

    def me
      render json: current_admin
    end

    def verify_password
      return render json: { success: false, message: I18n.t('message.admin.password_incorrect') } unless current_admin.valid_password? params['password']

      current_admin.regenerate_profile_change_token
      render json: {
        success: true,
        profile_change_token: current_admin.regenerate_profile_change_token,
        expires_in: 23.hours.after.to_i
      }
    end

    def change_email
      unless current_admin.profile_change_token == change_email_params[:profile_change_token]
        return render json: {
          success: false,
          message: I18n.t('message.admin.reset_password_token_missing')
        }
      end
      if Admin.where(email: change_email_params[:unconfirmed_email]).count.positive?
        return render json: {
          success: false,
          message: I18n.t('message.admin.email_already_in_use')
        }
      end

      current_admin.unconfirmed_email = change_email_params[:unconfirmed_email]
      current_admin.save!

      current_admin.generate_otp
      AdminMailer.with(model: current_admin).change_email_mail.deliver_later
      render json: { success: true }
    end

    def change_password
      unless current_admin.profile_change_token == change_password_params[:profile_change_token]
        return render json: {
          success: false,
          message: I18n.t('message.admin.reset_password_token_missing')
        }
      end

      current_admin.password = change_password_params[:password]
      current_admin.save!
      render json: { success: true }
    end

    def verify_unconfirmed_email
      return render json: { success: false, message: I18n.t('message.admin.otp_incorrect') } unless current_admin.otp == params['otp']
      return render json: { success: false, message: I18n.t('message.admin.no_unconfirmed_email') } if current_admin.unconfirmed_email.blank?

      current_admin.email = current_admin.unconfirmed_email
      current_admin.unconfirmed_email = nil
      current_admin.save!
      current_admin.invalidate_otp

      render json: { success: true, admin: current_admin }
    end
  end
end
