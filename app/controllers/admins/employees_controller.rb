module Admins
  class EmployeesController < ApplicationController
    include AuthenticateUser
    around_action :skip_bullet, if: -> { defined?(Bullet) }

    def index
      respond_to do |format|
        format.json do
          render json: paginate(Employee.where.not(profile_completed_at: nil).where(deleted_at: nil).includes(:skills).order(created_at: :desc, id: :desc).all)
        end
        format.csv do
          send_data Employee.to_csv.encode(Encoding::SJIS),
                    type: 'text/csv; charset=shift_jis; header=present',
                    filename: Employee.csv_filename,
                    disposition: 'attachment'
        end
      end
    end
  end
end
