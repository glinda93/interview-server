module Admins
  class EmployersController < ApplicationController
    include AuthenticateUser

    def index
      respond_to do |format|
        format.json do
          render json: paginate(Employer.where.not(confirmed_at: nil).order(created_at: :desc, id: :desc).all)
        end
        format.csv do
          send_data Employer.to_csv.encode(Encoding::SJIS),
                    type: 'text/csv; charset=shift_jis; header=present',
                    filename: Employer.csv_filename,
                    disposition: 'attachment'
        end
      end
    end
  end
end
