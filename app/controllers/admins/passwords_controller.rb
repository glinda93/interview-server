module Admins
  class PasswordsController < Devise::PasswordsController
    def edit
      token = params['reset_password_token']
      admin_url = Rails.application.config.setting[:admin_url]
      redirect_url = "#{admin_url}/#/auth/reset-password?reset_password_token=#{token}"
      redirect_to redirect_url, format: :html
    end
  end
end
