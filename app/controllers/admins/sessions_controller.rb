module Admins
  class SessionsController < Devise::SessionsController
    include SessionsHelper

    def create
      params = params_for_sign_in
      self.resource = Admin.find_for_authentication(email: params[:email])
      return render json: { success: false } unless resource&.valid_password?(params[:password])

      resource.send_otp_instructions
      render json: { success: true }
    end

    def otp_verify
      otp = params_for_otp_verify[:otp]
      email = params_for_otp_verify[:email]
      self.resource = Admin.find_by(email: email)

      raise ApplicationError, I18n.t('message.common.invalid_otp') if resource.blank?
      raise ApplicationError, I18n.t('message.common.invalid_otp') unless resource.otp == otp

      resource.invalidate_otp

      sign_in(resource_name, resource)
      headers['Authorization'] = "Bearer #{Warden::JWTAuth::UserEncoder.new.call(resource, resource_name, nil)[0]}"

      yield resource if block_given?
      respond_with resource, location: after_sign_in_path_for(resource)
    end
  end
end
