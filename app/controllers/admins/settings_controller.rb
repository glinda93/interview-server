module Admins
  class SettingsController < ApplicationController
    include AuthenticateUser

    def index
      key = params['key']
      render json: { value: Setting.send(key) }
    end

    def update
      setting_params = params.require(:setting).permit(:key, :value)
      Setting.find_or_initialize_by(key: setting_params[:key])
             .update!(value: setting_params[:value])

      render json: { setting: Setting.find_by(key: setting_params[:key]), success: true }
    end
  end
end
