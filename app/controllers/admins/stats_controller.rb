module Admins
  class StatsController < ApplicationController
    include AuthenticateUser
    include StatsHelper

    def employees
      render json: Stats::Employees.result
    end

    def employers
      render json: Stats::Employers.result
    end

    def report
      render json: {
        devices: Stats::Devices.calc_monthly(report_month_date),
        registrations: Stats::Registrations.calc_monthly(report_month_date),
        offers: Stats::Offers.calc_monthly(report_month_date)
      }
    end
  end
end
