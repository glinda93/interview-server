class ApplicationController < ActionController::Base
  include ApplicationHelper
  include ErrorHandler

  before_action :set_default_request_format, :set_active_storage_current_host
  skip_before_action :verify_authenticity_token
  respond_to :json

  def set_default_request_format
    request.format = :json unless params[:format]
  end

  def set_active_storage_current_host
    ActiveStorage::Current.host = request.base_url
  end

  def skip_bullet
    previous_value = Bullet.enable?
    Bullet.enable = false
    yield
  ensure
    Bullet.enable = previous_value
  end

  def index
    render json: { success: true }
  end
end
