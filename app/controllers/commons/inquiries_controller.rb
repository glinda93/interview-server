module Commons
  class InquiriesController < ApplicationController
    include InquiriesHelper

    def create
      inquiry = Inquiry.new inquiry_params
      inquiry.handle_receipt
      render json: { success: true }
    end
  end
end
