module Commons
  class SettingsController < ApplicationController
    include RenderContent

    def privacy_policy
      render_content Setting.privacy_policy
    end

    def terms
      render_content Setting.terms
    end
  end
end
