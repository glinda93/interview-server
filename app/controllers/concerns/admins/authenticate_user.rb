module Admins
  module AuthenticateUser
    extend ActiveSupport::Concern

    included do
      before_action :authenticate_admin!
    end
  end
end
