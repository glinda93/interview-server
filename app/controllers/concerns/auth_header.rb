module AuthHeader
  extend ActiveSupport::Concern

  included do
    private

    def set_bearer_token(resource) # rubocop:disable Naming/AccessorMethodName
      headers['Authorization'] = "Bearer #{Warden::JWTAuth::UserEncoder.new.call(resource, resource.class.name.underscore, nil)[0]}"
    end
  end
end
