module ChatController
  extend ActiveSupport::Concern

  included do
    around_action :skip_bullet, if: -> { defined?(Bullet) }
  end

  def index
    render json: mutate_pagination_fields(
      paginate(
        @user
          .chat_room_infos
          .joins(:chat_room)
          .includes([employers: { job: { images: { file_attachment: :blob } } }])
          .where(Arel.sql('`chat_rooms`.`last_message_at` IS NOT NULL'))
          .order(Arel.sql('COALESCE(`chat_rooms`.`last_message_at`, `chat_rooms`.`created_at`) DESC'))
      )
    )
  end

  def show
    chat_room_info = ChatRoomInfo.find_by!(chat_room_id: params['id'], chattable: @user)
    render json: chat_room_info.attributes_for(@user)
  end

  def message
    chat_room_info = ChatRoomInfo.find_by!(chat_room_id: params['id'], chattable: @user)
    render json: paginate(chat_room_info.chat_messages.order(created_at: :desc, id: :desc).offset(params['offset'] || 0))
  end

  def create
    params_for_create = params.require(:chat_room).permit(:chattable_id, :chattable_type)
    participants = [@user, find_user(scope: params_for_create[:chattable_type], id: params_for_create[:chattable_id])]
    render json: (ChatRoom.create_or_find_by_participants participants)
  end

  def upload_image
    params_for_image = params.require(:chat_message).permit(:chat_room_id, :image)
    chat_room = ChatRoom.find params_for_image[:chat_room_id]
    raise ApplicationError, I18n.t('message.common.chat_room_invalid') unless chat_room.chattables.include? @user

    ChatMessage.create(
      authorable: @user,
      chat_room: chat_room,
      image: params_for_image[:image]
    )
    # ChatMessage will be broadcasted, instead of JSON response
    render json: { success: true }
  end

  protected

  def mutate_pagination_fields(pagination)
    pagination[:rows] = pagination[:rows].map do |r|
      r.attributes_for @user
    end
    pagination
  end

  def skip_bullet
    previous_value = Bullet.enable?
    Bullet.enable = false
    yield
  ensure
    Bullet.enable = previous_value
  end

  def find_user(scope:, id:)
    scope = scope.camelcase
    klass = if scope == Employee.name
              Employee
            elsif scope == Employer.name
              Employer
            else
              raise ApplicationError, I18n.t('message.common.scope_invalid')
            end
    klass.find id
  end
end
