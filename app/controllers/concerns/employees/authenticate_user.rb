module Employees
  module AuthenticateUser
    extend ActiveSupport::Concern

    included do
      before_action :authenticate_employee!
    end
  end
end
