module Employees
  module AuthorizeUser
    extend ActiveSupport::Concern
    include AuthenticateUser

    included do
      before_action :authorize_employee!
    end

    def authorize_employee!
      @employee = current_employee
      render json: { message: I18n.t('message.employee.unconfirmed') }, status: :unauthorized unless @employee.confirmed?
      raise ApplicationError, I18n.t('message.employee.profile_not_completed') if @employee.profile_completed_at.blank?

      @user = @employee
    end
  end
end
