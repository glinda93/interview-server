module Employers
  module AuthenticateUser
    extend ActiveSupport::Concern

    included do
      before_action :authenticate_employer!
    end
  end
end
