module Employers
  module AuthorizeUser
    extend ActiveSupport::Concern
    include AuthenticateUser

    included do
      before_action :authorize_employer!
    end

    def authorize_employer!
      @employer = current_employer
      @job = @employer.job
      raise UnconfirmedError, I18n.t('message.employer.unconfirmed') unless @employer.confirmed?
      raise ApplicationError, I18n.t('message.employer.not_subscribed') unless @employer.subscribed?
      raise ApplicationError, I18n.t('message.employer.job_not_found') unless @job

      @user = @employer
    end
  end
end
