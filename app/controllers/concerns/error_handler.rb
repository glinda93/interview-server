module ErrorHandler
  extend ActiveSupport::Concern

  included do
    around_action :handle_exceptions, if: :handle_exceptions?
  end

  def handle_exceptions?
    ENV['API_HANDLE_EXCEPTIONS'] == 'true'
  end

  # Catch exception and return JSON-formatted error
  def handle_exceptions
    begin
      yield
    rescue UnconfirmedError => e
      status = 401
    rescue ApplicationError => e
      status = 400
      data = e.data if e.respond_to?(:data)
    rescue ActiveRecord::RecordNotFound => e
      status = 404
      message = I18n.t 'activerecord.errors.messages.record_not_found'
    rescue ActiveRecord::RecordInvalid => e
      status = 422
      message = e.message
    rescue StandardError => e
      status = 500
      data = e.backtrace
    end
    render json: { success: false, message: message || e.class.to_s, errors: [{ detail: e.message }], data: data }, status: status unless e.instance_of?(NilClass)
  end
end
