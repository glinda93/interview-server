module OfferController
  extend ActiveSupport::Concern

  included do
    rescue_from ActiveRecord::RecordNotFound, with: :handle_record_not_found
  end

  def create
    @params = params_for_create
    fill_offerable
    return unless validate_creation!

    @offer = Offer.new @params
    @offer.save!
    render json: { success: true }
  end

  def accept
    respond Offer::RESPONSES[:accepted]
    render json: { success: true }
  end

  def decline
    respond Offer::RESPONSES[:declined]
    render json: { success: true }
  end

  def destroy
    @offer = Offer.find params[:id]
    validate_destroy!
    Offer.destroy params[:id]
    render json: { success: true }
  end

  def handle_record_not_found
    render json: { success: false, message: I18n.t('message.common.offer_not_found') }
  end

  protected

  def respond(response)
    @offer = Offer.find params[:id]
    validate_response! response
    @offer.respond response
  end

  def validate_creation!
    offer_review_missing = @user.pending_review_offer
    if offer_review_missing.present?
      render json: { success: false, message: I18n.t('message.common.offer_not_allowed_before_review'), code: 'offer_not_allowed_before_review', data: offer_review_missing.id }
      return false
    end
    @recruitment = Recruitment.find @params[:recruitment_id]
    validate_recruitment_number

    unless @recruitment.offerable?
      raise ApplicationError, I18n.t('message.common.recruitment_already_completed') if recruitment.completed?
      raise ApplicationError, I18n.t('message.common.recruitment_expired') if recruitment.recruit_date_expired?

      raise ApplicationError, I18n.t('message.common.recruitment_not_available')
    end
    raise ApplicationError, I18n.t('message.common.offer_already_sent') if @user.ongoing_offer_to?(@params)
    raise ApplicationError, I18n.t('message.common.offer_already_received') if @user.ongoing_offer_from?(@params)

    true
  end

  def validate_response!(response)
    raise ApplicationError, I18n.t('message.common.offer_invalid') unless @offer.offered_to? @user
    raise ApplicationError, I18n.t('message.common.offer_already_responded') if @offer.responded?

    return unless response == Offer::RESPONSES[:accepted]

    @recruitment = @offer.recruitment
    validate_recruitment_number
    raise ApplicationError, I18n.t('message.common.recruitment_already_completed') if @offer.recruitment.completed?
  end

  def validate_destroy!
    raise ApplicationError, I18n.t('message.common.offer_invalid') unless @offer.offered_from? @user
    raise ApplicationError, I18n.t('message.common.offer_already_responded') if @offer.responded?
  end

  def validate_recruitment_number
    raise ApplicationError, I18n.t('message.common.recruitment_already_completed') if @recruitment.all_recruited?
  end

  def fill_offerable
    @params[:offerable_from_id] = @user.id
    @params[:offerable_from_type] = @user.class.name
  end
end
