module RenderContent
  extend ActiveSupport::Concern

  def render_content(content)
    respond_to do |format|
      format.json do
        render json: { value: add_viewport(content) }
      end
      format.html do
        render html: add_viewport(content).html_safe # rubocop:disable Rails/OutputSafety
      end
    end
  end

  def add_viewport(content)
    <<~HEREDOC
      <html lang="#{I18n.locale}">
      <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
      </head>
      <body>
      #{content}
      </body>
      </html>
    HEREDOC
  end
end
