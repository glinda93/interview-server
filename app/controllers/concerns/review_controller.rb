module ReviewController
  extend ActiveSupport::Concern

  def index
    render json: mutate_pagination_fields(paginate(@user.reviews.order(created_at: :desc)))
  end

  def create
    @params = params_for_create
    fill_default_fields
    review = Review.find_or_create_by(
      reviewable: @params[:reviewable],
      reviewable_from: @params[:reviewable_from],
      reviewable_to: @params[:reviewable_to]
    )
    review.update @params
    render json: review.attributes_for(@user)
  end
end
