module SettingController
  extend ActiveSupport::Concern
  include RenderContent

  def intro
    render_content Setting.send("intro_#{scope}")
  end

  def faq
    render_content Setting.send("faq_#{scope}")
  end
end
