module Employees
  class ChatsController < ApplicationController
    include AuthorizeUser
    include ChatController
  end
end
