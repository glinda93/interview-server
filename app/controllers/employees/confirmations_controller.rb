module Employees
  class ConfirmationsController < ApplicationController
    include AuthenticateUser
    include ConfirmationsHelper

    before_action :check_confirmation!

    def show
      confirmation_token = params[:confirmation_token]
      confirmation_fail if confirmation_token.blank?
      confirmation_fail if @employee.confirmation_token != confirmation_token
      @employee.phone = @employee.unconfirmed_phone if @employee.unconfirmed_phone.present?
      @employee.unconfirmed_phone = nil
      @employee.confirm!
      render json: @employee
    end

    def create
      if params['phone']
        @employee.unconfirmed_phone = params['phone']
        @employee.save!
      end
      @employee.send_confirmation_instructions
      render json: { success: true }
    end

    protected

    def check_confirmation!
      @employee = current_employee
      raise ApplicationError, I18n.t('message.employee.already_confirmed') unless @employee.confirmation_required?
    end

    def confirmation_fail
      raise ApplicationError, I18n.t('message.employee.confirmation_failed')
    end
  end
end
