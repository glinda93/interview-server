module Employees
  class EmployeesController < ApplicationController
    include AuthenticateUser

    def me
      render json: current_employee
    end

    def delete
      current_employee.soft_destroy
      render json: { success: true }
    end
  end
end
