module Employees
  class JobsController < ApplicationController
    include AuthorizeUser
    include JobsHelper

    def index
      search_params = params_for_search
      search_params[:location] = { pref: @user.profile_pref, city: @user.profile_city } if search_params[:location].blank?
      @pagination = get_jobs_pagination(search_params)
      mutate_pagination_fields
      render json: @pagination
    end

    def show
      @job = Job.with_images.find params[:id]
      wished = @employee.wishlist_job_ids.include?(@job.id)
      attributes =
        if @job.accepted_to? @employee
          @job.as_json
        else
          @job.attributes_for_others
        end
      render json: attributes.merge(wished: wished)
    end

    private

    def mutate_pagination_fields
      @pagination[:rows] = @pagination[:rows].map do |r|
        r.merge(wished: @employee.wishlist_job_ids.include?(r['id']))
      end
      @pagination
    end
  end
end
