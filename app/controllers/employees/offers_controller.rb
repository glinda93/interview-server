module Employees
  class OffersController < ApplicationController
    include AuthorizeUser
    include OfferController
    include OffersHelper

    before_action :init_creation!, only: :create # rubocop:disable Rails/LexicallyScopedActionFilter

    def index
      render json: exclude_fields(
        paginate(
          Offer.preload_for_employee.list_for(@user)
        )
      )
    end

    private

    def init_creation!
      @employer = Employer.find_by id: params_for_create[:offerable_to_id]
      raise ApplicationError, I18n.t('message.employee.employer_not_found') if @employer.nil?

      @job = @employer.job
      raise ApplicationError, I18n.t('message.employee.job_not_found') if @job.nil?
    end
  end
end
