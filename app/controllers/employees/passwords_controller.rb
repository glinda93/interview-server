module Employees
  class PasswordsController < ApplicationController
    include PasswordsHelper

    def create
      email_or_phone = params_for_create
      @employee = Employee.email_or_phone(email_or_phone).first
      raise ApplicationError, I18n.t('message.employee.no_such_registration') unless @employee

      by = if @employee.phone && @employee.phone.delete('+') == email_or_phone.delete('+')
             :phone
           else
             :email
           end
      @employee.send_otp_instructions(by: by)
      render json: { success: true }
    end

    def update
      @employee = Employee.reset_password_by_token(params_for_update)
      return render json: { success: true } if @employee.errors.empty?

      raise ActiveRecord::RecordInvalid, @employee
    end

    def otp_verify
      otp = params_for_otp_verify[:otp]
      email_or_phone = params_for_otp_verify[:email_or_phone]
      @employee = Employee.email_or_phone(email_or_phone).first
      raise ApplicationError, I18n.t('message.employee.no_such_registration') unless @employee
      raise ApplicationError, I18n.t('message.common.invalid_otp') unless @employee.otp == otp

      @employee.invalidate_otp
      render json: { success: true, reset_password_token: @employee.generate_reset_password_token }
    end
  end
end
