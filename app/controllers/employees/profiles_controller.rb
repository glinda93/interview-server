module Employees
  class ProfilesController < ApplicationController
    include AuthenticateUser
    include ProfilesHelper

    def update
      @employee = current_employee
      @params = profile_params
      @params[:profile_completed_at] = DateTime.now.utc if @employee.profile_completed_at.blank?
      handle_confirmation
      handle_update_avatar
      render json: @employee
    end

    def update_avatar
      @employee = current_employee
      @params = avatar_params
      @employee.avatar.purge
      @employee.avatar.attach @params[:avatar] if @params[:avatar].present?
      render json: { avatar_url: @employee.avatar_url }
    end

    def update_pn_enabled
      pn_enabled = params['pn_enabled']
      @employee = current_employee
      @employee.pn_enabled = pn_enabled
      @employee.save!
      device = nil
      device = Device.register_device(user: @employee, device_info: params['device']) if params['device'].present?
      render json: { success: true, device: device }
    end

    protected

    def handle_confirmation
      confirmation_required = reconfirmation_required? || !@employee.confirmed?
      if reconfirmation_required?
        @params[:confirmation_token] = nil
        @params[:unconfirmed_phone] = @params[:phone]
        @params.except(:phone)
      end

      @employee.update @params

      @employee.send_confirmation_instructions if confirmation_required
    end

    def handle_update_avatar
      return if @params[:avatar].blank?

      @employee.avatar.purge
      @employee.avatar.attach @params[:avatar]
    end

    def reconfirmation_required?
      @employee.confirmed? && @employee.phone && @employee[:phone].delete('+') != @params[:phone].delete('+')
    end
  end
end
