module Employees
  class ReviewsController < ApplicationController
    include AuthorizeUser
    include ReviewsHelper
    include ReviewController

    protected

    def fill_default_fields
      @params[:reviewable] = Offer.find @params[:reviewable_id]
      raise ApplicationError, I18n.t('message.common.offer_invalid') unless @params[:reviewable].reviewable? @employee

      @employer = if @params[:reviewable].offerable_to.is_a? Employer
                    @params[:reviewable].offerable_to
                  else
                    @params[:reviewable].offerable_from
                  end
      @params[:reviewable_from] = @employee
      @params[:reviewable_to] = @employer
    end
  end
end
