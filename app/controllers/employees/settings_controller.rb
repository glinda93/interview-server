module Employees
  class SettingsController < ApplicationController
    include AuthenticateUser
    include SettingController

    def scope
      :employee
    end
  end
end
