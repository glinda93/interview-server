module Employers
  class ChatsController < ApplicationController
    include AuthorizeUser
    include ChatController
  end
end
