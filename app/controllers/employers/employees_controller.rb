module Employers
  class EmployeesController < ApplicationController
    include AuthorizeUser
    include EmployeesHelper

    around_action :skip_bullet, only: [:reviews]

    def index
      search_params = employees_search_params
      search_params[:location] = @user.location_for_search if search_params[:location].blank?
      @pagination = get_employee_pagination(search_params)

      render json: @pagination
    end

    def show
      @employee = Employee.with_images.profile_completed.where(id: params[:id]).first
      raise ActiveRecord::RecordNotFound unless @employee

      render json: if @job.accepted_to? @employee
                     @employee.attributes_for_accepted(@employer)
                   else
                     @employee.attributes_for_employer(@employer)
                   end
    end

    def reviews
      employee = Employee.with_images.profile_completed.where(id: params[:id]).first
      raise ActiveRecord::RecordNotFound unless employee

      render json: mutate_pagination_fields(
        paginate(employee.reviews_received.includes(reviewable_from: { job: :skills }).order(created_at: :desc))
      )
    end

    private

    def mutate_pagination_fields(pagination)
      pagination[:rows] = pagination[:rows].map do |r|
        begin
          skills = r.reviewable_from.job.skills
        rescue StandardError => e
          raise e unless Rails.env.production?

          skills = []
        end
        r.as_json.merge({
                          skills: skills
                        })
      end
      pagination
    end
  end
end
