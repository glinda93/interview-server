module Employers
  class EmployersController < ApplicationController
    include AuthenticateUser

    def me
      render json: current_employer.attributes_for_self
    end

    def delete
      current_employer.soft_destroy
      render json: { success: true }
    end
  end
end
