module Employers
  class JobsController < ApplicationController
    include AuthenticateUser
    include JobsHelper

    around_action :skip_bullet, only: [:index, :update_recruitments, :update_images]

    def index
      render json: current_employer.job
    end

    def update
      @employer = current_employer
      @job = @employer.job || Job.new
      @job.employer = @employer
      @params = jobs_params @job
      fill_default_fields
      @job.update! @params
      @recruitments.each(&:save!)
      render json: @job
    end

    def update_images
      @employer = current_employer
      @job = @employer.job
      raise ActiveRecord::RecordNotFound unless @job

      @job.update_images images_params(@job)[:images]
      render json: @job.images
    end

    def update_recruitments
      @employer = current_employer
      @job = @employer.job
      raise ActiveRecrod::RecordNotFound unless @job

      @params = recruitment_params
      fill_recruitments
      @job.update! @params.except(:recruitments)
      @recruitments.each(&:save!)
      render json: @job
    end

    protected

    def fill_default_fields
      @params[:company_name] ||= @employer.company_name
      @params[:pref] ||= @employer.pref
      @params[:city] ||= @employer.city
      @params[:address] ||= @employer.address
      @params[:phone] ||= @employer.phone
      @params[:company_email] ||= @employer.company_email
      fill_recruitments
      @params = @params.except(:recruitments)
    end

    def fill_recruitments
      @recruitments = if @params[:recruitments].present?
                        recruitments = @params[:recruitments]
                        recruitments = recruitments.values if recruitments.respond_to?(:values)
                        recruitments.map do |r|
                          recruitment = if r[:id].present?
                                          @job.recruitments.where(id: r[:id]).first
                                        else
                                          Recruitment.new
                                        end
                          recruitment = Recruitment.new if recruitment.blank?
                          recruitment.job = @job
                          recruitment.number = r[:number]
                          recruitment.recruit_at = r[:recruit_at]
                          recruitment.status = r[:status]
                          recruitment
                        end
                      else
                        []
                      end
    end
  end
end
