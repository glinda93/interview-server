module Employers
  class OffersController < ApplicationController
    include AuthorizeUser
    include OfferController
    include OffersHelper

    def index
      render json: exclude_fields(
        paginate(
          @job.offers.list_for(@user)
        )
      )
    end

    def show
      offer = @job.offers.list_for(@user).where(id: params['id']).first

      raise ActiveRecord::RecordNotFound unless offer

      render json: exclude_fields_from_row(offer)
    end
  end
end
