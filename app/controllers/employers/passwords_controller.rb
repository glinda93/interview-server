module Employers
  class PasswordsController < ApplicationController
    include PasswordsHelper

    def create
      email = params_for_create
      employer = Employer.find_by(email: email)
      raise ApplicationError, I18n.t('message.employer.temporary_token_invalid') if employer.blank?

      employer.send_otp_instructions
      render json: { success: true }
    end

    def update
      employer = Employer.reset_password_by_token params_for_update
      return render json: { success: true } if employer.errors.empty?

      raise ActiveRecord::RecordInvalid, employer
    end

    def otp_verify
      otp = params_for_otp_verify[:otp]
      email = params_for_otp_verify[:email]
      employer = Employer.find_by email: email
      raise ApplicationError, I18n.t('message.employer.no_such_registration') if employer.blank?
      raise ApplicationError, I18n.t('message.common.invalid_otp') unless employer.otp == otp

      employer.invalidate_otp
      render json: { success: true, reset_password_token: employer.generate_reset_password_token }
    end
  end
end
