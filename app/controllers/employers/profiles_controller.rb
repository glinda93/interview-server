module Employers
  class ProfilesController < ApplicationController
    include AuthenticateUser
    include ProfilesHelper

    def update
      @profile = EmployerProfile.new profile_params
      @profile.validate!
      @profile.update employer: current_employer
      render json: current_employer
    end

    def update_pn_enabled
      pn_enabled = params['pn_enabled']
      @employer = current_employer
      @employer.pn_enabled = pn_enabled
      @employer.save!
      device = nil
      device = Device.register_device(user: @employer, device_info: params['device']) if params['device'].present?
      render json: { success: true, device: device }
    end
  end
end
