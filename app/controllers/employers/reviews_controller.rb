module Employers
  class ReviewsController < ApplicationController
    include AuthorizeUser
    include ReviewsHelper
    include ReviewController

    def fill_default_fields
      @params[:reviewable] = Offer.find @params[:reviewable_id]
      raise ApplicationError, I18n.t('message.common.offer_invalid') unless @params[:reviewable].reviewable? @employer

      @employee = if @params[:reviewable].offerable_to.is_a? Employee
                    @params[:reviewable].offerable_to
                  else
                    @params[:reviewable].offerable_from
                  end
      @params[:reviewable_from] = @employer
      @params[:reviewable_to] = @employee
    end
  end
end
