module Employers
  class SessionsController < Devise::SessionsController
    rescue_from Stripe::CardError, with: :handle_card_error

    def create
      self.resource = warden.authenticate!(auth_options)
      set_flash_message!(:notice, :signed_in)
      return handle_subscription_suspended_user if resource.subscription_suspended?

      sign_in_with_resource
    end

    def resubscribe
      return render json: { success: false }, status: :unprocessable_entity if params[:token].blank?

      authenticate_by_temporary_token! params[:token]
      update_stripe_token if card_params.present?

      resource.deleted_at = nil
      resource.resubscribe
      resource.save!
      sign_in(resource_name, resource)
      headers['Authorization'] = "Bearer #{Warden::JWTAuth::UserEncoder.new.call(resource, resource_name, nil)[0]}"
      render json: resource.attributes_for_self
    end

    protected

    def update_stripe_token
      token = Stripe::Token.create card_params
      resource.update_token token
    end

    def handle_card_error(error)
      raise ApplicationError, I18n.t("message.stripe.errors.#{error.code}", default: I18n.t('message.stripe.errors.processing_error'))
    end

    def sign_in_with_resource
      sign_in(resource_name, resource)
      yield resource if block_given?
      respond_with resource.attributes_for_self, location: after_sign_in_path_for(resource)
    end

    def handle_subscription_suspended_user
      render json: {
        subscription_suspended: true,
        subscription: resource.subscription,
        stripe_card: resource.stripe_card,
        token: resource.set_temporary_token
      }
    end

    def authenticate_by_temporary_token!(token)
      key = "auth/Employer/temporary_tokens/#{token}"
      resource = Rails.cache.read(key)
      raise ApplicationError, I18n.t('message.employer.temporary_token_invalid') unless resource

      self.resource = resource
    end

    def card_params
      params.permit(card: [:number, :exp_month, :exp_year, :cvc]).to_h['card']
    rescue StandardError
      nil
    end
  end
end
