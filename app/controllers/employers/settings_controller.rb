module Employers
  class SettingsController < ApplicationController
    include AuthenticateUser
    include SettingController

    def scope
      :employer
    end
  end
end
