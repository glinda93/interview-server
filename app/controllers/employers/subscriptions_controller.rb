module Employers
  class SubscriptionsController < ApplicationController
    include AuthenticateUser
    include SubscriptionsHelper

    def index
      render json: {
        stripe_customer: current_employer.stripe_customer,
        subscription: current_employer.subscription
      }
    end

    def create
      if current_employer.subscription.present? && card_params[:card].blank? && card_params[:token].blank?
        current_employer.resubscribe

        return render json: { success: true }
      end

      begin
        token = card_token
      rescue Stripe::CardError => e
        return handle_card_error(e)
      end

      begin
        current_employer.create_subscription token

        render json: { success: true }
      rescue ApplicationError => e
        render json: { success: false, message: e.message }, status: :unprocessable_entity
      rescue StandardError => e
        handle_standard_error e
      end
    end

    def update_card
      begin
        token = card_token
      rescue Stripe::CardError => e
        return handle_card_error(e)
      end
      stripe_customer = current_employer.stripe_customer
      return create if stripe_customer.nil?

      Stripe::Customer.update(stripe_customer['id'], { source: token['id'] })
      render json: token
    end

    protected

    def card_token
      return card_params[:token] if card_params[:token].present?

      Stripe::Token.create(card_params)
    end

    def handle_standard_error(error)
      logger.tagged("Employer", "Subscription") do
        logger.info("Subscription failed for #{current_employer.inspect}")
        logger.error error.message
        logger.debug error.backtrace.join "\n"
      end
      render json: { success: false, message: I18n.t('message.stripe.errors.processing_error') }, status: :unprocessable_entity
    end

    def handle_card_error(error)
      raise ApplicationError, I18n.t("message.stripe.errors.#{error.code}", default: I18n.t('message.stripe.errors.processing_error'))
    end
  end
end
