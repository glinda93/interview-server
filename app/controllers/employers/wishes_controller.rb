module Employers
  class WishesController < ApplicationController
    include AuthorizeUser
    include WishesHelper

    def index
      render json: mutate_pagination_fields(paginate(@employer.employee_wishes_included_all))
    end

    def create
      @params = params_for_create
      fill_wishable
      wish = Wish.find_or_create_by @params
      wish.save
      render json: { success: true }
    end

    def destroy
      @params = params_for_destroy
      fill_wishable
      Wish.where(@params).destroy_all
      render json: { success: true }
    end

    private

    def fill_wishable
      @params[:wishable_from_id] = @employer.id
      @params[:wishable_from_type] = @employer.class.name
    end
  end
end
