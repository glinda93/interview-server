class OauthController < ApplicationController
  include OauthHelper
  attr_accessor :current_user

  def apple_sign_in
    social_user = Oauth::Apple.get_user!(identity_token: apple_params[:identity_token], user_id: apple_params[:user_id], email: apple_params[:email])
    sign_in_with_social_user social_user: social_user, scope: scope!, device: apple_params[:device]
  end

  def twitter_sign_in
    social_user = Oauth::Twitter.get_user!(auth_token: twitter_params[:auth_token], auth_token_secret: twitter_params[:auth_token_secret])
    sign_in_with_social_user social_user: social_user, scope: scope!, device: apple_params[:device]
  end

  def facebook_sign_in
    social_user = Oauth::Facebook.get_user! access_token: facebook_params[:access_token]
    sign_in_with_social_user social_user: social_user, scope: scope!, device: apple_params[:device]
  end

  protected

  def scope!
    scope = params[:scope].to_s.capitalize
    case scope
    when 'Employee'
      Employee
    when 'Employer'
      Employer
    else
      raise ApplicationError, I18n.t('message.common.scope_invalid')
    end
  end

  def sign_in_with_social_user(social_user:, scope:, device:)
    self.current_user = scope.create_or_find_by_social_auth social_user: social_user, device: device
    return handle_subscription_suspended_user if current_user.is_a?(Employer) && current_user.subscription_suspended?

    if current_user.is_a?(Employee) && current_user.soft_destroyed?
      current_user.deleted_at = nil
      current_user.save!
    end
    sign_in(scope.name.underscore.to_sym, current_user)

    headers['Authorization'] = "Bearer #{Warden::JWTAuth::UserEncoder.new.call(current_user, scope.name.underscore, nil)[0]}"
    yield resource if block_given?
    respond_with current_user.attributes_for_self, location: after_sign_in_path_for(current_user)
  end

  def handle_subscription_suspended_user
    render json: { subscription_suspended?: true, subscription: current_user.subscription, token: current_user.set_temporary_token }
  end
end
