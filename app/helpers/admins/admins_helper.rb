module Admins
  module AdminsHelper
    def change_email_params
      params.require(:admin).permit(:profile_change_token, :unconfirmed_email)
    end

    def change_password_params
      params.require(:admin).permit(:profile_change_token, :password)
    end
  end
end
