module Admins
  module SessionsHelper
    def params_for_sign_in
      params.require(:admin).permit(:email, :password)
    end

    def params_for_otp_verify
      params.permit(:email, :otp)
    end
  end
end
