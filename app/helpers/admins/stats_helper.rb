module Admins
  module StatsHelper
    def report_month_date
      month_date = params['month']
      if month_date.blank?
        Time.zone.now
      else
        begin
          Time.zone.parse month_date
        rescue ArgumentError
          Time.zone.now
        end
      end
    end
  end
end
