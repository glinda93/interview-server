require 'net/http'

module ApplicationHelper
  def paginate(scope)
    collection = scope.page(params[:page])
    current = collection.current_page
    total = collection.total_pages
    per_page = collection.limit_value

    {
      pagination: {
        current: current,
        previous: (current > 1 ? (current - 1) : nil),
        next: (current >= total ? nil : (current + 1)),
        per_page: per_page,
        pages: total,
        count: collection.total_count
      },
      rows: collection
    }
  end
end
