module Commons
  module InquiriesHelper
    def inquiry_params
      params.require(:inquiry).permit(
        :scope,
        :first_name,
        :last_name,
        :first_name_kana,
        :last_name_kana,
        :company_name,
        :department,
        :email,
        :title,
        :content
      )
    end
  end
end
