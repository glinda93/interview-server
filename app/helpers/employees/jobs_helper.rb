module Employees
  module JobsHelper
    def params_for_search
      result = params.permit(:recruit_at, location: [:pref, :city], skill_ids: [])
      result[:location] = JapaneseCityData.replace_pref_and_city result[:location] if result[:location].present?
      result
    end

    private

    def get_jobs_pagination(search_params)
      scope = Job.active.with_images.includes([{ job_skills: :skill }, :skills, :recruitments_offerable])
      scope = scope.location(search_params[:location])
      scope = scope.skills(search_params[:skill_ids]) if search_params[:skill_ids]&.size&.positive?
      scope = scope.recruit_at(Time.zone.parse(search_params[:recruit_at])) if search_params[:recruit_at].present?
      scope = scope.where.not(recruitments_offerable: { id: nil })
      exclude_fields(paginate(scope))
    end

    def exclude_fields(pagination)
      pagination[:rows] = pagination[:rows].map(&:attributes_for_others)
      pagination
    end
  end
end
