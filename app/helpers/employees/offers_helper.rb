module Employees
  module OffersHelper
    def params_for_create
      result = params.require(:offer).permit(:offerable_to_id, :offerable_to_type, :recruitment_id)
      raise ArgumentError if result[:offerable_to_type] != Employer.name

      result
    end

    def exclude_fields(pagination)
      pagination[:rows] = pagination[:rows].map do |r|
        {
          id: r.id,
          recruitment: r.recruitment,
          offerable_to_id: r.offerable_to_id,
          offerable_to_type: r.offerable_to_type,
          offerable_from_id: r.offerable_from_id,
          offerable_from_type: r.offerable_from_type,
          responded_at: r.responded_at,
          response: r.response,
          job: r.recruitment.job.attributes_for_others,
          type: r.offerable_from.instance_of?(Employer) ? 'from' : 'to'
        }
      end
      pagination
    end
  end
end
