module Employees
  module PasswordsHelper
    def params_for_create
      params.require(:email_or_phone)
    end

    def params_for_otp_verify
      params.permit(:otp, :email_or_phone)
    end

    def params_for_update
      params.require(:employee).permit(:reset_password_token, :password)
    end
  end
end
