module Employees
  module ProfilesHelper
    def profile_params
      result = params.require(:profile).permit(
        :first_name,
        :last_name,
        :first_name_kana,
        :last_name_kana,
        :phone,
        :birthday,
        :pref,
        :city,
        :address,
        :profile_pref,
        :profile_city,
        :profile,
        :pn_enabled,
        :gender_id,
        :avatar,
        skill_ids: {}
      )
      result[:pref] = 0 if result[:pref].blank?
      result[:city] = 0 if result[:city].blank?
      result[:gender] = Gender.find result[:gender_id]
      result[:skill_ids] = result[:skill_ids].values if result[:skill_ids].respond_to?(:values)
      result[:skills] = result[:skill_ids][0, 5].map { |s| Skill.find s }
      JapaneseCityData.replace_pref_and_city result
      if result[:profile_pref].present?
        JapaneseCityData.replace_pref_and_city(result, pref_key: :profile_pref, city_key: :profile_city)
      else
        result[:profile_pref] = result[:pref]
        result[:profile_city] = result[:city]
      end
      result
    end

    def avatar_params
      params.permit(:avatar)
    end
  end
end
