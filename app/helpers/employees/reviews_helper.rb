module Employees
  module ReviewsHelper
    def params_for_create
      params.require(:review).permit(:reviewable_id, :reviewable_type, :comment, :rating)
    end

    def mutate_pagination_fields(pagination)
      pagination[:rows] = pagination[:rows].map(&:attributes_for_employee)
      pagination
    end
  end
end
