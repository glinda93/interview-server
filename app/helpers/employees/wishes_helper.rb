module Employees
  module WishesHelper
    def params_for_create
      params.require(:wish).permit(:wishable_to_id, :wishable_to_type)
    end

    def params_for_destroy
      params.require(:wish).permit(:wishable_to_id, :wishable_to_type)
    end
  end
end
