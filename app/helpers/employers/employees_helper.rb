module Employers
  module EmployeesHelper
    def employees_search_params
      params.permit(:gender, :page, location: [:pref, :city], skill_ids: [])
    end

    def exclude_fields(pagination)
      pagination[:rows] = pagination[:rows].map do |i|
        i.attributes_for_employer current_employer
      end
      pagination
    end

    def get_employee_pagination(search_params)
      scope = Employee.includes(:skills)
                      .with_images
                      .profile_completed
      scope = scope.profile_location(search_params[:location])
      scope = scope.gender(search_params[:gender]) if search_params[:gender]
      scope = scope.skills(search_params[:skill_ids]) if search_params[:skill_ids]&.size&.positive?
      exclude_fields(paginate(scope))
    end
  end
end
