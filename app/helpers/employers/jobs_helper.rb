module Employers
  module JobsHelper
    def jobs_params(job = nil)
      result = params.require(:job).permit(
        :company_name,
        :pref,
        :city,
        :address,
        :phone,
        :company_email,
        :desc_main,
        :desc_extra,
        :desc_skill,
        :desc_payment,
        :price,
        :price_type_id,
        :payment_method_id,
        :work_hour_from,
        :work_hour_to,
        :incl_commuting_fee,
        :child_allowed,
        :status,
        skill_ids: {},
        images: [:id, :data, :rank],
        recruitments: [:id, :number, :recruit_at, :status]
      )
      result[:images] = image_records(result[:images], job)
      result[:skill_ids] = result[:skill_ids].values if result[:skill_ids].respond_to?(:values)
      result[:skills] = result[:skill_ids][0, 5].map { |s| Skill.find s }
      result[:price_type] = PriceType.find result[:price_type_id]
      result[:payment_method] = PaymentMethod.find result[:payment_method_id]
      result[:status] = Job::STATUSES[:active] if result[:status].blank?
      JapaneseCityData.replace_pref_and_city result
    end

    def images_params(job = nil)
      result = params.require(:job).permit(images: [:id, :data, :rank])
      # convert to array if images param is object
      result[:images] = result[:images].values if result[:images].respond_to? :values
      result[:images] = image_records(result[:images], job)

      result
    end

    def recruitment_params
      params.permit(:status, recruitments: [:id, :number, :recruit_at, :status])
    end

    def image_records(images, job = nil)
      images = images.values if images.respond_to?(:values)
      images[0, 5].map do |i|
        image = if job.present? && i[:id].present?
                  Image.find_by(id: i[:id], job_id: job[:id])
                else
                  Image.new
                end
        image ||= Image.new
        image.rank = i[:rank]
        image.file.attach i[:data] if i[:data].present?
        image
      end
    end
  end
end
