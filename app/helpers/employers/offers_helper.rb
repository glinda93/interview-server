module Employers
  module OffersHelper
    def params_for_create
      params.require(:offer).permit(:offerable_to_id, :offerable_to_type, :recruitment_id)
    end

    def exclude_fields_from_row(row)
      if row.offerable_from.instance_of?(Employee)
        employee = row.offerable_from
        type = 'from'
      else
        employee = row.offerable_to
        type = 'to'
      end
      employee = if row.response == Offer::RESPONSES[:accepted]
                   employee.attributes_for_accepted current_employer
                 else
                   employee.attributes_for_employers
                 end
      item = {}
      item[:id] = row.id
      item[:employee] = employee
      item[:type] = type
      item[:recruitment] = row.recruitment
      item[:offerable_to_id] = row.offerable_to_id
      item[:offerable_to_type] = row.offerable_to_type
      item[:offerable_from_id] = row.offerable_from_id
      item[:offerable_from_type] = row.offerable_from_type
      item[:responded_at] = row.responded_at
      item[:response] = row.response
      item
    end

    def exclude_fields(pagination)
      pagination[:rows] = pagination[:rows].map do |r|
        exclude_fields_from_row r
      end
      pagination
    end
  end
end
