module Employers
  module PasswordsHelper
    def params_for_create
      params.require(:email)
    end

    def params_for_otp_verify
      params.permit(:otp, :email)
    end

    def params_for_update
      params.require(:employer).permit(:reset_password_token, :password)
    end
  end
end
