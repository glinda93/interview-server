module Employers
  module ProfilesHelper
    def profile_params
      result = params.require(:profile).permit(:company_name, :pref, :city, :address, :phone, :company_email, :desc_manager)
      JapaneseCityData.replace_pref_and_city result
    end
  end
end
