module Employers
  module ReviewsHelper
    def mutate_pagination_fields(pagination)
      pagination[:rows] = pagination[:rows].map(&:attributes_for_employer)
      pagination
    end

    def params_for_create
      params.require(:review).permit(:reviewable_id, :reviewable_type, :comment, :rating)
    end
  end
end
