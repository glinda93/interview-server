module Employers
  module SubscriptionsHelper
    def card_params
      params.require(:subscription).permit(token: [:id], card: [:number, :exp_month, :exp_year, :cvc]).to_h.with_indifferent_access
    end
  end
end
