module Employers
  module ViewHelper
    def base_url
      Rails.application.config.setting[:employer_url]
    end

    def offer_link(offer_id)
      "#{base_url}/tabs/offer/#{offer_id}"
    end

    def chat_room_link(chat_room_id)
      "#{base_url}/tabs/inbox/#{chat_room_id}"
    end
  end
end
