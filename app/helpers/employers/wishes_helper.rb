module Employers
  module WishesHelper
    def params_for_create
      params.require(:wish).permit(:wishable_to_id, :wishable_to_type)
    end

    def params_for_destroy
      params.require(:wish).permit(:wishable_to_id, :wishable_to_type)
    end

    def mutate_pagination_fields(pagination)
      pagination[:rows] = pagination[:rows].map do |u|
        u.attributes_for_employer current_employer
      end
      pagination
    end
  end
end
