module OauthHelper
  def permitted_device_params
    [:uuid, :platform, :model, :environment]
  end

  def twitter_params
    params.permit(:auth_token, :auth_token_secret, device: permitted_device_params)
  end

  def facebook_params
    params.permit(:access_token, device: permitted_device_params)
  end

  def apple_params
    params.permit(:identity_token, :user_id, :email, device: permitted_device_params)
  end
end
