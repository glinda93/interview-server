module ViewHelper
  def chat_message_display_text(message)
    display_text = message.message
    return '' unless display_text
    return display_text.truncate(100) if message_type(display_text) == 'text'

    I18n.t('message.common.image')
  end

  def message_type(message)
    return 'text' unless message
    return 'image' if message.starts_with?('[image]') && message.ends_with?('[/image]')

    'text'
  end
end
