module Chats
  class MessageCreatedJob < ApplicationJob
    queue_as :default

    def perform(chat_message_id:)
      @chat_message = ChatMessage.find_by(id: chat_message_id)
      return if @chat_message.blank?

      @author = @chat_message.authorable
      @chat_room = @chat_message.chat_room
      return unless @chat_room && @author

      update_last_message_at
      create_unread_messages
    end

    def update_last_message_at
      @chat_room.last_message_at = Time.now.utc
      @chat_room.save
    end

    def create_unread_messages
      recipients = @chat_room.sleeping_chattables + @chat_room.offline_chattables
      recipients = recipients.filter do |user|
        user != @author
      end

      recipients.map do |user|
        ChatUnreadMessage.create(
          user: user,
          chat_room: @chat_room,
          chat_message: @chat_message
        )
      end
    end
  end
end
