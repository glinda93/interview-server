module Chats
  class NotifyMessageEventJob < ApplicationJob
    queue_as :default

    def perform(message:, event:)
      chat_room = message.chat_room
      chat_event = {
        event: event,
        payload: message
      }
      ChatChannel.broadcast_to chat_room, chat_event
      chat_room.sleeping_chattables.each do |user|
        AppearanceChannel.broadcast_to user, chat_event
      end

      return if event != ChatChannel::EVENTS[:new_message]

      recipients = chat_room.sleeping_chattables + chat_room.offline_chattables
      pns = []

      recipients.each do |user|
        next if user == message.authorable

        pns += user.create_push_notification({
                                               title: message.authorable.full_name,
                                               body: message.truncated_content
                                             })
      end
      PushNotificationJob.perform_later
      pns
    end
  end
end
