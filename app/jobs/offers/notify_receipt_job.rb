module Offers
  class NotifyReceiptJob < ApplicationJob
    queue_as :default

    def perform(offer)
      @offer = offer
      broadcast
      push_notification
      mail
    end

    protected

    def broadcast
      return unless @offer.offerable_to.online?

      AppearanceChannel.broadcast_to @offer.offerable_to, {
        event: AppearanceChannel::EVENTS[:offer_received],
        payload: @offer.raw_attributes
      }
    end

    def mail
      OfferMailer.with(offer: @offer).new_offer_mail.deliver_later
    end

    def push_notification
      @offer.offerable_to.create_push_notification({
                                                     body: I18n.t('push_notification.offer_received', from: @offer.offerable_from_display_name)
                                                   })
      PushNotificationJob.perform_later
    end
  end
end
