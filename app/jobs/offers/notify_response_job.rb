module Offers
  class NotifyResponseJob < ApplicationJob
    queue_as :default

    def perform(offer)
      @offer = offer
      return unless @offer.responded?

      broadcast
      push_notification
      mail
    end

    protected

    def broadcast
      return unless @offer.offerable_from.online?

      AppearanceChannel.broadcast_to @offer.offerable_from, {
        event: AppearanceChannel::EVENTS[:offer_response],
        payload: @offer.raw_attributes
      }
    end

    def push_notification
      body = push_notification_body
      @offer.offerable_from.create_push_notification({ body: body })
      PushNotificationJob.perform_later
    end

    def push_notification_body
      return I18n.t 'push_notification.offer_accepted', to: @offer.offerable_to_display_name, locale: :ja if @offer.accepted?
      return I18n.t 'push_notification.offer_declined', to: @offer.offerable_to_display_name, locale: :ja if @offer.declined?

      nil
    end

    def mail
      OfferMailer.with(offer: @offer).response_mail.deliver_later
    end
  end
end
