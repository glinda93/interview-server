module Reviews
  class NotifyAvgRatingJob < ApplicationJob
    queue_as :default

    def perform(user)
      return unless user.online?

      AppearanceChannel.broadcast_to user, {
        event: AppearanceChannel::EVENTS[:avg_rating_updated],
        payload: user.avg_rating
      }
    end
  end
end
