class AdminMailer < ApplicationMailer
  def otp_mail
    @model = params[:model]
    mail(to: @model.email, subject: I18n.t('mail.subjects.admin_verification_code'))
  end

  def change_email_mail
    @model = params[:model]
    mail(to: @model.unconfirmed_email, subject: I18n.t('mail.subjects.admin_change_email'))
  end
end
