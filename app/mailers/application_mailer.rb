class ApplicationMailer < ActionMailer::Base
  default(
    from: "インタビュー <#{ENV['SMTP_USERNAME']}>",
    template_path: ->(mailer) { "mailers/#{mailer.class.name.underscore}" }
  )
  layout 'mailer'
end
