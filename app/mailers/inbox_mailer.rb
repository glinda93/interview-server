class InboxMailer < ApplicationMailer
  helper 'view'
  helper 'employers/view'

  def unread_messages_email
    @user = params[:user]
    @messages = params[:messages]

    mail(to: @user.email, subject: I18n.t('mail.subjects.unread_messages'))
  end
end
