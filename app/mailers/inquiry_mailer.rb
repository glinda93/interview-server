class InquiryMailer < ApplicationMailer
  # Send inquiry receipt mail to customer
  def receipt_mail
    @inquiry = params[:inquiry]
    @scope = scope
    mail(to: @inquiry[:email], subject: I18n.t('mail.subjects.inquiry_received'))
  end

  # Send inquiry content to customer service
  def customer_service_mail
    @inquiry = params[:inquiry]
    @scope = scope
    mail(
      to: Rails.application.config.setting[:customer_service_mail],
      subject: I18n.t('mail.subjects.new_inquiry'),
      from: "#{@inquiry[:last_name]}#{@inquiry[:first_name]} <#{@inquiry[:email]}>"
    )
  end

  private

  def scope
    return '求人企業' if @inquiry[:scope] == 'Employer'
    return '求職者' if @inquiry[:scope] == 'Employee'

    ''
  end
end
