class OfferMailer < ApplicationMailer
  helper 'employers/view'

  def new_offer_mail
    @offer = params[:offer]
    mail(to: @offer.offerable_to.email, subject: I18n.t('mail.subjects.new_offer'))
  end

  def response_mail
    @offer = params[:offer]
    mail(to: @offer.offerable_from.email, subject: @offer.accepted? ? I18n.t('mail.subjects.offer_accepted') : I18n.t('mail.subjects.offer_declined'))
  end
end
