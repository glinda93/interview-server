class PasswordMailer < ApplicationMailer
  def password_instructions
    @user = params[:user]
    @display_name = if @user.instance_of? Employer
                      @user.company_name || @user.email
                    else
                      @user.first_name || @user.email
                    end
    @otp = @user.otp
    mail(to: @user.email, subject: I18n.t('mail.subjects.reset_password'))
  end
end
