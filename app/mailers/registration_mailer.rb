class RegistrationMailer < ApplicationMailer
  def welcome_email
    @user = params[:user]
    mail(to: @user.email, subject: I18n.t('mail.subjects.welcome'))
  end
end
