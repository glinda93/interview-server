class Admin < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :validatable,
         :jwt_authenticatable, jwt_revocation_strategy: Devise::JWT::RevocationStrategies::Null

  include HasOtp
  include Appearable

  validates :email, presence: true, format: { with: Devise.email_regexp }
  validates :unconfirmed_email, format: { with: Devise.email_regexp }, allow_blank: true
  validates :password, length: { minimum: 8 }, unless: :password_nil?
  validates :password, presence: true, if: :new_record?

  delegate :nil?, to: :password, prefix: true

  def send_otp_instructions
    generate_otp
    AdminMailer.with(model: self).otp_mail.deliver_later
  end

  def profile_change_token
    Rails.cache.read profile_change_token_key
  end

  def profile_change_token_key
    "#{self.class.name}/#{id}/profile_change_token"
  end

  def regenerate_profile_change_token
    Rails.cache.delete profile_change_token_key
    token = generate_profile_change_token
    Rails.cache.write(profile_change_token_key, token, expires_in: 1.day)
    token
  end

  def generate_profile_change_token
    Faker::Lorem.characters(number: 100)
  end
end
