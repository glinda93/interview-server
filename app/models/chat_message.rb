class ChatMessage < ApplicationRecord
  attr_accessor :skip_broadcast

  belongs_to :chat_room
  belongs_to :authorable, polymorphic: true

  has_one_attached :image
  delegate_missing_to :image

  after_create_commit :perform_message_created_job
  before_destroy :broadcast_message_deleted, unless: :skip_broadcast
  after_save :set_markup_message
  after_save :broadcast_message, unless: :skip_broadcast

  scope :newer_than, ->(date) { where('created_at > ?', date) }
  scope :unread_messages, lambda { |chattable|
    joins(Arel.sql("INNER JOIN `chat_room_infos`
        ON (`chat_messages`.`chat_room_id` = `chat_room_infos`.`chat_room_id`
        AND `chat_room_infos`.`chattable_id` = '#{chattable.id}'
        AND `chat_room_infos`.`chattable_type` = '#{chattable.class.name}')"))
      .where.not(authorable: chattable)
      .where('`chat_messages`.`created_at` > `chat_room_infos`.`last_read_at` OR `chat_room_infos`.`last_read_at` IS NULL')
      .where('`chat_messages`.`chat_room_id` IN (?)', Array.wrap(chattable.chat_rooms.ids || []))
      .distinct
  }

  def broadcast_message
    chat_room.broadcast_message(self, saved_change_to_id? ? ChatChannel::EVENTS[:new_message] : ChatChannel::EVENTS[:message_updated])
  end

  def broadcast_message_deleted
    chat_room.broadcast_message_deleted self
  end

  def set_markup_message
    return if image.blank?

    self.message = "[image]#{Rails.application.routes.url_helpers.url_for(image)}[/image]"
    update_column(:message, message) # rubocop:disable Rails/SkipsModelValidations
  end

  def perform_message_created_job
    Chats::MessageCreatedJob.perform_later(chat_message_id: id)
  end

  def truncated_content
    return I18n.t('message.common.image') if image.present?

    message.truncate(50)
  end
end
