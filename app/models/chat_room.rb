class ChatRoom < ApplicationRecord
  include Streamable

  has_many :chat_room_infos, dependent: :destroy
  has_many :chat_messages, dependent: :destroy
  has_many :employees, through: :chat_room_infos, source: :chattable, source_type: Employee.name, dependent: :destroy
  has_many :employers, through: :chat_room_infos, source: :chattable, source_type: Employer.name, dependent: :destroy

  alias_attribute :participants, :chattables
  alias_attribute :online_participants, :online_chattables
  alias_attribute :offline_particiapnts, :offline_chattables
  alias_attribute :sleeping_participants, :sleeping_chattables

  scope :where_participants, lambda { |users|
    joins(:chat_room_infos)
      .merge(ChatRoomInfo.where(chattable: users))
      .group("`chat_rooms`.`id`")
      .having("COUNT(`chat_rooms`.`id`) = ?", users.count)
  }

  def self.create_or_find_by_participants(users)
    return nil if users.blank?

    existing = where_participants(users).first
    return existing if existing.present?

    model = create!
    users.each do |u|
      ChatRoomInfo.create_from chat_room: model, participant: u
    end
    model
  end

  def chattables
    chat_room_infos.includes([:chattable]).collect(&:chattable)
  end

  def online_chattables(on: nil)
    chattables.select { |user| user.online?(on: on) }
  end

  def offline_chattables(on: nil)
    chattables.reject { |user| user.online?(on: on) }
  end

  def sleeping_chattables
    online_chattables.reject do |user|
      user.online?(on: self)
    end
  end

  def broadcast_message(message, event = ChatChannel::EVENTS[:new_message])
    Chats::NotifyMessageEventJob.perform_now(message: message, event: event)
  end

  def broadcast_message_deleted(message)
    broadcast_message(message, ChatChannel::EVENTS[:message_deleted])
  end
end
