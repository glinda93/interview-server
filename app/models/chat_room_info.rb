class ChatRoomInfo < ApplicationRecord
  belongs_to :chat_room
  belongs_to :chattable, polymorphic: true

  has_many :chat_messages, through: :chat_room
  has_many :employees, through: :chat_room
  has_many :employers, through: :chat_room

  scope :where_chattables, ->(chattables) { where(chattable: chattables) }

  def self.create_from(chat_room:, participant:)
    create(
      chat_room: chat_room,
      chattable_id: participant.id,
      chattable_type: participant.class.name,
      last_read_at: nil
    )
  end

  def employer
    employers.first
  end

  def employee
    employees.first
  end

  def attributes
    super.merge({
                  unread_message_count: unread_message_count,
                  last_message: last_message
                })
  end

  def attributes_for(user)
    return attributes_for_employee if user.instance_of?(Employee)

    attributes_for_employer if user.instance_of?(Employer)
  end

  def attributes_for_employee
    attributes.merge({
                       job: employer.job
                     })
  end

  def attributes_for_employer
    attributes.merge({
                       employee: employee.attributes_for_employers
                     })
  end

  def unread_messages
    chat_messages.where.not(authorable: chattable).where('`chat_messages`.`created_at` > ?', last_read_at)
  end

  def unread_message_count
    unread_messages.count
  end

  delegate :participants, to: :chat_room

  def last_message
    chat_messages.order(created_at: :desc, id: :desc).first
  end
end
