class ChatUnreadMessage < ApplicationRecord
  belongs_to :user, polymorphic: true
  belongs_to :chat_room
  belongs_to :chat_message

  scope :for_notification, lambda { |user|
    includes(:user, :chat_room, :chat_message)
      .where(user: user)
      .order(chat_room_id: :asc)
      .order(created_at: :asc)
      .order(chat_message_id: :asc)
  }

  def self.all_users
    ChatUnreadMessage.includes(:user)
                     .order(created_at: :asc)
                     .order(user_type: :asc)
                     .all.map(&:user)
  end
end
