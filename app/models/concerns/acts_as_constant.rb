module ActsAsConstant
  extend ActiveSupport::Concern

  def attributes
    super.except('created_at', 'updated_at')
  end

  def invalidate_cache
    self.class.invalidate_cache
  end

  included do
    after_create :invalidate_cache
    after_destroy :invalidate_cache

    def self.all_cached
      Rails.cache.fetch(cache_key('all')) do
        all
      end
    end

    def self.invalidate_cache
      Rails.cache.delete cache_key('all')
    end

    def self.cache_key(name)
      "model/#{self.name.underscore}/#{name}"
    end
  end
end
