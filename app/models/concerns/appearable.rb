module Appearable
  extend ActiveSupport::Concern
  # rubocop:disable Metrics/BlockLength
  included do
    def self.appearance_list(on: nil)
      Rails.cache.fetch(appearance_cache_key(on: on)) do
        []
      end
    end

    def appearance_channels
      Rails.cache.fetch(appearance_channels_cache_key) do
        []
      end
    end

    def appear(on: nil)
      appearance_list = self.class.appearance_list(on: on)
      appearance_list.push id unless appearance_list.include? id
      Rails.cache.write(self.class.appearance_cache_key(on: on), appearance_list)
      return if on.blank?

      channels = appearance_channels
      channel_cache_key = self.class.appearance_channel_key on
      channels.push(channel_cache_key) unless channels.include? channel_cache_key
      Rails.cache.write(appearance_channels_cache_key, channels)
    end

    def disappear(on: nil)
      channels = appearance_channels
      if on.present?
        channels.delete self.class.appearance_channel_key on
        Rails.cache.write appearance_channels_cache_key, channels
      else
        channels.each do |c|
          disappear on: c if c.present?
        end
        Rails.cache.delete appearance_channels_cache_key
      end
      appearance_list = self.class.appearance_list on: on
      appearance_list.delete id
      Rails.cache.write (self.class.appearance_cache_key on: on), appearance_list
    end

    def online?(on: nil)
      appearance_list = self.class.appearance_list on: on
      appearance_list.include? id
    end

    def away
      nil # TO DO: implement away status
    end

    protected

    def self.appearance_channel_key(on)
      if on.is_a? ApplicationRecord
        "#{on.class.name}/#{on.id}"
      else
        on.to_s
      end
    end

    def self.appearance_cache_key(on: nil)
      key = "action_cable/appearance/#{name}"
      if on.present?
        channel_key = appearance_channel_key(on)
        key = "#{key}/#{channel_key}"
      end
      key
    end

    def appearance_channels_cache_key
      "action_cable/appearance/#{self.class.name}/#{id}"
    end
  end
  # rubocop:enable Metrics/BlockLength
end
