module ApplicationUser
  extend ActiveSupport::Concern
  include Welcomeable
  include Chattable
  include Reviewable
  include HasOtp
  include Appearable
  include SoftDestroyable
  include SocialAuthable
  include HasDevice
  include Offerable
  include Streamable
  include PushNotifiable

  def serializable_hash(options = nil)
    super(options).merge(confirmed_at: confirmed_at)
  end

  def full_address
    [pref, city, address].reject(&:blank?).join(" ")
  end
end
