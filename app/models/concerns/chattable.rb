module Chattable
  extend ActiveSupport::Concern

  included do
    has_many :chat_room_infos, as: :chattable, dependent: :destroy
    has_many :chat_rooms, through: :chat_room_infos, dependent: :destroy
    has_many :chat_messages, as: :authorable, dependent: :destroy
    has_many :chat_unread_messages, as: :user, dependent: :destroy
  end
end
