module CustomizedDevisable
  extend ActiveSupport::Concern

  included do
    devise :database_authenticatable, :registerable,
           :recoverable, :rememberable, :validatable, :confirmable,
           :jwt_authenticatable, jwt_revocation_strategy: Devise::JWT::RevocationStrategies::Null

    def self.confirm_by_token(confirmation_token, id)
      if confirmation_token.blank?
        confirmable = new
        confirmable.errors.add(:confirmation_token, :blank)
        return confirmable
      end

      if id.blank?
        confirmable = new
        confirmable.errors.add(:id, :blank)
        return confirmable
      end

      confirmable = find_first_by_auth_conditions(confirmation_token: confirmation_token, id: id)

      unless confirmable
        confirmation_digest = Devise.token_generator.digest(self, :confirmation_token, confirmation_token)
        confirmable = find_or_initialize_with_error_by(:confirmation_token, confirmation_digest)
      end

      confirmable.confirm if confirmable.persisted?
      confirmable
    end

    protected

    def generate_confirmation_token
      if confirmation_token && !confirmation_period_expired?
        @raw_confirmation_token = confirmation_token
      else
        self.confirmation_token = @raw_confirmation_token = ROTP::TOTP.new(ROTP::Base32.random).now
        self.confirmation_sent_at = Time.now.utc
      end
    end
  end
end
