module CustomizedRegisterable
  extend ActiveSupport::Concern

  included do
    attr_accessor :skip_registration_notification

    after_create :send_registration_notification
  end

  def send_registraion_notification
    return if skip_registration_notification
  end
end
