module HasDevice
  extend ActiveSupport::Concern

  included do
    has_many :devices, as: :owner, dependent: :destroy
  end

  def android_devices
    devices.where(platform: 'android')
  end

  def ios_devices
    devices.where(platform: 'ios')
  end

  def android_tokens
    android_devices.map(&:token)
  end

  def ios_tokens
    ios_devices.map(&:token)
  end

  def tokens
    devices.map(&:token)
  end
end
