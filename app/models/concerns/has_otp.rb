module HasOtp
  extend ActiveSupport::Concern

  def otp
    Rails.cache.read otp_cache_key
  end

  def generate_otp(force: false)
    Rails.cache.delete otp_cache_key if force
    Rails.cache.fetch(otp_cache_key, expires_in: 1.hour) do
      ROTP::TOTP.new(ROTP::Base32.random).now
    end
  end

  def invalidate_otp
    Rails.cache.delete otp_cache_key
  end

  protected

  def otp_cache_key
    "models/#{self.class.name.underscore}/#{id}/otp"
  end
end
