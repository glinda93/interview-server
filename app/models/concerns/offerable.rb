module Offerable
  extend ActiveSupport::Concern

  def pending_review_offer
    Offer.where_belongs_to(self).accepted.expired.not_reviewed_by(self).first
  end

  def ongoing_offer_to?(params)
    Offer.where(params.merge(offerable_from: self)).not_expired.count.positive?
  end

  def ongoing_offer_from?(params)
    Offer.where(params.merge(offerable_to: self)).not_expired.count.positive?
  end
end
