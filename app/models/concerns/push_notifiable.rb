module PushNotifiable
  extend ActiveSupport::Concern

  def create_push_notification(notification)
    PushNotification::Service.create(user: self, notification: notification)
  end
end
