module Reviewable
  extend ActiveSupport::Concern

  included do
    alias_attribute :reviews, :reviews_received

    has_many :reviews_left, as: :reviewable_from, class_name: 'Review', dependent: :destroy
    has_many :reviews_received, as: :reviewable_to, class_name: 'Review', dependent: :destroy

    def avg_rating
      reviews.average(:rating)
    end
  end
end
