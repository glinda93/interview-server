require 'faker'

module SocialAuthable
  extend ActiveSupport::Concern

  included do
    has_one :social_auth, as: :user, dependent: :destroy

    def self.create_or_find_by_social_auth(social_user:, device: nil)
      scope = name
      sociauth = SocialAuth.find_by(user_type: scope, uid: social_user[:id], provider: social_user[:provider])
      return sociauth.user if sociauth.present? && sociauth.user.present?

      if sociauth.present?
        sociauth.destroy
        return nil
      end

      sociauth = SocialAuth.new(
        user_type: scope,
        uid: social_user[:id],
        provider: social_user[:provider]
      )
      user = find_by(email: social_user[:email]) if social_user[:email].present?
      if user.blank? && device.present?
        device_model = Device.find_by(owner_type: scope, uuid: device[:uuid], platform: device[:platform], model: device[:model], environment: device[:environment])
        user = device_model.owner if device_model.present?
      end
      if user.blank?

        raise Oauth::EmailEmptyError, I18n.t('message.common.social_auth_email_empty', provider: social_user[:provider]) if social_user[:email].blank?

        password = Faker::Internet.password
        user = create!(
          email: social_user[:email],
          password: password,
          password_confirmation: password,
          confirmed_at: social_user[:provider] == 'apple' ? Time.now.utc : nil
        )
        if scope == Employer.name
          user.confirmed_at = Time.now.utc
          user.save
        end
      end

      sociauth.user = user
      sociauth.save!
      user.reload
      user
    end
  end
end
