module SoftDestroyable
  extend ActiveSupport::Concern

  included do
    scope :soft_destroyed, -> { where.not(deleted_at: nil) }
    scope :not_soft_destroyed, -> { where(deleted_at: nil) }
    scope :soft_destroy_expired, -> { soft_destroyed.where("deleted_at < ?", Rails.application.config.setting[:soft_destroy_expires_in].to_i.days.ago.utc) }

    def soft_destroyed?
      deleted_at.present?
    end

    def soft_destroy
      self.deleted_at = Time.now.utc
    end
  end
end
