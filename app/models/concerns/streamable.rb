module Streamable
  extend ActiveSupport::Concern

  def stream_name
    raise ArgumentError, 'model id is empty' if id.blank?

    "#{self.class.name.underscore}:#{id}"
  end
end
