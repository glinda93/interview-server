module Subscribable
  extend ActiveSupport::Concern

  included do
    alias_attribute :subscribed?, :subscribed
    has_one :subscription, as: :subscriber, dependent: :destroy
  end

  def subscribed
    subscription.present? && !subscription.suspended?
  end

  def subscribed?
    subscribed
  end

  def subscription_suspended?
    subscription.present? && subscription.suspended?
  end

  def subscribed_at
    return nil unless subscribed?

    subscription.created_at
  end

  def create_subscription(token)
    raise ApplicationError, I18n.t('message.employer.already_subscribed') if subscribed?

    if subscription.present? && !subscribed?
      update_token token
      resubscribe
      return
    end

    token = token[:id] unless token.is_a? String
    customer_params = stripe_customer_params.merge({ source: token })
    customer = Stripe::Customer.create(customer_params)
    subscription = Subscriptions::Stripe.create_subscription stripe_customer: customer
    data = {
      customer_id: customer[:id],
      subscription_id: subscription[:id]
    }
    self.subscription = Subscription.create({
                                              subscriber: self,
                                              data: data,
                                              payment_gateway: Subscription::PAYMENT_GATEWAYS[:stripe],
                                              trial_ends_at: Time.zone.at(subscription.trial_end)
                                            })
  end

  def update_token(token)
    token = token[:id] unless token.is_a? String
    Stripe::Customer.update(stripe_customer['id'], { source: token })
  end

  def resubscribe
    raise ApplicationError, I18n.t('message.employer.already_subscribed') if subscribed?
    raise ApplicationError, I18n.t('message.employer.payment_not_inited') if subscription.blank?

    customer = Stripe::Customer.retrieve subscription.data['customer_id']
    # if the trial period is not finished
    if subscription.trial_ends_at > Time.now.utc
      next_billing_at = subscription.trial_ends_at
      backdate = nil
    else
      # billing backdate is the start date of current month, i.e. customer needs to pay for the current month
      backdate = Time.now.utc.at_beginning_of_month
      next_billing_at = nil
    end

    logger.tagged("Employer", "Subscription") do
      logger.info("Resubscribe employer with ID: #{id}")
      logger.info("Subscription: #{subscription.inspect}")
      logger.info("backdate: #{backdate.inspect}, next_billing_at: #{next_billing_at.inspect}")
    end

    stripe_subscription = Subscriptions::Stripe.create_subscription stripe_customer: customer, backdate: backdate, next_billing_at: next_billing_at
    subscription.data['subscription_id'] = stripe_subscription[:id]
    subscription.suspended_at = nil
    subscription.save!
  end

  def delete_subscription
    return unless subscribed?

    subscription.destroy
  end

  def suspend_subscription
    return unless subscribed?

    subscription.suspend
  end

  def stripe_customer
    return nil unless subscription&.data && subscription.data['customer_id']

    begin
      Stripe::Customer.retrieve(subscription.data['customer_id'])
    rescue Stripe::InvalidRequestError
      nil
    end
  end

  def stripe_card
    customer = stripe_customer
    return nil unless customer

    begin
      Stripe::Customer.retrieve_source(customer.id, customer.default_source)
    rescue StandardError => e
      raise e unless Rails.env.production?

      nil
    end
  end

  protected

  def stripe_customer_params
    {
      name: company_name,
      email: email
    }
  end
end
