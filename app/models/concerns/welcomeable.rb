module Welcomeable
  extend ActiveSupport::Concern

  included do
    after_create :send_welcome_email
  end

  def send_welcome_email
    RegistrationMailer.with(user: self).welcome_email.deliver_later
  end
end
