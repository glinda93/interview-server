class Constant
  def self.all
    {
      genders: Gender.all_cached,
      payment_methods: PaymentMethod.all_cached,
      price_types: PriceType.all_cached,
      skills: Skill.all_cached
    }
  end

  def self.invalidate_cache
    Rails.cache.delete cache_key
  end

  def self.cache_key
    "models/#{self.class.name}"
  end
end
