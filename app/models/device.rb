class Device < ApplicationRecord
  belongs_to :owner, polymorphic: true

  validates :uuid, length: { maximum: 500 }, presence: true
  validates :platform, inclusion: %w[android ios web]
  validates :model, length: { maximum: 100 }
  validates :environment, inclusion: %w[development production test]
  validates :token, length: { maximum: 500 }

  class << self
    def register_device(user:, device_info:)
      device = Device.find_or_initialize_by(
        owner: user,
        uuid: device_info['uuid'],
        platform: device_info['platform'],
        model: device_info['model'],
        environment: device_info['environment']
      )
      device.update!(token: device_info['token'])

      device
    end
  end
end
