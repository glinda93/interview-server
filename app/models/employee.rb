require 'csv'

class Employee < ApplicationRecord
  include ApplicationUser

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :jwt_authenticatable, jwt_revocation_strategy: Devise::JWT::RevocationStrategies::Null

  belongs_to :gender
  has_many :employee_skills, dependent: :destroy
  has_many :skills, through: :employee_skills
  has_many :wishes, as: :wishable_from, dependent: :destroy
  has_many :job_wishes, through: :wishes, source: :wishable_to, source_type: Job.name, dependent: :destroy
  has_many :job_offers_sent, as: :offerable_from, class_name: 'Offer', dependent: :destroy
  has_many :job_offers_received, as: :offerable_to, class_name: 'Offer', dependent: :destroy

  has_one_attached :avatar

  validates :first_name, length: { maximum: 10 }, allow_nil: true
  validates :last_name, length: { maximum: 10 }, allow_nil: true
  validates :first_name_kana, length: { maximum: 20 }, allow_nil: true
  validates :last_name_kana, length: { maximum: 20 }, allow_nil: true
  validates :birthday, date_time: true, allow_nil: true
  validates :pref, length: { minimum: 1, maximum: 99 }, allow_nil: true
  validates :city, length: { minimum: 1, maximum: 99 }, allow_nil: true
  validates :phone, length: { minimum: 10, maximum: 16 }, numericality: true, allow_nil: true
  validates :profile, length: { maximum: 500 }, allow_nil: true
  validates :pn_enabled, inclusion: [true, false]
  validates :avatar, content_type: ['image/png', 'image/jpg', 'image/jpeg'], size: { less_than: 20.megabytes }

  scope :with_images, -> { includes(avatar_attachment: :blob) }
  scope :location, ->(loc) { where(pref: loc[:pref], city: loc[:city]) }
  scope :profile_location, ->(loc) { where(profile_pref: loc[:pref], profile_city: loc[:city]) }
  scope :gender, ->(gender) { where(gender_id: gender) }
  scope :skills, ->(skills) { includes(:skills).where(skills: { id: skills }) }
  scope :profile_completed, -> { not_soft_destroyed.where.not(profile_completed_at: nil) }
  scope :email_or_phone, ->(params) { where(email: params).or(where(phone: params.delete('+'))) }

  def self.confirm_within
    1.day
  end

  def self.to_csv
    attrs = %w[id created_at full_name birthday gender full_address email phone]

    CSV.generate(headers: true, encoding: Encoding::SJIS, force_quotes: true, row_sep: "\r\n") do |csv|
      csv << attrs.map do |attr|
        I18n.t("activerecord.attributes.employee.#{attr}")
      end

      where.not(profile_completed_at: nil).where(deleted_at: nil).preload(:gender, :skills).find_each do |user|
        csv << user.csv_record
      end
    end
  end

  def self.csv_filename
    "#{name.underscore.pluralize}_#{Time.zone.now.strftime('%Y_%m_%d')}.csv"
  end

  def csv_record
    [
      id,
      created_at.strftime('%Y-%m-%d %H:%M:%S'),
      full_name,
      birthday ? birthday.strftime('%Y-%m-%d') : '',
      gender ? gender.name : '',
      full_address,
      email,
      phone
    ]
  end

  def phone=(value)
    if value.blank?
      super(nil)
    else
      super(value.delete("+"))
    end
  end

  def age
    return nil if birthday.blank?

    now = Time.now.utc.to_date
    now.year - birthday.year - (now.month > birthday.month || (now.month == birthday.month && now.day >= birthday.day) ? 0 : 1)
  end

  def age_group
    return nil if age.blank?

    (age / 10).to_i * 10
  end

  def avatar_url
    return nil if avatar.blank?

    Rails.application.routes.url_helpers.url_for(avatar.blob.variant(resize_to_limit: [300, 300]).processed)
  end

  def full_name
    "#{last_name}#{first_name}"
  end

  def display_name
    I18n.t('message.employee.display_name', name: full_name)
  end

  def job_wishes_included_all
    job_wishes.includes({ images: { file_attachment: :blob } }, :job_skills, :skills)
  end

  def reviews_included_all
    reviews
  end

  def attributes
    super.merge({
                  age_group: age_group,
                  avatar_url: avatar_url,
                  skills: skills,
                  avg_rating: avg_rating,
                  confirmed_at: confirmed_at
                })
  end

  def attributes_for_self
    attributes
  end

  def attributes_for_employers
    as_json.except(
      'email',
      'phone',
      'birthday',
      'pn_enabled',
      'profile_completed_at',
      'pref',
      'city',
      'address',
      'updated_at',
      'deleted_at',
      'unconfirmed_phone'
    )
  end

  def attributes_for_employer(employer)
    attributes_for_accepted(employer).except(
      'email',
      'phone',
      'birthday',
      'pref',
      'city',
      'address',
      'updated_at',
      'deleted_at',
      'unconfirmed_phone'
    )
  end

  def attributes_for_accepted(employer)
    as_json.except(
      'pn_enabled',
      'profile_completed_at',
      'unconfirmed_phone'
    ).merge({
              wished: employer.wishlist_employee_ids.include?(id)
            })
  end

  def send_confirmation_instructions
    generate_confirmation_token unless confirmation_token
    RegistrationTexter.confirmation_instructions(self).deliver_later
  end

  def generate_confirmation_token
    self.confirmed_at = nil
    self.confirmation_sent_at = Time.now.utc
    self.confirmation_token = ROTP::TOTP.new(ROTP::Base32.random).now
    save!
  end

  def confirmation_period_expired?
    self.class.confirm_within && confirmation_sent_at && (Time.now.utc > confirmation_sent_at.utc + self.class.confirm_within)
  end

  def confirm!
    self.confirmed_at = Time.now.utc
    self.confirmation_token = nil
    save!
  end

  def confirmed?
    !!confirmed_at
  end

  def confirmation_required?
    !confirmed? || unconfirmed_phone.present?
  end

  def send_otp_instructions(by: nil)
    generate_otp
    if by.nil?
      by = if employee.confirmed?
             :phone
           else
             :email
           end
    end
    case by
    when :phone
      PasswordTexter.password_instructions(self).deliver_later
    when :email
      PasswordMailer.with(user: self).password_instructions.deliver_later
    end
  end

  def generate_reset_password_token
    set_reset_password_token
  end

  def soft_destroy
    self.deleted_at = Time.now.utc
    self.pn_enabled = false
    save!
  end

  def after_database_authentication
    update_attribute(:deleted_at, nil) # rubocop:disable Rails/SkipsModelValidations
  end

  def wishlist_job_ids
    Rails.cache.fetch("Employee/#{id}/Wishes/Jobs") do
      job_wishes.all.pluck(:id)
    end
  end
end
