class Employer < ApplicationRecord
  include ApplicationUser
  include CustomizedDevisable
  include Subscribable

  has_one :job, dependent: :destroy
  has_many :wishes, as: :wishable_from, dependent: :destroy
  has_many :employee_wishes, through: :wishes, source: :wishable_to, source_type: Employee.name, dependent: :destroy
  has_many :offers, as: :offerable_to, dependent: :destroy
  has_many :job_offers, through: :offers, source: :offerable_from, source_type: Job.name, dependent: :destroy

  alias_attribute :display_name, :company_name

  scope :has_job, -> { where.not(job: { id: nil }) }

  def self.to_csv
    attrs = %w[id created_at company_name desc_manager full_address email phone]

    CSV.generate(headers: true, encoding: Encoding::SJIS, force_quotes: true, row_sep: "\r\n") do |csv|
      csv << attrs.map do |attr|
        I18n.t("activerecord.attributes.employer.#{attr}")
      end

      where.not(confirmed_at: nil).find_each do |user|
        csv << user.csv_record
      end
    end
  end

  def self.csv_filename
    "#{name.underscore.pluralize}_#{Time.zone.now.strftime('%Y_%m_%d')}.csv"
  end

  def csv_record
    [
      id,
      created_at.strftime('%Y-%m-%d %H:%M:%S'),
      company_name,
      desc_manager,
      full_address,
      email,
      phone
    ]
  end

  def employee_wishes_included_all
    employee_wishes.includes([{ avatar_attachment: :blob }, :employee_skills, :skills])
  end

  def attributes_for_self
    as_json.merge({
                    subscribed: subscribed?,
                    confirmed: confirmed_at.present?,
                    avg_rating: avg_rating
                  })
  end

  def full_address
    "#{pref}#{city}#{address}"
  end

  def full_name
    return job.company_name if job.present?

    company_name
  end

  def location_for_search
    if job
      {
        pref: job.pref,
        city: job.city
      }
    else
      {
        pref: pref,
        city: city
      }
    end
  end

  def soft_destroy
    suspend_subscription
    self.pn_enabled = false
    self.deleted_at = Time.now.utc
    if job.present?
      job.status = Job::STATUSES[:inactive]
      job.save!
    end
    save!
    reload
  end

  def set_temporary_token
    token = Faker::Lorem.characters(number: 100)
    Rails.cache.write("auth/Employer/temporary_tokens/#{token}", self, expires_in: 1.hour)
    token
  end

  def send_otp_instructions
    generate_otp
    PasswordMailer.with(user: self).password_instructions.deliver_now
  end

  def generate_reset_password_token
    set_reset_password_token
  end

  def wishlist_employee_ids
    Rails.cache.fetch("Employer/#{id}/Wishes/Employees") do
      employee_wishes.all.pluck(:id)
    end
  end

  def avatar_url
    return nil if job.blank? || job.images.blank? || job.images[0].blank?

    job.images[0].url
  end
end
