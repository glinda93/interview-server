class EmployerProfile
  include ActiveModel::Model

  attr_accessor(
    :company_name,
    :pref,
    :city,
    :address,
    :phone,
    :company_email,
    :desc_manager
  )

  validates :company_name, presence: true, length: { minimum: 2, maximum: 100 }
  validates :pref, presence: true, length: { minimum: 1, maximum: 99 }
  validates :city, presence: true, length: { minimum: 1, maximum: 99 }
  validates :address, presence: true, length: { minimum: 2, maximum: 150 }
  validates :phone, presence: true, length: { minimum: 10, maximum: 15 }, numericality: true
  validates :company_email, presence: true, format: { with: Devise.email_regexp }
  validates :desc_manager, length: { minimum: 1, maximum: 100 }, allow_blank: true

  def fill(employer)
    employer.company_name = company_name
    employer.pref = pref
    employer.city = city
    employer.address = address
    employer.phone = phone
    employer.company_email = company_email
    employer.desc_manager = desc_manager
  end

  def update(employer:)
    fill employer
    employer.save
  end
end
