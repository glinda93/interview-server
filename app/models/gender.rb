class Gender < ApplicationRecord
  include ActsAsConstant
  has_many :employees, dependent: :destroy
end
