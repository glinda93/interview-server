class Image < ApplicationRecord
  belongs_to :job

  has_one_attached :file
  delegate_missing_to :file

  validates :rank, presence: true, numericality: true
  validates :file, attached: true, content_type: ['image/png', 'image/jpg', 'image/jpeg'], size: { less_than: 20.megabytes }

  def url
    Rails.application.routes.url_helpers.url_for(file.variant(resize_to_limit: [300, 300]).processed)
  rescue StandardError
    ''
  end

  def attributes
    super.merge({
                  url: url
                })
  end
end
