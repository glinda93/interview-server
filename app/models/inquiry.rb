class Inquiry
  include ActiveModel::Model

  attr_accessor(
    :scope,
    :first_name,
    :last_name,
    :first_name_kana,
    :last_name_kana,
    :company_name,
    :department,
    :email,
    :title,
    :content
  )

  validates :scope, presence: true, inclusion: %w[employee employer]
  validates :first_name, length: { maximum: 10 }, presence: true
  validates :last_name, length: { maximum: 10 }, presence: true
  validates :first_name_kana, length: { maximum: 20 }, presence: true
  validates :last_name_kana, length: { maximum: 20 }, presence: true
  validates :company_name, length: { maximum: 100 }, allow_nil: true
  validates :department, allow_nil: true, length: { maximum: 100 }
  validates :email, presence: true, format: { with: Devise.email_regexp }
  validates :title, presence: true, length: { minimum: 2, maximum: 500 }
  validates :content, presence: true, length: { minimum: 2, maximum: 999 }

  def handle_receipt
    InquiryMailer.with(inquiry: as_json.with_indifferent_access).receipt_mail.deliver_later
    InquiryMailer.with(inquiry: as_json.with_indifferent_access).customer_service_mail.deliver_later
  end

  def save!
    self
  end

  def save
    self
  end
end
