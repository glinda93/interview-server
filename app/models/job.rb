class Job < ApplicationRecord
  include Reviewable
  STATUSES = {
    active: 'ACTIVE',
    inactive: 'INACTIVE',
    draft: 'DRAFT'
  }.freeze

  belongs_to :employer
  belongs_to :price_type
  belongs_to :payment_method
  has_many :job_skills, dependent: :destroy
  has_many :skills, through: :job_skills
  has_many :images, dependent: :destroy
  has_many :recruitments, dependent: :destroy
  has_many :recruitments_offerable, lambda {
                                      offerable
                                        .order(recruit_at: :desc)
                                        .order(created_at: :desc)
                                        .limit(Rails.application.config.setting[:max_available_recruitments])
                                        .where('recruit_at >= ?', Time.zone.now.beginning_of_day)
                                        .where(status: STATUSES[:active])
                                    }, class_name: 'Recruitment', inverse_of: :job
  has_many :offers, through: :recruitments, dependent: :destroy
  has_many :employee_offers, through: :offers, source: :offerable_from, source_type: Employee.name, dependent: :destroy
  has_many :employer_offers, through: :offers, source: :offerable_to, source_type: Employer.name, dependent: :destroy

  alias_attribute :display_name, :company_name

  scope :location, ->(loc) { where(pref: loc[:pref], city: loc[:city]) }
  scope :skills, ->(skills) { includes(:skills).where(skills: { id: skills }) }
  scope :recruit_at, lambda { |date|
    joins(:recruitments_offerable)
      .where(recruitments_offerable: { recruit_at: date.beginning_of_day..date.end_of_day })
      .where.not(recruitments_offerable: { id: nil })
  }
  scope :with_images, -> { includes({ images: { file_attachment: :blob } }) }
  scope :active, -> { where(status: STATUSES[:active]) }

  validates :employer, presence: true
  validates :company_name, presence: true, length: { minimum: 2, maximum: 100 }
  validates :pref, presence: true, length: { minimum: 1, maximum: 99 }
  validates :city, presence: true, length: { minimum: 1, maximum: 99 }
  validates :address, presence: true, length: { minimum: 2, maximum: 150 }
  validates :phone, presence: true, length: { minimum: 8, maximum: 15 }, numericality: true
  validates :company_email, presence: true, format: { with: Devise.email_regexp }
  validates :desc_main, length: { maximum: 500 }
  validates :desc_extra, length: { maximum: 500 }
  validates :desc_skill, length: { maximum: 500 }
  validates :desc_payment, length: { maximum: 500 }
  validates :price, presence: true, numericality: { greater_than: 0 }
  validates :price_type, presence: true
  validates :payment_method, presence: true
  validates :work_hour_from, presence: true, date_time: true
  validates :work_hour_to, presence: true, date_time: true
  validates :incl_commuting_fee, inclusion: [true, false]
  validates :child_allowed, inclusion: [true, false]
  validates :status, inclusion: STATUSES.values

  validate :work_hour_from_cannot_be_after_work_hour_to

  def work_hour_from=(val)
    self[:work_hour_from] = DateTime.parse(val).strftime("%H:%M")
  end

  def work_hour_to=(val)
    self[:work_hour_to] = DateTime.parse(val).strftime("%H:%M")
  end

  def attributes
    super.merge({
                  images: images.includes(file_attachment: :blob),
                  skills: skills,
                  avg_rating: avg_rating,
                  recruitments_offerable: recruitments_offerable
                })
  end

  def attributes_for_others
    as_json.except(
      'phone',
      'company_email'
    )
  end

  def work_hour_from_cannot_be_after_work_hour_to
    errors.add(:work_hour_from, I18n.t('activerecord.errors.messages.cannot_be_after')) if work_hour_from >= work_hour_to
  end

  def accepted_to?(employee)
    offers.where_belongs_to(employee).accepted.count.positive?
  end

  def update_images(images_data)
    image_ids = []
    images_data.each do |i|
      i.job = self
      i.save!
      image_ids.push(i.id)
    end
    images.where.not(id: image_ids).destroy_all
    reload
    self
  end
end
