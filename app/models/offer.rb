class Offer < ApplicationRecord
  RESPONSES = {
    accepted: 'ACCEPTED',
    declined: 'DECLINED'
  }.freeze

  attr_accessor :skip_notify_new_offer, :skip_notify_remove

  belongs_to :recruitment
  belongs_to :offerable_to, polymorphic: true
  belongs_to :offerable_from, polymorphic: true
  has_many :reviews, as: :reviewable, dependent: :destroy

  delegate :job, to: :recruitment

  scope :not_expired, lambda {
    joins(:recruitment).where("`recruitments`.`recruit_at` >= ?", Time.zone.now.beginning_of_day)
  }
  scope :expired, -> { joins(:recruitment).where("`recruitments`.`recruit_at` < ?", Time.zone.now.beginning_of_day) }
  scope :where_belongs_to, lambda { |model|
    where(offerable_from: model).or(where(offerable_to: model))
  }
  scope :responded, -> { where.not(responded_at: nil) }
  scope :not_responded, -> { where(responded_at: nil) }
  scope :accepted, -> { responded.where(response: RESPONSES[:accepted]) }
  scope :not_accepted, -> { where.not(response: RESPONSES[:accepted]).or(where(response: nil)) }
  scope :declined, -> { responded.where(response: RESPONSES[:declined]) }
  scope :not_declined, -> { responded.where.not(response: RESPONSES[:declined]).or(where(response: nil)) }
  scope :expired_or_declined, -> { expired.or(declined) }
  scope :obsolete, -> { expired.where.not(id: accepted) }
  scope :reviewed, -> { includes(:reviews).where.not(review: { id: nil }) }
  scope :not_reviewed, -> { includes(:reviews).where(review: { id: nil }) }
  scope :review_missing, -> { where("`offers`.`reviews_count` < ?", 2).or(where(reviews_count: nil)) }
  scope :review_missing_hard, -> { joins(:reviews).having("COUNT(`reviews`.`id`) < ?", 2).group(:id) }
  scope :review_missing_remind, -> { where("`offers`.`responded_at` <= ?", 24.hours.ago).accepted.review_missing }
  scope :reviewed_by, ->(user) { left_outer_joins(:reviews).where(reviews: { reviewable_from: user }) }
  scope :not_reviewed_by, lambda { |user|
    joins(Arel.sql("LEFT OUTER JOIN `reviews` ON `reviews`.`reviewable_type` = '#{name}'
        AND `reviews`.`reviewable_id` = `#{table_name}`.`id`
        AND `reviews`.`reviewable_from_type` = '#{user.class.name}'
		    AND `reviews`.`reviewable_from_id` = '#{user.id}'
    "))
      .where(reviews: { id: nil })
  }
  scope :preload_for_employee, -> { preload({ recruitment: { job: [{ images: { file_attachment: :blob } }, { job_skills: :skill }, :skills] } }, :offerable_from) }
  scope :list_for_query, lambda { |user|
    where("`offers`.`id` IN (?)", Array.wrap(Offer.where_belongs_to(user).accepted.not_reviewed_by(user).pluck(:id)))
      .or(not_expired.not_declined)
  }
  scope :list_for, lambda { |user|
    where(offerable_from: user, visible_to_offerable_from: true)
      .or(where(offerable_to: user, visible_to_offerable_to: true))
  }

  after_create :notify_new_offer, unless: :skip_notify_new_offer

  before_destroy :notify_remove, unless: :skip_notify_remove
  after_destroy :update_recruitment_matches_count
  after_save :update_recruitment_matches_count

  def offered_to?(user)
    offerable_to&.eql?(user)
  end

  def offered_from?(user)
    offerable_from&.eql?(user)
  end

  def responded?
    responded_at.present?
  end

  def accepted?
    responded? && response == RESPONSES[:accepted]
  end

  def declined?
    responded? && response == RESPONSES[:declined]
  end

  def respond(response)
    self.responded_at = DateTime.now.utc
    self.response = response
    if response != RESPONSES[:accepted]
      self.visible_to_offerable_from = false
      self.visible_to_offerable_to = false
    end

    save!
    notify_response
    self
  end

  def attributes
    super.merge({
                  offerable: offerable,
                  offerable_from: offerable_from,
                  offerable_to: offerable_to,
                  reviews: reviews
                })
  end

  def raw_attributes
    ApplicationRecord.instance_method(:attributes).bind_call(self)
  end

  def belongs_to?(model)
    (offerable_from_type == model.class.name && offerable_from_id == model.id) || (offerable_to_type == model.class.name && offerable_to_id == model.id) || (offerable_type == model.class.name && offerable_id == model.id) # rubocop:disable Layout/LineLength
  end

  def reviewable?(model)
    belongs_to?(model) && accepted?
  end

  def notify_new_offer
    Offers::NotifyReceiptJob.perform_later self
  end

  def notify_remove
    return unless offerable_to&.online?

    AppearanceChannel.broadcast_to offerable_to, {
      event: AppearanceChannel::EVENTS[:offer_deleted],
      payload: id
    }
  end

  def notify_response
    Offers::NotifyResponseJob.perform_later self
  end

  def offerable_from_display_name
    if offerable_from.is_a? Employee
      offerable_from.display_name
    else
      recruitment.display_name
    end
  end

  def offerable_to_display_name
    if offerable_to.is_a? Employee
      offerable_to.display_name
    else
      recruitment.display_name
    end
  end

  def users_did_not_review
    users = [offerable_from, offerable_to]
    return users if reviews.blank?

    reviews.each do |r|
      users.delete r.author
    end
    users
  end

  def update_recruitment_matches_count
    recruitment.matches_count = self.class.accepted.where(recruitment: recruitment).count
    recruitment.save
  end
end
