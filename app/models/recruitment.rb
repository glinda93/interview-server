class Recruitment < ApplicationRecord
  STATUSES = {
    active: 'ACTIVE',
    inactive: 'INACTIVE'
  }.freeze
  belongs_to :job
  delegate_missing_to :job

  has_many :offers, dependent: :destroy
  has_many :matches, -> { where.not(responded_at: nil).where(response: Offer::RESPONSES[:accepted]) }, class_name: 'Offer', inverse_of: :recruitment, dependent: :destroy

  scope :offerable, -> { where(completed_at: nil).where("matches_count < number") }
  scope :recruit_at, ->(date) { where(recruit_at: date.localtime.beginning_of_day..date.localtime.end_of_day).or(where(recruit_at: nil)) }

  validates :recruit_at, presence: true, date_time: true
  validates :number, presence: true, numericality: { only_integer: true, maximum: 50 }
  validates :status, inclusion: STATUSES.values

  def completed?
    completed_at.present? && matches_count == number
  end

  def offerable?
    status_offerable? && !recruit_date_expired? && !completed?
  end

  def status_offerable?
    job && job.status = Job::STATUSES[:active] && status == STATUSES[:active]
  end

  def recruit_date_expired?
    recruit_at.beginning_of_day < Time.zone.now.beginning_of_day
  end

  def accepted_count
    Offer.where(recruitment: self).where(response: Offer::RESPONSES[:accepted]).count
  end

  def all_recruited?
    number <= accepted_count
  end
end
