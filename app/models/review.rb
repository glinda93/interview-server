class Review < ApplicationRecord
  alias_attribute :author, :reviewable_from

  belongs_to :reviewable, polymorphic: true, counter_cache: true
  belongs_to :reviewable_to, polymorphic: true
  belongs_to :reviewable_from, polymorphic: true

  validates :rating, presence: true, numericality: { in: [0, 5] }
  validates :comment, length: { maximum: 500 }

  after_create :update_offer_visible_on_create
  after_destroy :update_offer_visible_on_destroy
  after_destroy :broadcast_update_avg_rating_on_destroy
  after_save :update_recruitment_completed_at
  after_save :broadcast_update_avg_rating_on_save

  def attributes_for(user)
    return attributes_for_employer if user.instance_of?(Employer)
    return attributes_for_employee if user.instance_of?(Employee)

    attributes
  end

  def attributes_for_employee
    attributes.merge({
                       skills: reviewable.recruitment.skills
                     })
  end

  def attributes_for_employer
    attributes.merge({
                       age_group: employee.present? ? employee.age_group : nil,
                       gender_id: employee.gender_id
                     })
  end

  def employer
    return reviewable_to if reviewable_to.instance_of?(Employer)

    reviewable_from if reviewable_from.instance_of?(Employer)
  end

  def employee
    return reviewable_to if reviewable_to.instance_of?(Employee)

    reviewable_from if reviewable_from.instance_of?(Employee)
  end

  def update_recruitment_completed_at
    return unless reviewable.is_a? Offer
    return if reviewable.recruitment.blank?

    recruitment = reviewable.recruitment

    return if recruitment.completed?

    review_completed_count = Offer.accepted.where(recruitment: recruitment).where(reviews_count: 2).count
    return unless review_completed_count == recruitment.number

    recruitment.completed_at = Time.now.utc
    recruitment.save!
  end

  def update_offer_visible_on_create
    return unless reviewable.is_a? Offer

    offer = reviewable

    if author == offer.offerable_from && offer.visible_to_offerable_from
      offer.visible_to_offerable_from = false
      offer.save
    elsif author == offer.offerable_to && offer.visible_to_offerable_to
      offer.visible_to_offerable_to = false
      offer.save
    end
  end

  def update_offer_visible_on_destroy
    return unless reviewable.is_a? Offer

    offer = reviewable

    if author == offer.offerable_from && !offer.visible_to_offerable_from
      offer.visible_to_offerable_from = true
      offer.save
    elsif author == offer.offerable_to && !offer.visible_to_offerable_to
      offer.visible_to_offerable_to = true
      offer.save
    end
  end

  def broadcast_update_avg_rating_on_save
    Reviews::NotifyAvgRatingJob.perform_later reviewable_to if saved_change_to_rating?
  end

  def broadcast_update_avg_rating_on_destroy
    Reviews::NotifyAvgRatingJob.perform_later reviewable_to
  end
end
