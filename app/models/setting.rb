class Setting < ApplicationRecord
  class << self
    def get_value(key)
      row = find_by key: key
      return nil if row.blank?

      row.value
    end

    def method_missing(name) # rubocop:disable Style/MissingRespondToMissing
      get_value name
    end
  end
end
