class Skill < ApplicationRecord
  include ActsAsConstant

  has_and_belongs_to_many :employees # rubocop:disable Rails/HasAndBelongsToMany
  has_and_belongs_to_many :jobs # rubocop:disable Rails/HasAndBelongsToMany
end
