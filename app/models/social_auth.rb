class SocialAuth < ApplicationRecord
  belongs_to :user, polymorphic: true
end
