class Subscription < ApplicationRecord
  PAYMENT_GATEWAYS = {
    stripe: 'Stripe'
  }.freeze

  belongs_to :subscriber, polymorphic: true

  before_destroy :unsubscribe

  def suspend
    unsubscribe skip_customer: true
    data['subscription_id'] = nil
    self.suspended_at = Time.zone.now
    save!
  end

  def suspended?
    suspended_at.present?
  end

  def unsubscribe(skip_customer: false)
    Stripe::Subscription.delete data["subscription_id"] if data["subscription_id"].present?
    Stripe::Customer.delete data["customer_id"] if data["customer_id"].present? && !skip_customer
  rescue StandardError => e
    logger.tagged("Employer", "Subscription") do
      logger.info("Subscription cancellation failed for #{inspect}")
      logger.error e.message
      logger.debug e.backtrace.join "\n"
    end
  end
end
