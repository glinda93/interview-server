class Wish < ApplicationRecord
  belongs_to :wishable_to, polymorphic: true
  belongs_to :wishable_from, polymorphic: true

  after_create :invalidate_cache
  after_destroy :invalidate_cache

  def invalidate_cache
    cache_key = "#{wishable_from_type.capitalize}/#{wishable_from_id}/#{self.class.name.pluralize}/#{wishable_to_type.capitalize.pluralize}"
    Rails.cache.delete cache_key
  end
end
