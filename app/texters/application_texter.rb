class ApplicationTexter < Textris::Base
  default from: "インタビュー <#{Rails.application.config.setting.twilio[:phone_number]}>"
  default template_path: "texters"

  def e164(phone)
    return phone if phone.starts_with? '+'

    unless Rails.env.test?
      phone = phone[1..] if phone.starts_with?('0')
      phone = "81#{phone}"
    end
    "+#{phone}"
  end
end
