class PasswordTexter < ApplicationTexter
  def password_instructions(employee)
    @params = employee
    text to: e164(employee.phone)
  end
end
