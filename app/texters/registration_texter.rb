class RegistrationTexter < ApplicationTexter
  def confirmation_instructions(employee)
    @params = employee
    phone = if !employee.confirmed? && employee.unconfirmed_phone.present?
              employee.unconfirmed_phone
            else
              employee.phone
            end
    raise ApplicationError, I18n.t('message.employee.phone_empty') if phone.blank?

    phone_e164 = e164(phone)
    Rails.logger.info("Send confirmation instruction to #{phone_e164}")
    begin
      text to: phone_e164
    rescue ArgumentError => e
      raise e unless Rails.env.production?

      raise ApplicationError, I18n.t('message.employee.invalid_phone')
    end
  end
end
