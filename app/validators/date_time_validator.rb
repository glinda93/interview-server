class DateTimeValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    record.errors.add(attribute, :invalid) if record.public_send("#{attribute}_before_type_cast").present? && value.blank?
  end
end
