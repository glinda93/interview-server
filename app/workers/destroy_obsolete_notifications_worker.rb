class DestroyObsoleteNotificationsWorker
  include Sidekiq::Worker
  sidekiq_options retry: 3

  def perform(*_args)
    Rpush::Notification.where("created_at <= ?", 24.hours.ago).delete_all
  end
end
