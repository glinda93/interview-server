class DestroyObsoleteOffersWorker
  include Sidekiq::Worker
  sidekiq_options retry: 3

  def perform(*_args)
    offers = Offer.obsolete.all
    offers.each(&:destroy)
  end
end
