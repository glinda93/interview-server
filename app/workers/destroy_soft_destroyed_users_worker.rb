class DestroySoftDestroyedUsersWorker
  include Sidekiq::Worker
  sidekiq_options retry: 3

  def perform(*_args)
    deleting_users = Employee.soft_destroy_expired.all + Employer.soft_destroy_expired.all
    deleting_users.each(&:destroy)
  end
end
