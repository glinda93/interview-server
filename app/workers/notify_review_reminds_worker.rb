class NotifyReviewRemindsWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(*_args)
    Offer.review_missing_remind.where(reminded_at: nil).each do |offer|
      users = offer.users_did_not_review
      users.each do |u|
        name = if u.eql? offer.offerable_from
                 offer.offerable_to_display_name
               else
                 offer.offerable_from_display_name
               end
        PushNotification::Service.create(user: u, notification: {
                                           body: I18n.t('push_notification.review_remind', name: name, locale: :ja)
                                         })
      end
      offer.reminded_at = Time.now.utc
      offer.save
      PushNotificationJob.perform_later
    end
  end
end
