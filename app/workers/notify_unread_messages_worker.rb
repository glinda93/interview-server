class NotifyUnreadMessagesWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(*_args)
    users = ChatUnreadMessage.all_users

    users.each do |user|
      chat_unread_messages = ChatUnreadMessage.for_notification user
      messages = chat_unread_messages.limit(10).all.map(&:chat_message)
      continue unless messages.count

      begin
        InboxMailer.with(user: user, messages: messages).unread_messages_email.deliver_now
      rescue StandardError => e
        raise e if Rails.env.test?

        Rails.logger.error e.to_s
      end
      chat_unread_messages.destroy_all
    end
  end
end
