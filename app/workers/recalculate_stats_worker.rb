class RecalculateStatsWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(*_args)
    Stats::Employees.recalculate
    Stats::Employers.recalculate
  end
end
