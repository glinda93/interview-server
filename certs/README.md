1. Download push notification certificates, `PushEmployee.cer` and `PushEmployer.cer`

2. Convert `cer` files to `p12` files using Keychain Access

3. Use the following commands for both certificates:

```shell
openssl x509 -in aps.cer -inform der -out aps.pem 
openssl pkcs12 -nocerts -out key.pem -in key.p12
cat key.pem aps.pem > final_cert.pem
```

Reference: https://riptutorial.com/ios/example/20423/generating-a--pem-certificate-from-your--cer-file--to-pass-on-to-the-server-developer