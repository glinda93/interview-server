require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

Dotenv::Railtie.load

module InterviewServer
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.1
    config.autoloader = :classic

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    config.time_zone = "Asia/Tokyo"
    config.eager_load_paths << Rails.root.join('lib')
    config.middleware.use ActionDispatch::Flash
    config.autoload_paths << Rails.root.join('lib')
    config.i18n.default_locale = :ja
    config.setting = Rails.application.config_for(:setting)
    config.textris_delivery_method = [:twilio, :log]
    config.log_tags = [->(_r) { DateTime.now }]
  end
end
