# frozen_string_literal: true

AppleAuth.configure do |config|
  config.apple_client_id = ENV.fetch('BUNDLE_ID_EMPLOYEE', '')
  config.apple_private_key = File.read(ENV.fetch('APPLE_SIGN_IN_PRIVATE_KEY'))
  config.apple_key_id = ENV.fetch('APPLE_SIGN_IN_KEY_ID')
  config.apple_team_id = ENV.fetch('APPLE_SIGN_IN_TEAM_ID')
  config.redirect_uri = ENV.fetch('APPLE_SIGN_IN_REDIRECT_URI', '')
end
