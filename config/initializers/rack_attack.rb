module Rack
  class Attack
    throttle('req/ip', limit: 999, period: 5.minutes, &:ip)

    throttle('sign_in/ip', limit: 5, period: 20.seconds) do |req|
      req.ip if req.path.ends_with?('sign_in') && req.post?
    end

    throttle('confirmation/confirmation_token', limit: 5, period: 20.seconds) do |req|
      req.params['confirmation_token'].to_s.downcase.gsub(/\s+/, "").presence if req.path.ends_with?('confirmation') && req.get?
    end

    throttle('confirmation/send_confirmation_instructions', limit: 3, period: 20.seconds) do |req|
      req.ip if req.path.ends_with?('send_confirmation_token') && req.post?
    end

    throttle('password', limit: 5, period: 20.seconds) do |req|
      req.ip if req.path.ends_with?('password')
    end
  end
end
