require 'sidekiq/web'
require 'sidekiq/cron/web'

sidekiq_redis_config = {
  url: ENV.fetch('REDIS_URL', 'redis://localhost:6379/0')
}

sidekiq_redis_config[:password] = ENV.fetch('REDIS_PASSWORD') if ENV.fetch('REDIS_PASSWORD', '').present?

Sidekiq.configure_server do |config|
  config.redis = sidekiq_redis_config
end

Sidekiq.configure_client do |config|
  config.redis = sidekiq_redis_config
end

schedule_file = "config/schedule.yml"

Sidekiq::Cron::Job.load_from_hash! YAML.load_file(schedule_file) if File.exist?(schedule_file)

Sidekiq::Web.use ActionDispatch::Cookies
Sidekiq::Web.use ActionDispatch::Session::CookieStore, key: "_interslice_session"

Sidekiq::Web.use(Rack::Auth::Basic) do |user, password|
  Rack::Utils.secure_compare(::Digest::SHA256.hexdigest(user), ::Digest::SHA256.hexdigest(ENV.fetch('SIDEKIQ_DASHBOARD_USER', 'sidekiqadmin'))) &
    Rack::Utils.secure_compare(::Digest::SHA256.hexdigest(password), ::Digest::SHA256.hexdigest(ENV.fetch('SIDEKIQ_DASHBOARD_PASSWORD', 'superlongpassword')))
end
