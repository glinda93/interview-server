Twilio.configure do |config|
  config.account_sid = Rails.application.config.setting.twilio[:account_sid]
  config.auth_token = Rails.application.config.setting.twilio[:auth_token]
end
