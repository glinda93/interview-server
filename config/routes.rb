Rails.application.routes.draw do
  default_url_options host: ENV['HOST'], port: ENV['PORT']

  mount ActionCable.server => "/cable"

  scope "admin" do
    mount Sidekiq::Web => "/sidekiq"
  end

  scope "api/v#{ENV['API_VERSION']}", defaults: { format: :json } do
    get '/', to: 'application#index'

    devise_for :employers,
               defaults: { format: :json },
               controllers: {
                 confirmations: 'employers/confirmations',
                 sessions: 'employers/sessions',
                 registrations: 'employers/registrations',
                 passwords: 'employers/passwords'
               }
    devise_for :employees, defaults: { format: :json }, controllers: { passwords: 'employees/passwords' }
    devise_for :admins, defaults: { format: :json }, skip: [:registrations],
                        controllers: { sessions: 'admins/sessions', passwords: 'admins/passwords' }

    devise_scope :employer do
      put 'employers/resubscribe', to: 'employers/sessions#resubscribe'
    end

    devise_scope :admin do
      post 'admins/otp_verify', to: 'admins/sessions#otp_verify'
    end

    post 'oauth/apple', to: 'oauth#apple_sign_in'
    post 'oauth/twitter', to: 'oauth#twitter_sign_in'
    post 'oauth/facebook', to: 'oauth#facebook_sign_in'

    namespace :commons do
      resources :constants, only: :index
      resources :inquiries, only: :create
      get 'settings/terms', to: 'settings#terms'
      get 'settings/privacy_policy', to: 'settings#privacy_policy'
      get 'settings/gdpr', to: 'gdpr#index'
    end

    namespace :employers do
      get 'me', to: 'employers#me'
      post 'password/otp_verify', to: 'passwords#otp_verify'
      delete 'delete', to: 'employers#delete'
      put 'profiles', to: 'profiles#update'
      put 'profiles/update_pn_enabled', to: 'profiles#update_pn_enabled'
      resources :jobs, only: [:index]
      put 'jobs', to: 'jobs#update'
      put 'jobs/update_images', to: 'jobs#update_images'
      put 'jobs/update_recruitments', to: 'jobs#update_recruitments'
      get 'employees', to: 'employees#index'
      get 'employees/:id', to: 'employees#show'
      get 'employees/:id/reviews', to: 'employees#reviews'
      get 'wishes', to: 'wishes#index'
      post 'wishes', to: 'wishes#create'
      delete 'wishes', to: 'wishes#destroy'
      resources :offers, only: [:index, :show, :create, :destroy]
      post 'offers/:id/accept', to: 'offers#accept'
      post 'offers/:id/decline', to: 'offers#decline'
      get 'chats/message', to: 'chats#message'
      post 'chats/upload_image', to: 'chats#upload_image'
      resources :chats, only: [:index, :show, :create]
      resources :reviews, only: [:index, :create]
      resources :subscriptions, only: [:index, :create]
      put 'subscriptions/update_card', to: 'subscriptions#update_card'
      get 'settings/intro', to: 'settings#intro'
      get 'settings/faq', to: 'settings#faq'
    end

    namespace :employees do
      get 'me', to: 'employees#me'
      delete 'delete', to: 'employees#delete'
      get 'confirmation', to: 'confirmations#show'
      post 'confirmation', to: 'confirmations#create'
      post 'password', to: 'passwords#create'
      put 'password', to: 'passwords#update'
      post 'password/otp_verify', to: 'passwords#otp_verify'
      put 'profiles', to: 'profiles#update'
      put 'profiles/update_avatar', to: 'profiles#update_avatar'
      put 'profiles/update_pn_enabled', to: 'profiles#update_pn_enabled'
      get 'jobs', to: 'jobs#index'
      get 'jobs/:id', to: 'jobs#show'
      get 'wishes', to: 'wishes#index'
      post 'wishes', to: 'wishes#create'
      delete 'wishes', to: 'wishes#destroy'
      resources :offers, only: [:index, :create, :destroy]
      post 'offers/:id/accept', to: 'offers#accept'
      post 'offers/:id/decline', to: 'offers#decline'
      get 'chats/message', to: 'chats#message'
      post 'chats/upload_image', to: 'chats#upload_image'
      resources :chats, only: [:index, :show, :create]
      resources :reviews, only: [:index, :create]
      get 'settings/intro', to: 'settings#intro'
      get 'settings/faq', to: 'settings#faq'
    end

    namespace :admins do
      get 'me', to: 'admins#me'
      post 'verify_password', to: 'admins#verify_password'
      post 'change_email', to: 'admins#change_email'
      post 'verify_unconfirmed_email', to: 'admins#verify_unconfirmed_email'
      post 'change_password', to: 'admins#change_password'
      resources :employees, only: [:index]
      resources :employers, only: [:index]
      resources :settings, only: [:index]
      put 'settings', to: 'settings#update'
      get 'stats/employees', to: 'stats#employees'
      get 'stats/employers', to: 'stats#employers'
      get 'stats/report', to: 'stats#report'
    end
  end
end
