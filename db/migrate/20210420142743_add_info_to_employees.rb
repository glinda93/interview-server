class AddInfoToEmployees < ActiveRecord::Migration[6.1]
  def up
    change_table :employees do |t|
      t.belongs_to :gender, default: 1, after: :email
      t.string :phone, limit: 20, after: :email
      t.string :unconfirmed_phone, limit: 20, after: :email
      t.string :first_name, limit: 30, after: :email
      t.string :last_name, limit: 30, after: :email
      t.string :first_name_kana, limit: 30, after: :email
      t.string :last_name_kana, limit: 30, after: :email
      t.datetime :birthday, after: :email
      t.string :pref, limit: 100, after: :email
      t.string :city, limit: 100, after: :email
      t.text :profile, after: :email
      t.string :address, limit: 191, after: :email
      t.string :profile_pref, limit: 100, after: :email
      t.string :profile_city, limit: 100, after: :email
      t.datetime :profile_completed_at, null: true, after: :email
      t.boolean :pn_enabled, default: false, after: :email
      t.datetime :deleted_at, after: :updated_at
    end

    add_index :employees, :phone, unique: true
  end

  def down
    change_table :employees do |t|
      t.remove :first_name, :last_name, :first_name_kana, :last_name_kana, :birthday, :gender_id, :pref, :city, :profile, :pn_enabled, :deleted_at
    end
  end
end
