class AddInfoToEmployers < ActiveRecord::Migration[6.1]
  def up
    change_table :employers do |t|
      t.string :company_name, limit: 191, after: :email
      t.string :pref, limit: 100, after: :email
      t.string :city, limit: 100, after: :email
      t.string :address, limit: 191, after: :email
      t.string :phone, limit: 20, after: :email
      t.string :company_email, limit: 50, after: :email
      t.string :desc_manager, limit: 100, after: :email
      t.boolean :pn_enabled, default: false, after: :email
      t.datetime :deleted_at, after: :updated_at
    end
  end
  def down
    change_table :employers do |t|
      t.remove :company_name, :pref, :city, :address, :phone, :company_email, :pn_enabled, :deleted_at
    end
  end
end
