class CreatePriceTypes < ActiveRecord::Migration[6.1]
  def change
    create_table :price_types do |t|
      t.string :name, null: false
      t.integer :rank, null: false, default: 1
      t.timestamps
    end
  end
end
