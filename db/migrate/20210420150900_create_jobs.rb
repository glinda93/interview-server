class CreateJobs < ActiveRecord::Migration[6.1]
  def change
    create_table :jobs do |t|
      t.belongs_to :employer
      t.string :company_name, null: false, limit: 191
      t.string :pref, limit: 100
      t.string :city, limit: 100
      t.string :address, limit: 191
      t.string :phone, limit: 20
      t.string :company_email, limit: 50
      t.text :desc_main
      t.text :desc_skill
      t.text :desc_extra
      t.text :desc_payment
      t.text :desc_employer
      t.integer :price, null: false
      t.belongs_to :price_type
      t.belongs_to :payment_method
      t.string :work_hour_from, null: false, default: "00:00", limit: 20
      t.string :work_hour_to, null: false, default: "24:00", limit: 20
      t.boolean :incl_commuting_fee, default: false
      t.boolean :child_allowed, default: false
      t.string :status, limit: 10, default: 'ACTIVE'
      t.timestamps
    end
  end
end
