class CreateJobSkills < ActiveRecord::Migration[6.1]
  def change
    create_table :job_skills do |t|
      t.belongs_to :job
      t.belongs_to :skill
      t.timestamps
    end
  end
end
