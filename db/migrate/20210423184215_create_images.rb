class CreateImages < ActiveRecord::Migration[6.1]
  def change
    create_table :images do |t|
      t.belongs_to :job
      t.integer :rank, null: false, default: 1
      t.timestamps
    end
  end
end
