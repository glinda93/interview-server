class CreateWishes < ActiveRecord::Migration[6.1]
  def change
    create_table :wishes do |t|
      t.references :wishable_from, polymorphic: true
      t.references :wishable_to, polymorphic: true
    end
  end
end
