class CreateOffers < ActiveRecord::Migration[6.1]
  def change
    create_table :offers do |t|
      t.belongs_to :recruitment
      t.references :offerable_from, polymorphic: true
      t.references :offerable_to, polymorphic: true
      t.datetime :responded_at, null: true
      t.string :response, null: true
      t.integer :reviews_count, default: 0
      t.timestamps
    end
  end
end
