class CreateChatRooms < ActiveRecord::Migration[6.1]
  def change
    create_table :chat_rooms do |t|
      t.datetime :last_message_at, null: true
      t.timestamps
    end
  end
end
