class CreateChatRoomInfos < ActiveRecord::Migration[6.1]
  def change
    create_table :chat_room_infos do |t|
      t.belongs_to :chat_room
      t.references :chattable, polymorphic: true
      t.datetime :last_read_at
      t.timestamps
    end
  end
end
