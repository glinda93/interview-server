class CreateChatMessages < ActiveRecord::Migration[6.1]
  def change
    create_table :chat_messages do |t|
      t.belongs_to :chat_room
      t.references :authorable, polymorphic: true
      t.text :message, null: true
      t.timestamps
    end
  end
end
