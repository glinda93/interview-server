class CreateReviews < ActiveRecord::Migration[6.1]
  def change
    create_table :reviews do |t|
      t.references :reviewable, polymorphic: true
      t.references :reviewable_from, polymorphic: true
      t.references :reviewable_to, polymorphic: true
      t.text :comment
      t.float :rating, default: 0
      t.timestamps
    end
  end
end
