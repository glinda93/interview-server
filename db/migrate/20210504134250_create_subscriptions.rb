class CreateSubscriptions < ActiveRecord::Migration[6.1]
  def change
    create_table :subscriptions do |t|
      t.references :subscriber, polymorphic: true
      t.string :payment_gateway, null: false, default: 'Stripe'
      t.json :data
      t.datetime :trial_ends_at
      t.datetime :suspended_at
      t.timestamps
    end
  end
end
