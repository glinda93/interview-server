class CreateDevices < ActiveRecord::Migration[6.1]
  def change
    create_table :devices do |t|
      t.belongs_to :owner, polymorphic: true
      t.string :uuid
      t.string :platform, limit: 30
      t.string :model
      t.string :token
      t.timestamps
    end
  end
end
