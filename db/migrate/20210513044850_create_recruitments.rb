class CreateRecruitments < ActiveRecord::Migration[6.1]
  def change
    create_table :recruitments do |t|
      t.belongs_to :job
      t.datetime :recruit_at
      t.integer :matches_count, default: 0
      t.integer :number
      t.datetime :completed_at
      t.string :status, limit: 30
      t.timestamps
    end
  end
end
