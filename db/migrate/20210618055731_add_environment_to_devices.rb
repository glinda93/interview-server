class AddEnvironmentToDevices < ActiveRecord::Migration[6.1]
  def up
    add_column :devices, :environment, :string, default: 'production'
  end

  def down
    remove_column :devices, :environment
  end
end
