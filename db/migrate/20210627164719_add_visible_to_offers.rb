class AddVisibleToOffers < ActiveRecord::Migration[6.1]
  def up
    add_column :offers, :visible_to_offerable_to, :boolean, default: true
    add_column :offers, :visible_to_offerable_from, :boolean, default: true
  end

  def down
    remove_column :offers, :visible_to_offerable_to
    remove_column :offers, :visible_to_offerable_from
  end
end
