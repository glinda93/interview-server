class AddRemindedAtToOffers < ActiveRecord::Migration[6.1]
  def up
    add_column :offers, :reminded_at, :datetime, null: true
  end

  def down
    remove_column :offers, :reminded_at
  end
end
