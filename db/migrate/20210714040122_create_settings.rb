class CreateSettings < ActiveRecord::Migration[6.1]
  def change
    create_table :settings do |t|
      t.string :key, null: false
      t.text :value, limit: (16.megabytes - 1)
      t.timestamps

      t.index [:key], unique: true
    end
  end
end
