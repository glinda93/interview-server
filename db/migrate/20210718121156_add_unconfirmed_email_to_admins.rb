class AddUnconfirmedEmailToAdmins < ActiveRecord::Migration[6.1]
  def up
    add_column :admins, :unconfirmed_email, :string, null: true, after: :email
  end

  def down
    remove_column :admins, :unconfirmed_email
  end
end
