class CreateChatUnreadMessages < ActiveRecord::Migration[6.1]
  def change
    create_table :chat_unread_messages do |t|
      t.belongs_to :user, polymorphic: true
      t.belongs_to :chat_room
      t.belongs_to :chat_message
      t.timestamps
    end
  end
end
