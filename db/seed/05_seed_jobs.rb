if Rails.env.development?
  employer = Employer.new(
    company_name: '株式会社TvT',
    email: 'no-reply-interview@tvt.co.jp',
    pref: '北海道',
    city: '札幌市',
    address: 'ヤマガタケン, ヒガシネシ, ロクタ, 308-1289',
    phone: '81671607229',
    company_email: 'info@tvt.co.jp',
    password: '11111111',
    password_confirmation: '11111111',
    confirmed_at: DateTime.now.utc
  )

  employer.save!

  job = Job.new(
    employer: employer,
    company_name: '株式会社TvT',
    pref: '北海道',
    city: '札幌市',
    address: 'ヤマガタケン, ヒガシネシ, ロクタ, 308-1289',
    phone: '81671607229',
    company_email: 'info@tvt.co.jp',
    price_type: PriceType.find(1),
    payment_method: PaymentMethod.find(1),
    price: 3000,
    desc_main: '仕事の内容：テキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト',
    desc_skill: '歓迎するスキル：テキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト',
    desc_extra: 'その他特記事項・PR：テキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト',
    desc_payment: 'その他特記事項：テキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト',
    incl_commuting_fee: true,
    child_allowed: true,
    work_hour_from: '08:00',
    work_hour_to: '20:00',
    status: Job::STATUSES[:active],
    images: [
      Image.new(
        job: job,
        rank: 1,
        file: Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/image.png'), 'image/png')
      )
    ],
    skills: [
      Skill.find(1),
      Skill.find(2),
      Skill.find(3),
      Skill.find(4),
      Skill.find(5)
    ]
  )

  job.save!

  if ENV['sample']
    Seeds::SampleJobs.seed
  end

  puts 'Jobs and employers seeded'
end