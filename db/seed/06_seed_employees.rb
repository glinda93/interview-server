if Rails.env.development?
  employee = Employee.new(
    email: 'sanasieo234@gmail.com',
    pref: '北海道',
    city: '札幌市',
    password: '11111111',
    password_confirmation: '11111111',
    phone: '18303650917',
    first_name: '湊崎',
    last_name: '紗夏',
    first_name_kana: 'サナ',
    last_name_kana: 'ミナトザキ',
    birthday: '1996-12-29',
    profile: 'テキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト',
    pn_enabled: true,
    gender: Gender.find(2),
    skills: [
      Skill.find(1),
      Skill.find(2),
      Skill.find(3),
      Skill.find(4),
      Skill.find(5)
    ],
    profile_completed_at: DateTime.now.utc,
    confirmed_at: DateTime.now.utc,
    avatar: Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/image.png'), 'image/png')
  )

  employee.save!

  if ENV['sample']
    Seeds::SampleEmployees.seed
  end

  puts 'Employees seeded'
end
