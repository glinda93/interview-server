def key_exists?(key)
  if Setting.get_value(key)
    puts "setting `#{key}` already exists"
    true
  else
    false
  end
end

def load_yaml(name)
  YAML.load_file(Rails.root.join("db/fixture/settings/#{name}.yml")).with_indifferent_access[name]
end

def seed_intro
  data = load_yaml :intro
  [:employee, :employer].map do |scope|
    key = "intro_#{scope}"
    next if key_exists?(key)

    Setting.create(
      key: key,
      value: data[scope]
    )
  end
end

def seed_privacy_policy
  key = :privacy_policy
  data = load_yaml key
  return if key_exists? key

  Setting.create(
    key: key,
    value: data
  )
end

def seed_terms
  key = :terms
  data = load_yaml key
  return if key_exists? key

  Setting.create(
    key: key,
    value: data
  )
end

def seed_faq
  data = load_yaml :faq
  [:employee, :employer].map do |scope|
    key = "faq_#{scope}"
    next if key_exists?(key)

    Setting.create(
      key: key,
      value: data[scope]
    )
  end
end

puts "seeding intros..."
seed_intro

puts "seeding privacy policy..."
seed_privacy_policy

puts "seeding terms..."
seed_terms

puts "seeding faq..."
seed_faq
