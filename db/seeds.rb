def truncate_table(model)
  model.connection.truncate(model.table_name)
  puts "Truncated table `#{model.table_name}`"
end

def seed_constants(model:, data:)
  truncate_table model
  count = 0
  data.each.with_index(1) do |name, rank|
    record = model.create(name: name, rank: rank)
    record.save!
    count += 1
  end
  puts "Seeded #{count} records into `#{model.table_name}`"
end

puts 'Start seeding...'
Dir[File.join(Rails.root, 'db', 'seed', '*.rb')].sort.each { |seed| load seed }
puts 'Seed complete'
