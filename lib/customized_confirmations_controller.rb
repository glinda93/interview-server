class CustomizedConfirmationsController < Devise::ConfirmationsController
  include AuthHeader
  include ErrorHandler
  before_action :disable_legacy_flow, except: [:show, :create]

  def show
    self.resource = resource_class.confirm_by_token(params[:confirmation_token], params[:id])
    yield resource if block_given?

    if resource.errors.empty?
      set_flash_message!(:notice, :confirmed)
      set_bearer_token resource
      respond_with_navigational(resource.attributes_for_self) { redirect_to after_confirmation_path_for(resource_name, resource) }
    else
      respond_with_navigational(resource.errors.full_messages, status: :unprocessable_entity) { render :new }
    end
  end

  def disable_legacy_flow
    render nothing: true, status: :not_found
  end
end
