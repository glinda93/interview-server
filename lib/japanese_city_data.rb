class JapaneseCityData
  def self.fetch_japanese_city_data
    Rails.cache.fetch('japanese_city_data') do
      uri = URI.parse('https://raw.githubusercontent.com/glinda93/japanese-city-data/master/src/japanese-city-data.json')
      http = Net::HTTP.new(uri.host, uri.port)
      http.open_timeout = 10
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      request = Net::HTTP::Get.new(uri.to_s)
      response = http.request request
      json = response.body
      JSON.parse(json, symbolize_names: true)
    end
  end

  def self.pref_by_id(id)
    fetch_japanese_city_data.find { |p| p[:value].to_s == id.to_s }
  end

  def self.city_by_pref_and_id(pref_id, id)
    pref = pref_by_id(pref_id)
    return nil if pref.nil?
    return nil if pref[:children].nil?

    pref[:children].find { |c| c[:value].to_s == id.to_s }
  end

  def self.replace_pref_and_city(param, pref_key: :pref, city_key: :city)
    pref_id = param[pref_key]
    pref = pref_by_id(param[pref_key])
    city = city_by_pref_and_id(pref_id, param[city_key])
    param[pref_key] = pref ? pref[:label] : nil
    param[city_key] = city ? city[:label] : nil
    param
  end
end
