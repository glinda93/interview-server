module Oauth
  class Apple
    def self.get_user!(identity_token:, user_id:, email:)
      validate!(identity_token, user_id)
      {
        id: user_id,
        email: email,
        provider: 'apple'
      }
    end

    def self.validate!(identity_token, user_id)
      AppleAuth::UserIdentity.new(user_id, identity_token).validate!
    end
  end
end
