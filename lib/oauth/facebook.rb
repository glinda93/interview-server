module Oauth
  class Facebook
    def self.get_user!(access_token:)
      url = "https://graph.facebook.com/me?fields=id,first_name,last_name,email,picture&access_token=#{access_token}"
      uri = URI.parse(url)
      http = Net::HTTP.new(uri.host, uri.port)
      http.open_timeout = 5
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      request = Net::HTTP::Get.new(uri.to_s)
      response = http.request request
      response = JSON.parse response.body
      {
        id: response['id'],
        email: response['email'] || '',
        provider: 'Facebook'
      }
    end
  end
end
