module Oauth
  class Twitter
    def self.get_user!(auth_token:, auth_token_secret:)
      consumer_key = ENV.fetch('TWITTER_CONSUMER_KEY', '')
      consumer_secret = ENV.fetch('TWITTER_CONSUMER_SECRET', '')
      url = "https://api.twitter.com/1.1/account/verify_credentials.json?include_email=true"
      uri = URI.parse(url)
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      req = Net::HTTP::Get.new(uri.request_uri)
      req["Host"] = "api.twitter.com"
      oauth = {
        consumer_key: consumer_key,
        consumer_secret: consumer_secret,
        token: auth_token,
        token_secret: auth_token_secret
      }
      req["Authorization"] = SimpleOAuth::Header.new(:get, uri.to_s, {}, oauth)
      response = http.request req
      user = JSON.parse response.body
      raise ApplicationError, I18n.t('message.common.twitter_login_failed') unless user && user['id'].present?

      {
        id: user['id'],
        email: user['email'] || '',
        provider: 'Twitter'
      }
    end
  end
end
