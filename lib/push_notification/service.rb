module PushNotification
  class Service
    DEFAULT_NOTIFICATION = {
      title: I18n.t('push_notification.title', locale: :ja),
      body: '',
      sound: 'default'
    }.freeze
    class << self
      def create(user:, notification:)
        notifications = []
        return notifications unless notification_enabled?(user)

        notification = DEFAULT_NOTIFICATION.merge notification
        android_ids = user.android_tokens
        notifications.push create_gcm_notification registration_ids: android_ids, notification: notification if android_ids.present?

        ios_devices = user.ios_devices
        ios_devices.each do |device|
          token = device.token
          n = create_apn_notification device_token: token,
                                      notification: notification,
                                      scope: user.class.name.underscore,
                                      environment: device.environment
          notifications.push n if n.present?
        end
        notifications
      end

      def create_gcm_notification(registration_ids:, notification:)
        n = Rpush::Gcm::Notification.new
        n.app = Rpush::Gcm::App.find_by(name: android_app_name)
        n.registration_ids = registration_ids
        n.notification = notification
        if Rails.env.production?
          n.save
        else
          n.save!
        end
        n
      end

      def create_apn_notification(device_token:, notification:, scope:, environment:)
        api = config[:apn][:api]
        n = if api == 'apns2'
              Rpush::Apns2::Notification.new
            else
              Rpush::Apnsp8::Notification.new
            end
        n.app = if api == 'apns2'
                  Rpush::Apns2::App.find_by(name: ios_app_name(scope), environment: environment)
                else
                  Rpush::Apnsp8::App.find_by(name: ios_app_name(scope), environment: environment)
                end
        n.device_token = device_token
        n.alert = {
          title: notification[:title],
          body: notification[:body]
        }
        if api == 'apns2'
          n.data = {
            headers: { 'apns-topic': Rails.application.config.setting.rpush[:apn][:bundle_id][scope.to_sym] }
          }
        end
        n.save!
        n
      end

      def flush
        Rpush.push
      end

      def notification_enabled?(user)
        user&.devices&.count && user.pn_enabled
      end

      def android_app_name
        "#{Rails.application.config.setting.rpush[:app_name_prefix]}_android"
      end

      def ios_app_name(scope)
        "#{Rails.application.config.setting.rpush[:app_name_prefix]}_ios_#{scope}"
      end

      def create_android_app
        return if Rpush::Gcm::App.find_by(name: android_app_name).present?

        app = Rpush::Gcm::App.new
        app.name = android_app_name
        app.auth_key = config[:fcm][:server_key]
        app.connections = 1
        app.save!
        app
      end

      def create_ios_apps
        scopes = [:employee, :employer]
        environments = [:development, :production]
        config = Rails.application.config.setting.rpush
        scopes.each do |scope|
          environments.each do |env|
            create_apnsp8_app(config, scope, env) if config[:apn][:api] == 'apnsp8'
            create_apns2_app(config, scope, env) if config[:apn][:api] == 'apns2'
          end
        end
      end

      def create_apns2_app(config, scope, environment)
        app_name = ios_app_name scope
        return if Rpush::Apns2::App.find_by(name: app_name, environment: environment)

        app = Rpush::Apns2::App.new
        app.name = app_name
        app.certificate = File.read(config[:apn][:apns2][:cert][scope])
        app.environment = config[:apn][:environment]
        app.password = config[:apn][:apns2][:password]
        app.bundle_id = config[:apn][:bundle_id][scope]
        app.connections = 1
        app.environment = environment
        app.save!
        app
      end

      def create_apnsp8_app(config, scope, environment)
        app_name = ios_app_name scope
        return if Rpush::Apnsp8::App.find_by(name: app_name, environment: environment)

        app = Rpush::Apnsp8::App.new
        app.name = app_name
        app.apn_key = File.read(config[:apn][:apnsp8][:key])
        app.environment = environment
        app.apn_key_id = config[:apn][:apnsp8][:key_id]
        app.team_id = config[:apn][:apnsp8][:team_id]
        app.bundle_id = config[:apn][:bundle_id][scope]
        app.connections = 1
        app.save!
        app
      end

      def config
        Rails.application.config.setting.rpush
      end
    end
  end
end
