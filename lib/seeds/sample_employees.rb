module Seeds
  class SampleEmployees
    def self.seed
      FactoryBot.create_list(:employee, 25)
    end
  end
end
