module Seeds
  class SampleJobs
    def self.seed
      FactoryBot.create_list(:job, 25)
    end
  end
end
