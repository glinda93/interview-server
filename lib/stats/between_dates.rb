module Stats
  module BetweenDates
    extend ActiveSupport::Concern

    class_methods do
      attr_accessor :from, :to

      def scope
        if from.present? && to.present?
          default_scope.where(created_at: from..to)
        elsif from.present?
          default_scope.where("created_at >= ?", from)
        elsif to.present?
          default_scope.where("created_at < ?", to)
        else
          default_scope
        end
      end

      def set_period(from: nil, to: nil, date: nil, month: nil)
        if month.present?
          self.from = month.beginning_of_month
          self.to = month.end_of_month
        elsif date.present?
          self.from = date.beginning_of_day
          self.to = date.end_of_day
        else
          self.from = from
          self.to = to
        end
      end

      def calc_monthly(month_date)
        stats = {
          dates: []
        }
        date_cursor = month_date.beginning_of_month
        end_date = month_date.end_of_month
        while date_cursor < end_date
          stats[:dates].push({
                               date: date_cursor,
                               stats: calculate(date: date_cursor)
                             })
          date_cursor += 1.day
        end
        stats.merge({
                      month: calculate(month: month_date),
                      all_time: calculate(to: month_date.end_of_month)
                    })
      end
    end
  end
end
