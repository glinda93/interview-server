module Stats
  module Cachable
    extend ActiveSupport::Concern

    class_methods do
      def result
        Rails.cache.fetch(cache_key, expires_in: 1.day) do
          calculate
        end
      end

      def recalculate
        invalidate_cache
        result
      end

      def invalidate_cache
        Rails.cache.delete cache_key
      end

      def calc_percent(count, total)
        return 0 if total.zero?

        count.to_f / total * 100
      end
    end
  end
end
