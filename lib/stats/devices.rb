module Stats
  class Devices
    include BetweenDates

    class << self
      def default_scope
        Device.where(environment: 'production')
      end

      def calculate(from: nil, to: nil, date: nil, month: nil)
        set_period(from: from, to: to, date: date, month: month)

        stats = {
          total: scope.count
        }
        [Employee, Employer].map do |user_class|
          user_scope = user_class.name.downcase
          scope_user = scope.where(owner_type: user_class.name)
          stats[user_scope] = {
            total: scope_user.count,
            ios: scope_user.where(platform: 'ios').count,
            android: scope_user.where(platform: 'android').count
          }
        end
        stats
      end
    end
  end
end
