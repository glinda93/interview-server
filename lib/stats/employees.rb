module Stats
  class Employees
    include Cachable

    class << self
      def calculate
        {
          total: total_count,
          today_count: today_count,
          stats: {
            genders: by_genders,
            age_groups: by_age_groups,
            skills: by_skills,
            prefs: by_prefs
          }
        }.with_indifferent_access
      end

      def cache_key
        "stats/employees"
      end

      def default_scope
        Employee.where.not(profile_completed_at: nil)
                .where(deleted_at: nil)
      end

      def total_count
        default_scope.count
      end

      def today_count
        default_scope.where("created_at >= ?", Time.zone.now.beginning_of_day).count
      end

      def by_age_groups
        total = total_count.to_f
        stats = {}
        [10, 20, 30, 40, 50, 60].map do |age_group|
          count = count_by_age_group(age_group)
          stats[age_group] = {
            label: I18n.t('activerecord.attributes.employee.age_group', age_group: age_group),
            count: count,
            percent: calc_percent(count, total)
          }
        end
        stats
      end

      def by_genders
        total = total_count
        stats = {}
        genders = Gender.all
        genders.map do |gender|
          count = count_by_gender_id(gender.id)
          stats[gender.id] = {
            label: gender.name,
            count: count,
            percent: calc_percent(count, total)
          }
        end

        stats
      end

      def by_prefs
        total = total_count
        prefs = JapaneseCityData.fetch_japanese_city_data
        stats = {}
        prefs.map do |pref|
          count = count_by_pref(pref[:label])
          stats[pref[:value]] = {
            label: pref[:label],
            count: count,
            percent: calc_percent(count, total)
          }
        end

        stats
      end

      def by_skills
        total_query = <<-SQL.squish
          SELECT COUNT(*) FROM `employee_skills`
        SQL
        total = ActiveRecord::Base.connection.exec_query(total_query).rows[0][0].to_f
        skills = Skill.all
        stats = {}
        skills.map do |skill|
          count = count_by_skill(skill)
          stats[skill.id] = {
            label: skill.name,
            count: count,
            percent: calc_percent(count, total)
          }
        end

        stats
      end

      def count_by_age_group(age_group)
        age_group = (age_group / 10).to_i * 10
        age_group = 10 if age_group < 10
        age_group = 60 if age_group > 60

        from_date = age_group == 60 ? nil : (age_group + 9).years.ago
        to_date = age_group == 10 ? nil : age_group.years.ago
        if from_date.present? && to_date.present?
          default_scope.where(birthday: from_date.beginning_of_day..to_date.end_of_day).count
        elsif from_date.present? && to_date.blank?
          default_scope.where("birthday >= ?", from_date).count
        elsif from_date.blank? && to_date.present?
          default_scope.where("birthday < ?", to_date).count
        else
          default_scope.all.count
        end
      end

      def count_by_gender_id(gender_id)
        gender_id = gender_id.id if gender_id.is_a? ActiveRecord

        default_scope.where(gender_id: gender_id).count
      end

      def count_by_pref(pref)
        default_scope.where(pref: pref).count
      end

      def count_by_skill(skill)
        default_scope.joins(:skills).where(skills: { id: skill.id }).count
      end
    end
  end
end
