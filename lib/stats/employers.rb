module Stats
  class Employers
    include Cachable

    class << self
      def cache_key
        "stats/employers"
      end

      def calculate
        {
          total: total_count,
          today_count: today_count,
          stats: {
            skills: by_skills,
            price_types: by_price_types,
            prefs: by_prefs,
            incl_commuting_fee: by_incl_commuting_fee,
            child_allowed: by_child_allowed,
            payment_methods: by_payment_methods,
            subscriptions: by_subscriptions
          }
        }
      end

      def default_scope
        Employer.where.not(confirmed_at: nil)
                .where(deleted_at: nil)
                .joins(:job, :subscription)
      end

      def total_count
        default_scope.count
      end

      def today_count
        default_scope.where("`employers`.`created_at` >= ?", Time.zone.now.beginning_of_day).count
      end

      def by_skills
        total_query = <<-SQL.squish
          SELECT COUNT(*) FROM `job_skills`
        SQL
        total = ActiveRecord::Base.connection.exec_query(total_query).rows[0][0].to_f
        skills = Skill.all
        stats = {}

        skills.map do |skill|
          count = count_by_skill(skill)
          stats[skill.id] = {
            label: skill.name,
            count: count,
            percent: calc_percent(count, total)
          }
        end

        stats
      end

      def by_price_types
        price_types = PriceType.all
        total = total_count
        stats = {}

        price_types.map do |price_type|
          count = count_by_price_type price_type
          stats[price_type.id] = {
            label: price_type.name,
            count: count,
            percent: calc_percent(count, total)
          }
        end

        stats
      end

      def by_prefs
        total = total_count
        prefs = JapaneseCityData.fetch_japanese_city_data
        stats = {}
        prefs.map do |pref|
          count = count_by_pref(pref[:label])
          stats[pref[:value]] = {
            label: pref[:label],
            count: count,
            percent: calc_percent(count, total)
          }
        end

        stats
      end

      def by_incl_commuting_fee
        total = total_count
        stats = {}
        [true, false].map do |incl_commuting_fee|
          count = count_by_incl_commuting_fee(incl_commuting_fee)
          stats[incl_commuting_fee.to_s] = {
            label: incl_commuting_fee ? I18n.t('stats.employer.incl_commuting_fee_true') : I18n.t('stats.employer.incl_commuting_fee_false'),
            count: count,
            percent: calc_percent(count, total)
          }
        end

        stats
      end

      def by_child_allowed
        total = total_count
        stats = {}

        [true, false].map do |child_allowed|
          count = count_by_child_allowed(child_allowed)
          stats[child_allowed.to_s] = {
            label: child_allowed ? I18n.t('stats.employer.child_allowed_true') : I18n.t('stats.employer.child_allowed_false'),
            count: count,
            percent: calc_percent(count, total)
          }
        end

        stats
      end

      def by_payment_methods
        total = total_count
        stats = {}
        payment_methods = PaymentMethod.all

        payment_methods.map do |payment_method|
          count = count_by_payment_method(payment_method)
          stats[payment_method.id] = {
            label: payment_method.name,
            count: count,
            percent: calc_percent(count, total)
          }
        end
      end

      def by_subscriptions
        total = total_count
        stats = {}
        trial_count = default_scope.where("`subscriptions`.`trial_ends_at` >= ?", Time.zone.now).count
        paid_count = default_scope.where("`subscriptions`.`trial_ends_at` < ?", Time.zone.now).count
        stats[:trial] = {
          label: I18n.t('stats.employer.trial'),
          count: trial_count,
          percent: calc_percent(trial_count, total)
        }
        stats[:paid] = {
          label: I18n.t('stats.employer.paid'),
          count: paid_count,
          percent: calc_percent(paid_count, total)
        }
        stats
      end

      def count_by_skill(skill)
        Job.joins(:skills).where(skills: { id: skill.id }).count
      end

      def count_by_price_type(price_type)
        default_scope.where(job: { price_type_id: price_type.id }).count
      end

      def count_by_pref(pref)
        default_scope.where(job: { pref: pref }).count
      end

      def count_by_incl_commuting_fee(bool)
        default_scope.where(job: { incl_commuting_fee: bool }).count
      end

      def count_by_child_allowed(bool)
        default_scope.where(job: { child_allowed: bool }).count
      end

      def count_by_payment_method(payment_method)
        default_scope.where(job: { payment_method_id: payment_method.id }).count
      end
    end
  end
end
