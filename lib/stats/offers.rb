module Stats
  class Offers
    include BetweenDates

    class << self
      def default_scope
        Offer
      end

      def calculate(from: nil, to: nil, date: nil, month: nil)
        set_period(from: from, to: to, date: date, month: month)

        {
          total: scope.count,
          employee: scope.where(offerable_from_type: 'Employee').count,
          employer: scope.where(offerable_from_type: 'Employer').count,
          accepted: scope.accepted.count
        }
      end
    end
  end
end
