module Stats
  class Registrations
    include BetweenDates

    class << self
      attr_accessor :default_scope

      def calculate(from: nil, to: nil, date: nil, month: nil)
        set_period(from: from, to: to, date: date, month: month)
        # Employee
        self.default_scope = Employee.where.not(profile_completed_at: nil)
        stats = {}
        stats[:employee] = scope.count

        # Employer
        self.default_scope = Employer.where.not(confirmed_at: nil)
        stats[:employer] = scope.count

        stats
      end
    end
  end
end
