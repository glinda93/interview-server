module Subscriptions
  class Stripe
    class << self
      def create_subscription(stripe_customer:, backdate: nil, next_billing_at: nil)
        @customer = stripe_customer
        @backdate = backdate
        @next_billing_at = next_billing_at
        params = subscription_params
        Rails.logger.tagged("Employer", "Subscription") do
          Rails.logger.info "Creating subscription with the following params: #{params.as_json}"
        end
        ::Stripe::Subscription.create params
      end

      protected

      def subscription_params
        params = {
          customer: @customer[:id],
          items: [
            {
              price: Rails.application.config.setting.stripe[:price_id]
            }
          ]
        }
        if @backdate.present?
          params.merge({
                         backdate_start_date: @backdate.to_i,
                         billing_cycle_anchor: @backdate > Time.zone.now ? @backdate.to_i : (@backdate + 1.month).at_beginning_of_month.to_i
                       })
        else
          trial_end = billing_cycle_anchor.to_i
          params.merge({
                         trial_end: trial_end,
                         billing_cycle_anchor: trial_end
                       })
        end
      end

      def billing_cycle_anchor
        @next_billing_at.presence || (Time.now.utc + Rails.application.config.setting[:trial_periods].to_i.months).at_beginning_of_month
      end
    end
  end
end
