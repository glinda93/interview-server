require 'rails_helper'

RSpec.describe ::AppearanceChannel, type: :channel do
  let(:user) { create(:employee) }
  let(:scope) { user.class.name.underscore.to_sym }
  let(:token) { Warden::JWTAuth::UserEncoder.new.call(user, scope, nil)[0] }

  before do
    Rails.cache.clear
    stub_connection current_user: user
    subscribe
  end

  it "calls current_user#appear" do
    expect(subscription).to be_confirmed
    expect(user.online?).to eq true
  end

  context 'when unsubscribed' do
    it 'calls current_user#disappear' do
      subscription.unsubscribe_from_channel
      expect(user.online?).to eq false
    end
  end

  describe 'appear' do
    let(:appearing_on) { 'TestRoom' }

    it 'calls current_user#appear' do
      perform :appear, { appearing_on: appearing_on }
      expect(user.online?(on: appearing_on)).to eq true
    end
  end

  describe 'register_device' do
    let(:params) { { device: { platform: 'ios', model: 'iPhone 14.5', uuid: '71213991', token: '1234567890', environment: 'development' } }.with_indifferent_access }

    it 'creates or find device' do
      expect do
        perform :register_device, params
      end.to change { user.devices.count }.by 1
      expect do
        perform :register_device, params
      end.to change { user.devices.count }.by 0
    end

    it 'broadcasts to current_user' do
      assert_broadcasts user, 1 do
        perform :register_device, params
      end
    end
  end
end
