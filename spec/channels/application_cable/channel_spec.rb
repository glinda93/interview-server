require 'rails_helper'

class StreamableClass
  def stream_name
    'foo'
  end
end

class NonStreamableClass
  def to_param
    'bar'
  end
end

RSpec.describe ApplicationCable::Channel, type: :channel do
  describe 'serialize_broadcasting' do
    let(:streamable) { StreamableClass.new }
    let(:non_streamable) { NonStreamableClass.new }

    context 'when streamable is given' do
      it 'calls stream_name' do
        expect(described_class.serialize_broadcasting(streamable)).to eq 'foo'
      end
    end

    context 'when non streamable object is given' do
      it 'calls super' do
        expect(described_class.serialize_broadcasting(non_streamable)).to eq 'bar'
      end
    end
  end
end
