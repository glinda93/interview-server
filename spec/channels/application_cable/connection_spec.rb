require 'rails_helper'

RSpec.describe ApplicationCable::Connection, type: :channel do
  let(:user) { create(:employee) }
  let(:scope) { user.class.name.underscore.to_sym }
  let(:token) { Warden::JWTAuth::UserEncoder.new.call(user, scope, nil)[0] }

  context 'when token and scope is correct' do
    it 'successfully connects' do
      connect '/cable', params: { token: token, scope: scope }
      expect(connection.current_user.id).to eq user.id
    end
  end

  context 'when invalid token is given' do
    it 'rejects connection' do
      expect do
        connect '/cable', params: { token: 'wrong', scope: scope }
      end.to have_rejected_connection
    end
  end
end
