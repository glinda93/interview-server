require 'rails_helper'

RSpec.describe ChatChannel, type: :channel do
  let(:chat_room) { create(:chat_room) }
  let(:user) { chat_room.participants.first }

  before do
    Rails.cache.clear
    ActiveJob::Base.queue_adapter = :test
    stub_connection current_user: user
  end

  describe 'subscribed' do
    it 'streams from chat room name' do
      subscribe chat_room_id: chat_room.id
      assert_has_stream(described_class.broadcasting_for(chat_room))
      expect(user.online?(on: chat_room)).to be true
    end

    context 'when invalid params is given' do
      it 'rejects connection' do
        subscribe chat_room_id: 'wrong'
        expect(subscription.rejected?).to be true
      end
    end
  end

  describe 'unsubscribed' do
    before do
      subscribe chat_room_id: chat_room.id
    end

    it 'disappears user' do
      subscription.unsubscribe_from_channel
      expect(user.online?(on: chat_room)).to be false
    end
  end

  describe 'read_all' do
    let(:chat_room_info) { chat_room.chat_room_infos.where(chattable: user).first }

    before do
      subscribe chat_room_id: chat_room.id
    end

    it 'sets last_read_at' do
      expect(chat_room_info.last_read_at).to be_nil
      perform :read_all
      chat_room_info.reload
      expect(chat_room_info.last_read_at).not_to be_nil
    end
  end

  describe 'create_message' do
    let(:message) { Faker::Lorem.sentence }

    before do
      subscribe chat_room_id: chat_room.id
    end

    it 'creates a new message' do
      expect do
        perform :create_message, { message: message }
      end.to change { ChatMessage.all.count }.by 1
      chat_message = chat_room.chat_messages.where(authorable: user).first
      expect(chat_message.message).to eql message
    end

    it 'notifies new message' do
      expect do
        perform :create_message, { message: message }
      end.to have_enqueued_job PushNotificationJob
    end
  end

  describe 'delete_message' do
    let(:message) { create(:chat_message, chat_room: chat_room, authorable: user) }

    before do
      subscribe chat_room_id: chat_room.id
      message
    end

    it 'deletes a message' do
      expect do
        perform :delete_message, { id: message.id }
      end.to change(ChatMessage, :count).by(-1)
    end

    it 'notifies message deleted' do
      expect do
        perform :create_message, { message: message }
      end.to have_enqueued_job PushNotificationJob
    end
  end
end
