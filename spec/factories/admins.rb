FactoryBot.define do
  factory :admin do
    email { Faker::Internet.unique.email }
    password { 'superlongpassword' }
    password_confirmation { password }
  end
end
