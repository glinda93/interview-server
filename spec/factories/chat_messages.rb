FactoryBot.define do
  factory :chat_message do
    chat_room
    authorable { chat_room.participants.first }
    message { Faker::Lorem.sentence }
  end
end
