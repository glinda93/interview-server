FactoryBot.define do
  factory :chat_room_info do
    chat_room
    chattable factory: :employee
    last_read_at { DateTime.now.utc }

    trait :for_employer do
      chattable factory: :employer
    end

    trait :not_read do
      last_read_at { nil }
    end
  end
end
