FactoryBot.define do
  factory :chat_room do
    transient do
      participants { [create(:employee), create(:employer, :with_job)] }
    end

    after(:create) do |obj, evaluator|
      evaluator.participants.each do |user|
        create(:chat_room_info, :not_read, chat_room: obj, chattable: user)
      end
    end
  end
end
