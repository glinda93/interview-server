FactoryBot.define do
  factory :device do
    owner factory: :employee
    uuid { Faker::Number.unique.between(from: 1_000_000_000, to: 9_999_999_999).to_s }
    platform { 'ios' }
    model { 'iPhone 14.5' }
    token { Faker::Lorem.unique.characters(number: 40) }

    trait :ios do
      platform { 'ios' }
      model { 'iPhone 14.5' }
    end

    trait :android do
      platform { 'android' }
      model { 'goldfish' }
    end
  end
end
