FactoryBot.define do
  factory :employee do
    email { Faker::Internet.unique.email }
    phone { Faker::PhoneNumber.cell_phone_in_e164 }
    unconfirmed_phone { nil }
    pref { JapaneseCityData.pref_by_id(pref_id)[:label]  }
    city { JapaneseCityData.city_by_pref_and_id(pref_id, city_id)[:label] }
    address { Faker::Address.street_address }
    profile_pref { pref }
    profile_city { city }
    password { '11111111' }
    password_confirmation { '11111111' }
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    first_name_kana { 'テキスト' }
    last_name_kana { 'テキスト' }
    birthday { Faker::Date.between(from: "1970-01-01", to: "2003-01-01") }
    gender { Gender.find([1, 2].sample) }
    avatar { Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/image.png'), 'image/png') }
    profile { Faker::Lorem.sentence }
    profile_completed_at { DateTime.now.utc }
    pn_enabled { true }
    confirmed_at { Time.now.utc }
    confirmation_token { nil }
    deleted_at { nil }

    trait :confirmed do
      confirmed_at { Time.now.utc }
    end

    trait :unconfirmed do
      confirmed_at { nil }
    end

    trait :soft_destroyed do
      transient do
        soft_destroyed? { true }
      end
    end

    trait :no_device do
      transient do
        devices_count { 0 }
      end
    end

    transient do
      skills_count { Faker::Number.between(from: 1, to: 5) }
      pref_id { Faker::Number.between(from: 0, to: 1) }
      city_id { Faker::Number.between(from: 0, to: 1) }
      with_social_auth { true }
      devices_count { Faker::Number.between(from: 1, to: 3) }
    end

    after(:build) do |employee, evaluator|
      employee.skills = Array.new(evaluator.skills_count).map.with_index do |_i, index|
        Skill.find(index + 1)
      end
    end

    after(:build) do |obj, evaluator|
      obj.social_auth = build(:social_auth, user: obj) if evaluator.with_social_auth
    end

    after(:build) do |obj, evaluator|
      obj.devices = Array.new(evaluator.devices_count).map do
        build(:device, owner: obj)
      end
    end

    after(:create) do |obj, evaluator|
      obj.soft_destroy if evaluator.soft_destroyed?
      obj.deleted_at = evaluator.deleted_at if evaluator.deleted_at.present?
      obj.save
    end
  end
end
