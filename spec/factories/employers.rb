FactoryBot.define do
  factory :employer do
    email { Faker::Internet.unique.email }
    company_name { "#{Faker::Lorem.word}会社" }
    pref { JapaneseCityData.pref_by_id(pref_id)[:label]  }
    city { JapaneseCityData.city_by_pref_and_id(pref_id, city_id)[:label] }
    address { Faker::Address.street_address }
    phone { Faker::PhoneNumber.cell_phone_in_e164.delete('+') }
    company_email { Faker::Internet.email }
    desc_manager { Faker::Lorem.sentence }
    password { phone[-6..].chars.join }
    password_confirmation { password }
    confirmed_at { DateTime.now.utc }
    social_auth
    pn_enabled { true }
    deleted_at { nil }

    trait :unconfirmed do
      confirmed_at { nil }
    end

    trait :unsubscribed do
      transient do
        subscribed? { false }
      end
    end

    trait :soft_destroyed do
      transient do
        soft_destroyed? { true }
      end
    end

    trait :with_job do
      transient do
        with_job { true }
      end
    end

    transient do
      pref_id { Faker::Number.between(from: 0, to: 1) }
      city_id { Faker::Number.between(from: 0, to: 1) }
      subscribed? { true }
      with_social_auth { true }
      devices_count { Faker::Number.between(from: 1, to: 3) }
      with_job { false }
    end

    after(:build) do |obj, evaluator|
      obj.social_auth = build(:social_auth, user: obj) if evaluator.with_social_auth
    end

    after(:build) do |obj, evaluator|
      obj.devices = Array.new(evaluator.devices_count).map do
        build(:device, owner: obj)
      end
    end

    after(:build) do |obj, evaluator|
      obj.job = build(:job, employer: obj) if evaluator.with_job
    end

    after(:create) do |obj, evaluator|
      if evaluator.subscribed?
        obj.subscription = create(:subscription, subscriber: obj)
        obj.save
        obj.reload
      end
    end

    after(:create) do |obj, evaluator|
      if evaluator.soft_destroyed?
        obj.soft_destroy
        if evaluator.deleted_at.present?
          obj.deleted_at = evaluator.deleted_at
          obj.save
        end
        obj.reload
      end
    end
  end
end
