FactoryBot.define do
  factory :image do
    job
    rank { Faker::Number.between(from: 1, to: 5) }
    file { Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/image.png'), 'image/png') }
  end
end
