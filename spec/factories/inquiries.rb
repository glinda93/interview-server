FactoryBot.define do
  factory :inquiry do
    scope { 'Employee' }
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    first_name_kana { 'テキスト' }
    last_name_kana { 'テキスト' }
    company_name { "#{Faker::Lorem.word}会社" }
    department { Faker::Lorem.word }
    email { Faker::Internet.email }
    title { Faker::Lorem.sentence }
    content { Faker::Lorem.sentence }
  end
end
