require 'stripe_mock'

FactoryBot.define do
  factory :job do
    employer
    company_name { "#{Faker::Lorem.word}職業" }
    pref { employer.pref }
    city { employer.city }
    address { Faker::Address.street_address }
    phone { Faker::PhoneNumber.cell_phone_in_e164.delete('+') }
    company_email { Faker::Internet.email }
    price_type { PriceType.find 1 }
    payment_method { PaymentMethod.find 1 }
    price { Faker::Number.between(from: 1000, to: 9999) }
    desc_main { Faker::Lorem.sentence }
    desc_skill { Faker::Lorem.sentence }
    desc_extra { Faker::Lorem.sentence }
    desc_payment { Faker::Lorem.sentence }
    desc_employer { Faker::Lorem.sentence }
    incl_commuting_fee { true }
    child_allowed { true }
    work_hour_from { "10:00" }
    work_hour_to { "20:00" }

    status { Job::STATUSES[:active] }

    trait :inactive do
      status { Job::STATUSES[:inactive] }
    end

    transient do
      skills_count { Faker::Number.between(from: 1, to: 5) }
      images_count { Faker::Number.between(from: 1, to: 5) }
      recruitment_count { 3 }
    end

    after(:build) do |job, evaluator|
      job.images = Array.new(evaluator.images_count).map.with_index do |_i, index|
        build(:image, rank: index + 1, job: job)
      end

      job.skills = Array.new(evaluator.skills_count).map.with_index do |_i, index|
        Skill.find(index + 1)
      end

      job.recruitments = Array.new(evaluator.recruitment_count).map do
        build(:recruitment, job: job)
      end
    end
  end
end
