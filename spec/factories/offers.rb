FactoryBot.define do
  factory :offer do
    recruitment
    offerable_from factory: :employee
    offerable_to { recruitment.employer }

    trait :from_employee do
      offerable_from factory: :employee
      offerable_to { recruitment.employer }
    end

    trait :from_employer do
      offerable_from { recruitment.employer }
      offerable_to factory: :employee
    end

    trait :to_employee do
      from_employer
    end

    trait :to_employer do
      from_employee
    end

    trait :accepted do
      response { Offer::RESPONSES[:accepted] }
      responded_at { DateTime.now.utc }
    end

    trait :declined do
      response { Offer::RESPONSES[:declined] }
      responded_at { DateTime.now.utc }
    end
  end
end
