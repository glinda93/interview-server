FactoryBot.define do
  factory :recruitment do
    job
    recruit_at { 1.day.after.utc }
    number { Faker::Number.between(from: 1, to: 5) }
    completed_at { nil }
    status { Recruitment::STATUSES[:active] }

    trait :inactive do
      status { Recruitment::STATUSES[:inactive] }
    end

    trait :completed do
      completed { Time.now.utc }
    end
  end
end
