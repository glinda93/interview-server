FactoryBot.define do
  factory :review do
    reviewable factory: :offer
    reviewable_from { reviewable.offerable_from }
    reviewable_to { reviewable.offerable_to }
    rating { Faker::Number.between(from: 0, to: 5) }
    comment { Faker::Lorem.sentence }
  end
end
