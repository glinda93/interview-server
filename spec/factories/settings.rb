FactoryBot.define do
  factory :setting do
    key { 'test_setting_key' }
    value { 'test_setting_value' }
  end
end
