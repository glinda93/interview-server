FactoryBot.define do
  factory :social_auth do
    user { create(:employee) }
    provider { 'twitter' }
    uid { Faker::Number.between(from: 100_000_000, to: 999_999_999) }

    trait :employee do
      user { create(:employee) }
    end

    trait :employer do
      user { create(:employer) }
    end

    trait :twitter do
      provider { 'twitter' }
    end

    trait :facebook do
      provider { 'facebook' }
    end
  end
end
