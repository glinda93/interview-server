FactoryBot.define do
  factory :subscription do
    subscriber factory: :employer
    payment_gateway { Subscription::PAYMENT_GATEWAYS[:stripe] }
    data do
      {
        customer_id: 'cus_JQElL1InFCrp04',
        subscription_id: 'sub_JQElc97O4Q7Cqg'
      }
    end
  end
end
