FactoryBot.define do
  factory :wish do
    wishable_to factory: :job
    wishable_from factory: :employee
  end

  trait :employee_wish do
    wishable_to factory: :job
    wishable_from factory: :employee
  end

  trait :employer_wish do
    wishable_to factory: :employee
    wishable_from factory: :employer
  end
end
