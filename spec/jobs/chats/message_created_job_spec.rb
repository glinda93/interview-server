require 'rails_helper'

RSpec.describe Chats::MessageCreatedJob, type: :job do
  include ActiveJob::TestHelper

  let(:message) { create(:chat_message) }
  let(:author) { message.authorable }
  let(:chat_room) { message.chat_room }

  before do
    clear_enqueued_jobs
    ActiveJob::Base.queue_adapter = :test
    message
  end

  describe 'perform_later' do
    it 'enqueues a job' do
      expect do
        described_class.perform_later chat_message: message
      end.to have_enqueued_job
    end
  end

  describe 'perform' do
    it 'updates last_message_at' do
      chat_room.last_message_at = nil
      chat_room.save!

      expect do
        described_class.perform_now chat_message_id: message.id
        chat_room.reload
      end.to change(chat_room, :last_message_at).from(nil).to(Time)
    end

    it 'creates chat unread messages' do
      expect do
        described_class.perform_now chat_message_id: message.id
      end.to change(ChatUnreadMessage, :count).by 1
    end
  end
end
