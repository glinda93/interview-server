require 'rails_helper'

RSpec.describe Chats::NotifyMessageEventJob, type: :job do
  include ActiveJob::TestHelper

  let(:message) { create(:chat_message) }
  let(:user) { message.authorable }
  let(:chat_room) { message.chat_room }
  let(:recipient) do
    chat_room.chattables.detect do |chattable|
      chattable != user
    end
  end
  let(:devices_count) do
    chattables = chat_room.chattables
    count = 0
    chattables.each do |user|
      count += user.android_devices.count
      count += user.ios_devices.count
    end
    count
  end

  let(:recipient_devices_count) do
    recipient.android_devices.count + recipient.ios_devices.count
  end

  before do
    clear_enqueued_jobs
    ActiveJob::Base.queue_adapter = :test
    message
  end

  describe 'perform_later' do
    it 'enqueus a job' do
      expect do
        described_class.perform_later message: message, event: ChatChannel::EVENTS[:new_message]
      end.to have_enqueued_job
    end
  end

  describe 'perform' do
    it 'sends push notifications' do
      expect do
        described_class.perform_now message: message, event: ChatChannel::EVENTS[:new_message]
      end.to change { Rpush::Notification.count }.by recipient_devices_count
    end
  end
end
