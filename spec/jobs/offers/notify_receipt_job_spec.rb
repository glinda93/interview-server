require 'rails_helper'

RSpec.describe Offers::NotifyReceiptJob, type: :job do
  include ActiveJob::TestHelper
  let(:offer) { create(:offer) }

  before do
    clear_enqueued_jobs
    ActiveJob::Base.queue_adapter = :test
    offer
  end

  describe 'perform_later' do
    it 'enqueus a job' do
      expect do
        described_class.perform_later offer
      end.to have_enqueued_job
    end
  end

  describe 'perform' do
    around { |example| perform_enqueued_jobs(&example) }

    it 'sends a push notification' do
      expect do
        described_class.perform_now offer
      end.to change { Rpush::Notification.count }.by offer.offerable_to.devices.count
    end

    it 'sends an email' do
      expect do
        described_class.perform_now offer
      end.to change { ActionMailer::Base.deliveries.length }.by 1
    end
  end
end
