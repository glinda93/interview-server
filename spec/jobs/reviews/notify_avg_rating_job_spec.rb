require 'rails_helper'

RSpec.describe Reviews::NotifyAvgRatingJob, type: :job do
  include ActiveJob::TestHelper
  let(:review) { create(:review) }

  before do
    clear_enqueued_jobs
    ActiveJob::Base.queue_adapter = :test
    review
  end

  describe 'perform_later' do
    it 'enqueues a job' do
      expect do
        described_class.perform_later review
      end.to have_enqueued_job
    end
  end

  describe 'perform' do
    before do
      allow(AppearanceChannel).to receive(:broadcast_to)
      allow(review.reviewable_to).to receive(:online?).and_return true
    end

    it 'broadcasts avg rating to reviewable_to' do
      described_class.perform_now review.reviewable_to
      expect(AppearanceChannel).to have_received(:broadcast_to).with(review.reviewable_to, {
                                                                       event: AppearanceChannel::EVENTS[:avg_rating_updated],
                                                                       payload: review.reviewable_to.avg_rating
                                                                     })
    end
  end
end
