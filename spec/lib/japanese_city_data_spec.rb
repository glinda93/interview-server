require 'rails_helper'

RSpec.describe JapaneseCityData do
  describe '#get_japanese_city_data' do
    it 'loads Japanese prefectures and city data from an external file' do
      expect(described_class.fetch_japanese_city_data).to be_an_instance_of(Array)
    end
  end

  describe '#pref_by_id' do
    it 'returns prefecture data by id' do
      pref = described_class.pref_by_id(0)
      expect(pref[:label]).not_to be_nil
      expect(pref[:value]).not_to be_nil
      expect(pref[:children]).to be_an_instance_of(Array)
    end
  end

  describe '#city_by_pref_and_id' do
    it 'returns city data by prefecture id and city id' do
      city = described_class.city_by_pref_and_id(0, 0)
      expect(city[:label]).not_to be_nil
      expect(city[:value]).not_to be_nil
    end
  end
end
