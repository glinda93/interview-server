require 'rails_helper'

RSpec.describe Stats::Offers do
  describe 'calculate' do
    it 'works' do
      expect(described_class.calculate).to be_a Hash
    end
  end

  describe 'calc_monthly' do
    it 'works' do
      expect(described_class.calc_monthly(Time.zone.now)).to be_a Hash
    end
  end
end
