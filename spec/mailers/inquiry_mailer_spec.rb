require 'rails_helper'

RSpec.describe InquiryMailer do
  let(:inquiry) { create(:inquiry) }

  describe 'receipt_mail' do
    let(:mail) { described_class.with(inquiry: inquiry.as_json.with_indifferent_access).receipt_mail }
    let(:mail_content) { mail.html_part.body.decoded }

    it 'receipt mail' do
      expect(mail_content).to match inquiry.first_name
    end
  end

  describe 'customer_service_mail' do
    let(:mail) { described_class.with(inquiry: inquiry.as_json.with_indifferent_access).customer_service_mail }
    let(:mail_content) { mail.html_part.body.decoded }

    it 'customer service mail' do
      expect(mail_content).to match inquiry.first_name
    end
  end
end
