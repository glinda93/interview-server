require 'rails_helper'

RSpec.describe Admin, type: :model do
  let(:model) { create(:admin) }

  describe 'factory' do
    it 'returns admin' do
      expect(model).to be_a described_class
    end
  end
end
