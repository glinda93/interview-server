require 'rails_helper'

RSpec.describe ChatMessage, type: :model do
  include ActiveJob::TestHelper
  let(:employer) { create(:employer) }
  let(:employee) { create(:employee) }
  let(:chat_room) { create(:chat_room, participants: [employer, employee]) }
  let(:new_message) { create(:chat_message, chat_room: chat_room, authorable: employee) }

  before do
    clear_enqueued_jobs
  end

  around { |example| perform_enqueued_jobs(&example) }

  describe 'unread_messages' do
    it 'returns unread messages by user' do
      expect do
        new_message
      end.to change { described_class.unread_messages(employer).count }.by 1

      expect(described_class.unread_messages(employer)).to eq [new_message]
      expect(described_class.unread_messages(employee)).to eq []
    end
  end

  describe 'perform_message_created_job' do
    it 'creates unread message' do
      expect do
        new_message
      end.to change {
        ChatUnreadMessage.where(user: employer, chat_room: chat_room).count
      }.by(1).and change {
        ChatUnreadMessage.where(user: employee, chat_room: chat_room).count
      }.by 0
    end
  end
end
