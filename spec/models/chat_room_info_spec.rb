require 'rails_helper'

RSpec.describe ChatRoomInfo, type: :model do
  let(:chat_room_info) { create(:chat_room_info) }

  it 'responds to unread_messages' do
    expect(chat_room_info).to respond_to(:unread_messages)
  end
end
