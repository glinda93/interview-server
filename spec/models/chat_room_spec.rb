require 'rails_helper'

RSpec.describe ChatRoom, type: :model do
  let(:chat_room) { create(:chat_room) }

  describe 'factory' do
    it 'creates a chat room' do
      expect(chat_room).to be_kind_of ApplicationRecord
      expect(chat_room.chattables.count).to eq 2
    end
  end

  describe 'where_participants' do
    let(:employer) { create(:employer) }
    let(:employee) { create(:employee) }
    let(:chat_room) { described_class.create_or_find_by_participants [employee, employer] }

    before do
      chat_room
      3.times do
        described_class.create_or_find_by_participants [employee, create(:employer)]
      end
      2.times do
        described_class.create_or_find_by_participants [employer, create(:employee)]
      end
    end

    it 'finds a chat room by participants' do
      expect(described_class.where_participants([employee, employer]).count.length).to eq 1
      expect(described_class.where_participants([employee, employer]).first.id).to eq chat_room.id
    end
  end
end
