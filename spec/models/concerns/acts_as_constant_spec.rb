require 'rails_helper'

class ActsAsConstantModel < ApplicationRecord
  include ActsAsConstant

  def self.table_name
    "constants"
  end

  def id
    1
  end
end

RSpec.describe ActsAsConstant do
  let(:model_class) { ActsAsConstantModel }
  let(:constant_model) { model_class.new }

  describe '#cache_key' do
    it 'returns cache key for a given path' do
      expect(model_class.cache_key("ids")).to be_instance_of String
    end
  end
end
