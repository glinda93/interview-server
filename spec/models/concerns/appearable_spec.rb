require 'rails_helper'

class AppearableModel
  include Appearable

  def id
    1
  end
end

RSpec.describe Appearable do
  before do
    Rails.cache.clear
  end

  let(:model_class) { AppearableModel }
  let(:model) { model_class.new }

  describe 'appear' do
    let(:employee) { create(:employee) }

    it 'adds id to appearance_list' do
      model.appear
      expect(model_class.appearance_list).to include model.id
    end

    it 'adds channel to appearance_channels' do
      expect { model.appear(on: 'foo') }.to change { model.appearance_channels.count }.by 1
      expect { model.appear(on: employee) }.to change { model.appearance_channels.count }.by 1
    end
  end

  describe 'disappear' do
    let(:channel) { 'TestChannel' }
    let(:model) do
      model = model_class.new
      model.appear
      model.appear(on: channel)
      model
    end

    context 'when channel name is present' do
      it 'leaves user from a channel' do
        expect(model.online?).to eq true
        expect(model.online?(on: channel)).to eq true
        model.disappear(on: channel)
        expect(model.online?(on: channel)).to eq false
      end
    end

    context 'when channel name is nil' do
      it 'leaves user from all channel' do
        model.disappear
        expect(model.online?(on: channel)).to eq false
        expect(model.online?).to eq false
      end
    end
  end

  describe 'online?' do
    let(:channel) { 'TestChannel' }
    let(:model) do
      model = model_class.new
      model.appear
      model.appear(on: channel)
      model
    end

    it 'returns true if user is online; false otherwise' do
      expect(model.online?).to eq true
      expect(model.online?(on: channel)).to eq true
      expect(model.online?(on: 'WrongChannel')).to eq false
    end
  end
end
