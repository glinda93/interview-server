require 'rails_helper'

class StreamableModel
  attr_accessor :id

  include Streamable
end

RSpec.describe Streamable do
  let(:model_class) { StreamableModel }
  let(:model) { StreamableModel.new }

  describe 'stream_name' do
    before do
      model.id = 1
    end

    it 'returns string' do
      expect(model.stream_name).to be_a String
    end

    context 'when id is empty' do
      before do
        model.id = nil
      end

      it 'throws ArgumentError' do
        expect do
          model.stream_name
        end.to raise_error ArgumentError
      end
    end
  end
end
