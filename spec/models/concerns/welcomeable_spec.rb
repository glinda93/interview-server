require 'rails_helper'

RSpec.describe Welcomeable, use_transactional_fixtures: false do
  before do
    Temping.create :welcome_model do
      include Welcomeable # rubocop:disable RSpec/DescribedClass

      with_columns do |t|
        t.string :email
      end
    end
  end

  let(:model_class) { WelcomeModel }
  let(:model) { model_class.new(email: 'foo@bar.com') }
  let(:mail) { RegistrationMailer.with(user: model).welcome_email }

  describe "after create" do
    it "sends welcome email" do
      expect { model.save! }.to change { ActionMailer::Base.deliveries.count }.by(1)
      expect(mail.to).to eql([model.email])
      expect(mail.subject).to eql(I18n.t('mail.subjects.welcome'))
    end
  end
end
