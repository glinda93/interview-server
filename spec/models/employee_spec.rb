require 'rails_helper'

RSpec.describe Employee, type: :model do
  describe 'soft_destroy' do
    let(:employee) { create(:employee) }

    it 'successfully but softly... destroys an employee' do
      employee.soft_destroy
      expect(employee.deleted_at).not_to be_nil
      expect(employee.pn_enabled).to be false
    end
  end

  describe 'wishlist_job_ids' do
    let(:employee) { create(:employee) }
    let(:wish) { create(:wish, :employee_wish, wishable_from: employee) }

    before do
      wish
    end

    it 'returns array of job ids in wishlist' do
      expect(employee.wishlist_job_ids).to eq [wish.wishable_to_id]
    end

    context 'when wish is created or updated' do
      it 'updates cache' do
        expect do
          create(:wish, :employee_wish, wishable_from: employee)
        end.to change { employee.wishlist_job_ids.length }.by 1
        expect do
          wish.destroy
        end.to change { employee.wishlist_job_ids.length }.by(-1)
      end
    end
  end

  describe 'to_csv' do
    let(:employee_count) { 1 }
    let(:csv) { described_class.to_csv }

    before do
      employee_count.times do
        create(:employee)
      end
    end

    it 'returns csv format of all employees' do
      expect(csv).to be_a String
      expect(csv.lines.count).to eq employee_count + 1
    end
  end
end
