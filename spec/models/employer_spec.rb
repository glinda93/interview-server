require 'rails_helper'
require 'stripe_mock'

RSpec.describe Employer, type: :model do
  describe 'factory' do
    let(:employer) { create(:employer) }

    it 'creates an Employer' do
      expect(employer).not_to be_nil
      expect(employer.subscribed?).to be true
      expect(employer.subscription.data["customer_id"]).to match "cus_*"
    end

    context 'when unsubscribed' do
      let(:employer) { create(:employer, :unsubscribed) }

      it 'creates an unsubscribed employer' do
        expect(employer.subscribed?).to be false
      end
    end
  end

  describe 'send_otp_instructions' do
    let(:employer) { create(:employer) }
    let(:mail) { PasswordMailer.with(user: employer).password_instructions }

    before do
      employer
    end

    it 'sends otp instructions' do
      expect do
        employer.send_otp_instructions
      end.to change { ActionMailer::Base.deliveries.length }.by 1

      expect(mail.html_part.body.decoded).to match employer.otp
    end
  end

  describe 'wishlist_employee_ids' do
    let(:employer) { create(:employer) }
    let(:wish) { create(:wish, :employer_wish, wishable_from: employer) }

    before do
      wish
    end

    it 'returns array of employee ids in wishlist' do
      expect(employer.wishlist_employee_ids).to eq [wish.wishable_to_id]
    end

    context 'when wish is created or destroyed' do
      it 'updates cache' do
        expect do
          create(:wish, :employer_wish, wishable_from: employer)
        end.to change { employer.wishlist_employee_ids.length }.by 1
        expect do
          wish.destroy
        end.to change { employer.wishlist_employee_ids.length }.by(-1)
      end
    end
  end

  describe 'soft_destroy' do
    let(:employer) { create(:employer, :unsubscribed) }
    let(:job) { create(:job, employer: employer) }
    let(:stripe_helper) { StripeMock.create_test_helper }
    let(:token) { stripe_helper.generate_card_token }
    let(:product) { stripe_helper.create_product }
    let(:price) { stripe_helper.create_price({ product: product[:id] }) }

    before do
      job
      StripeMock.start
      Rails.application.config.setting.stripe[:price_id] = price[:id]
      employer.create_subscription token
      employer.reload
    end

    after { StripeMock.stop }

    it 'sets deleted_at' do
      expect(employer.job).not_to be_nil
      employer.soft_destroy
      expect(employer.pn_enabled).to eq false
      expect(employer.deleted_at).not_to be_nil
    end

    it 'sets job status inactive' do
      employer.soft_destroy
      expect(employer.job.status).to eq Job::STATUSES[:inactive]
    end

    it 'unsubscribes' do
      employer.soft_destroy
      expect(employer.subscribed?).to eq false
    end
  end
end
