require 'rails_helper'

RSpec.describe Job, type: :model do
  let(:job) { create(:job, recruitment_count: 0) }
  let(:recruitment) { create(:recruitment, job: job) }

  before do
    job.reload
  end

  describe 'recruit_at' do
    let(:recruitment) { create(:recruitment, job: job, recruit_at: 3.days.after.localtime.beginning_of_day) }
    let(:recruit_at) { recruitment.recruit_at }

    before do
      recruitment
      create(:recruitment, job: job)
      create(:recruitment, job: job)
    end

    it 'returns jobs which are available and recruiting at given date' do
      expect(described_class.recruit_at(recruit_at)).to eq [job]
    end
  end
end
