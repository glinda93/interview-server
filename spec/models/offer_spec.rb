require 'rails_helper'

RSpec.describe Offer, type: :model do
  let(:model) { create(:offer) }

  describe 'offerable_from_display_name' do
    it 'returns string' do
      expect(model.offerable_from_display_name).to be_instance_of String
    end

    context 'when offer is from employee' do
      let(:model) { create(:offer, :from_employee) }

      it 'returns display name of employee' do
        expect(model.offerable_from_display_name).to match model.offerable_from.display_name
      end
    end

    context 'when offer is from employer' do
      let(:model) { create(:offer, :from_employer) }

      it 'returns display name of job' do
        expect(model.offerable_from_display_name).to match model.job.display_name
      end
    end
  end

  describe 'offerable_to_display_name' do
    it 'returns a string' do
      expect(model.offerable_to_display_name).to be_instance_of String
    end

    context 'when offer is to employee' do
      let(:model) { create(:offer, :to_employee) }

      it 'returns display name of employee' do
        expect(model.offerable_to_display_name).to match model.offerable_to.display_name
      end
    end

    context 'when offer is to employer' do
      let(:model) { create(:offer, :to_employer) }

      it 'returns display name of job' do
        expect(model.offerable_to_display_name).to match model.job.display_name
      end
    end
  end

  describe 'response' do
    include ActiveJob::TestHelper
    before do
      clear_enqueued_jobs
      ActiveJob::Base.queue_adapter = :test
      model
    end

    it 'enqueus NotifyResponseJob' do
      expect do
        model.respond(Offer::RESPONSES[:accepted])
      end.to have_enqueued_job Offers::NotifyResponseJob
    end
  end

  describe 'review_missing_hard' do
    let(:review_completed) do
      offer = create(:offer, :accepted, :from_employee)
      create(:review, reviewable: offer, reviewable_from: offer.offerable_from, reviewable_to: offer.offerable_to)
      create(:review, reviewable: offer, reviewable_from: offer.offerable_to, reviewable_to: offer.offerable_from)
      offer
    end
    let(:review_incomplete) do
      offer = create(:offer, :accepted, :from_employee)
      create(:review, reviewable: offer, reviewable_from: offer.offerable_from, reviewable_to: offer.offerable_to)
      offer
    end

    it 'returns offers which does not have at least two reviews' do
      review_completed
      review_incomplete
      expect(described_class.review_missing_hard.count.length).to eq 1
      expect(described_class.review_missing_hard.first.id).to eq review_incomplete.id
    end
  end

  describe 'users_did_not_review' do
    let(:offer) { create(:offer, :accepted) }

    before do
      create(:review, reviewable: offer, reviewable_from: offer.offerable_from, reviewable_to: offer.offerable_to)
    end

    it 'returns users who have not left a review' do
      expect(offer.users_did_not_review).to eq [offer.offerable_to]
    end
  end

  describe 'reviewed_by' do
    let(:offer) { create(:offer, :accepted) }
    let(:review) { create(:review, reviewable: offer, reviewable_from: offer.offerable_from, reviewable_to: offer.offerable_to) }

    before do
      offer
      review
    end

    it 'returns offers reviewed by user' do
      expect(described_class.reviewed_by(offer.offerable_from).first).to eq offer
      expect(described_class.reviewed_by(offer.offerable_to).count).to eq 0
    end
  end

  describe 'not_reviewed_by' do
    let(:offer) { create(:offer, :accepted) }
    let(:review) { create(:review, reviewable: offer, reviewable_from: offer.offerable_from, reviewable_to: offer.offerable_to) }

    before do
      offer
      review
    end

    it 'returns reviews which are accepted and belongs to user but not reviewed by user' do
      expect(described_class.not_reviewed_by(offer.offerable_from).first).to be_nil
      expect(described_class.not_reviewed_by(offer.offerable_to).first.id).to eq offer.id
    end
  end

  describe 'update_recruitment_matches_count' do
    let(:recruitment) { model.recruitment }

    it 'updates recruitment matches count' do
      model.respond(Offer::RESPONSES[:accepted])
      recruitment.reload
      expect(recruitment.matches_count).to eq 1
    end
  end
end
