require 'rails_helper'

RSpec.describe Recruitment, type: :model do
  let(:recruitment) { create(:recruitment) }
  let(:job) { recruitment.job }

  it 'delegates missing method to job' do
    expect(recruitment).to respond_to(:company_name)
    expect(recruitment.company_name).to eq job.company_name
  end

  it 'responds to matches' do
    expect(recruitment).to respond_to(:matches)
  end

  describe 'recruit_at' do
    let(:recruit_at) { 3.days.after.beginning_of_day.localtime }
    let(:recruitment) { create(:recruitment, recruit_at: recruit_at) }

    before do
      recruitment
      create(:recruitment)
      create(:recruitment)
    end

    it 'returns recruitments that recurit at given date' do
      expect(described_class.recruit_at(recruit_at)).to eq [recruitment]
    end
  end
end
