require 'rails_helper'

RSpec.describe Review, type: :model do
  describe 'update_recruitment_completed_at' do
    let(:job) { create(:job, recruitment_count: 0) }
    let(:recruitment) { create(:recruitment, job: job, number: 1) }
    let(:offer) { create(:offer, :accepted, recruitment: recruitment) }

    before do
      create(:review, reviewable: offer, reviewable_from: offer.offerable_from, reviewable_to: offer.offerable_to)
    end

    it 'updates recruitment completed at' do
      expect(recruitment.completed_at).to be_nil
      create(:review, reviewable: offer, reviewable_from: offer.offerable_to, reviewable_to: offer.offerable_from)
      recruitment.reload
      offer.reload
      expect(offer.reviews_count).to eq 2
      expect(recruitment.completed_at).not_to be_nil
    end
  end

  describe 'update_offer_visible_on_create' do
    let(:job) { create(:job, recruitment_count: 0) }
    let(:recruitment) { create(:recruitment, job: job, number: 1) }
    let(:offer) { create(:offer, :accepted, recruitment: recruitment) }

    it 'updates offer visible' do
      expect do
        create(:review, reviewable: offer, reviewable_from: offer.offerable_from, reviewable_to: offer.offerable_to)
      end.to change(offer, :visible_to_offerable_from).from(true).to(false)

      expect do
        create(:review, reviewable: offer, reviewable_from: offer.offerable_to, reviewable_to: offer.offerable_from)
      end.to change(offer, :visible_to_offerable_to).from(true).to(false)
    end
  end

  describe 'update_offer_visible_on_destroy' do
    let(:job) { create(:job, recruitment_count: 0) }
    let(:recruitment) { create(:recruitment, job: job, number: 1) }
    let(:offer) { create(:offer, :accepted, recruitment: recruitment) }
    let(:review) { create(:review, reviewable: offer, reviewable_from: offer.offerable_from, reviewable_to: offer.offerable_to) }

    before do
      review
    end

    it 'updates offer visible' do
      expect do
        review.destroy
      end.to change(offer, :visible_to_offerable_from).from(false).to(true)
    end
  end
end
