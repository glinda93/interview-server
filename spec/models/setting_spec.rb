require 'rails_helper'

RSpec.describe Setting, type: :model do
  let(:model) { create(:setting) }

  describe 'get_value' do
    context 'when key is hit' do
      it 'returns value of record' do
        expect(described_class.get_value(model.key)).to eq model.value
      end
    end

    context 'when key is missing' do
      it 'returns nil' do
        expect(described_class.get_value('not_working')).to eq nil
      end
    end
  end

  describe 'method_missing' do
    it 'calls get_value' do
      expect(described_class.send(model.key)).to eq model.value
    end
  end
end
