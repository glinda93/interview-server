require 'rails_helper'

RSpec.describe Admins::AdminsController, type: :request do
  let(:admin) { create(:admin) }
  let(:headers) { auth_headers(user: admin) }

  describe "GET #{admins_api_path 'me'}" do
    it 'returns current admin' do
      get admins_api_path('me'), headers: headers
      expect(json_body[:id]).to eq admin.id
    end
  end

  describe "POST #{admins_api_path('verify_password')}" do
    let(:password) { 'correct_password' }
    let(:incorrect_password) { 'incorrect_password' }
    let(:admin) { create(:admin, password: password, password_confirmation: password) }

    context 'when password is correct' do
      it 'returns success true and profile_change_token' do
        post admins_api_path('verify_password'), headers: headers, params: {
          password: password
        }.to_json
        admin.reload
        expect(json_body[:success]).to eq true
        expect(json_body[:profile_change_token]).to be_a String
        expect(json_body[:profile_change_token]).to eq admin.profile_change_token
      end
    end

    context 'when password is wrong' do
      it 'returns success false' do
        post admins_api_path('verify_password'), headers: headers, params: {
          password: incorrect_password
        }.to_json
        admin.reload
        expect(json_body[:success]).to eq false
        expect(admin.profile_change_token).to be_nil
      end
    end
  end

  describe "POST #{admins_api_path 'change_email'}" do
    let(:unconfirmed_email) { 'foo@bar.com' }
    let(:mail) do
      AdminMailer.with(model: admin).change_email_mail
    end
    let(:mail_content) { mail.html_part.body.decoded }

    before do
      admin.send(:regenerate_profile_change_token)
    end

    around { |example| perform_enqueued_jobs(&example) }

    context 'when profile_change_token is correct' do
      let(:params) do
        {
          admin: {
            profile_change_token: admin.profile_change_token,
            unconfirmed_email: unconfirmed_email
          }
        }
      end

      it 'updates unconfirmed_email' do
        post admins_api_path('change_email'), headers: headers, params: params.to_json
        admin.reload
        expect(admin.unconfirmed_email).to eq unconfirmed_email
      end

      it 'sends change_email mail' do
        expect do
          post admins_api_path('change_email'), headers: headers, params: params.to_json
        end.to change { ActionMailer::Base.deliveries.length }.by 1
        expect(mail_content).to match admin.otp
      end
    end

    context 'when profile_change_token is incorrect' do
      let(:params) do
        {
          admin: {
            profile_change_token: 'not_working',
            unconfirmed_email: unconfirmed_email
          }
        }
      end

      it 'returns false' do
        post admins_api_path('change_email'), headers: headers, params: params.to_json
        expect(json_body[:success]).to eq false
      end
    end
  end

  describe "POST #{admins_api_path 'verify_unconfirmed_email'}" do
    let(:unconfirmed_email) { 'foo@example.com' }
    let(:otp) { admin.otp }
    let(:params) { { otp: otp } }

    before do
      admin.unconfirmed_email = unconfirmed_email
      admin.save!
      admin.generate_otp
    end

    context 'when otp is correct' do
      it 'changes email' do
        post admins_api_path("verify_unconfirmed_email"), params: params.to_json, headers: headers
        admin.reload
        expect(json_body[:success]).to eq true
        expect(json_body[:admin][:id]).to eq admin.id
        expect(json_body[:admin][:email]).to eq unconfirmed_email
        expect(json_body[:admin][:unconfirmed_email]).to eq nil
        expect(admin.otp).to eq nil
      end
    end

    context 'when otp is not correct' do
      let(:otp) { '' }

      it 'returns false' do
        post admins_api_path("verify_unconfirmed_email"), params: params.to_json, headers: headers
        expect(json_body[:success]).to eq false
      end
    end
  end
end
