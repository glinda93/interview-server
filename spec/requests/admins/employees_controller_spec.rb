require 'rails_helper'

RSpec.describe Admins::EmployeesController, type: :request do
  let(:admin) { create(:admin) }
  let(:headers) { auth_headers(user: admin) }

  describe "GET #{admins_api_path 'employees'}" do
    let(:employees_count) { 1 }

    before do
      FactoryBot.create_list(:employee, employees_count)
    end

    context 'when format is json' do
      it 'returns pagination' do
        get admins_api_path('employees'), headers: headers
        expect(json_body[:pagination][:count]).to eq employees_count
      end
    end

    context 'when format is csv' do
      it 'returns csv attachment' do
        get admins_api_path('employees.csv'), headers: headers
        expect(response.body).to eq Employee.to_csv
        expect(response.header['Content-Type']).to match 'text/csv'
        expect(response.header['Content-Disposition']).to match "attachment; filename=\"#{Employee.csv_filename}\""
      end
    end
  end
end
