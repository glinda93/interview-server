require 'rails_helper'

RSpec.describe Admins::EmployersController, type: :request do
  let(:admin) { create(:admin) }
  let(:headers) { auth_headers(user: admin) }

  describe "GET #{admins_api_path 'employers'}" do
    let(:employers_count) { 1 }

    before do
      FactoryBot.create_list(:employer, employers_count)
    end

    context 'when format is json' do
      it 'returns pagination' do
        get admins_api_path('employers'), headers: headers
        expect(json_body[:pagination][:count]).to eq employers_count
      end
    end

    context 'when format is csv' do
      it 'returns csv attachment' do
        get admins_api_path('employers.csv'), headers: headers
        expect(response.body).to eq Employer.to_csv
        expect(response.header['Content-Type']).to match 'text/csv'
        expect(response.header['Content-Disposition']).to match "attachment; filename=\"#{Employer.csv_filename}\""
      end
    end
  end
end
