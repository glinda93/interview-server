require 'rails_helper'

RSpec.describe Admins::SessionsController, type: :request do
  let(:password) { 'adminpassword' }
  let(:model) { create(:admin, password: password) }

  describe "POST #{admins_api_path 'sign_in'}" do
    let(:params) do
      {
        admin: {
          email: model.email,
          password: password
        }
      }
    end

    context 'when email and password match' do
      around { |example| perform_enqueued_jobs(&example) }

      let(:params) do
        {
          admin: {
            email: model.email,
            password: password
          }
        }
      end

      let(:mail) do
        AdminMailer.with(model: model).otp_mail
      end

      let(:mail_content) { mail.html_part.body.decoded }

      it 'returns true' do
        post admins_api_path('sign_in'), params: params
        resp = json_body
        expect(resp[:success]).to eq true
      end

      it 'does not set authorization header' do
        post admins_api_path('sign_in'), params: params
        expect(response.headers['Authorization']).to be_nil
      end

      it 'generates otp' do
        expect do
          post admins_api_path('sign_in'), params: params
        end.to change(model, :otp).from(nil).to(String)
      end

      it 'sends otp mail' do
        expect do
          post admins_api_path('sign_in'), params: params
        end.to change { ActionMailer::Base.deliveries.length }.by 1
        expect(mail_content).to match model.otp
      end
    end

    context 'when email or password does not match' do
      let(:params) do
        {
          admin: {
            email: model.email,
            password: 'wrongpassword'
          }
        }
      end

      it 'returns false' do
        post admins_api_path('sign_in'), params: params
        resp = json_body
        expect(resp[:success]).to eq false
      end
    end
  end

  describe "POST #{admins_api_path 'otp_verify'}" do
    let(:params) do
      {
        email: model.email,
        otp: model.generate_otp
      }
    end

    context 'when email and otp matches' do
      it 'signs in admin' do
        post admins_api_path('otp_verify'), params: params
        resp = json_body
        expect(resp[:id]).to eq model.id
      end

      it 'sets authorization header' do
        post admins_api_path('otp_verify'), params: params
        expect(response.headers['Authorization']).to be_a String
      end
    end

    context 'when email or otp does not match' do
      let(:params) do
        {
          email: model.email,
          otp: 'abcdefg'
        }
      end

      it 'throws ApplicationError' do
        expect do
          post admins_api_path('otp_verify'), params: params
        end.to raise_error ApplicationError, I18n.t('message.common.invalid_otp')
      end
    end
  end
end
