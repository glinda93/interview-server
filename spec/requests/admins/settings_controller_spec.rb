require 'rails_helper'

RSpec.describe Admins::SettingsController, type: :request do
  let(:admin) { create(:admin) }
  let(:headers) { auth_headers(user: admin) }
  let(:model) { create(:setting) }

  describe "GET #{admins_api_path 'settings'}" do
    it 'returns setting value' do
      get admins_api_path("settings?key=#{model.key}"), headers: headers
      expect(json_body[:value]).to eq model.value
    end
  end

  describe "PUT #{admins_api_path 'settings'}" do
    let(:updated_value) { 'updated_value' }
    let(:params) do
      {
        setting: {
          key: model.key,
          value: updated_value
        }
      }.to_json
    end

    it 'updates setting value' do
      put admins_api_path('settings'), params: params, headers: headers
      model.reload
      expect(model.value).to eq updated_value
    end
  end
end
