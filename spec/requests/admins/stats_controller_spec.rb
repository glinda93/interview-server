require 'rails_helper'

RSpec.describe Admins::StatsController, type: :request do
  let(:admin) { create(:admin) }
  let(:headers) { auth_headers(user: admin) }

  describe "GET #{admins_api_path 'stats/employees'}" do
    it 'returns current employee stats' do
      get admins_api_path('stats/employees'), headers: headers
      expect(json_body[:total_count]).to eq Stats::Employees.result[:total_count]
    end
  end

  describe "GET #{admins_api_path 'stats/employers'}" do
    it 'returns current employer stats' do
      get admins_api_path('stats/employers'), headers: headers
      expect(json_body[:total_count]).to eq Stats::Employers.result[:total_count]
    end
  end

  describe "GET #{admins_api_path 'stats/report'}" do
    it 'returns devices and offers stats' do
      get admins_api_path('stats/report'), headers: headers
      expect(json_body[:devices]).to be_a Hash
      expect(json_body[:offers]).to be_a Hash
    end

    context 'when month param is given' do
      it 'returns monthly report' do
        get admins_api_path("stats/report?month=#{Time.zone.now}"), headers: headers
        expect(json_body[:devices]).to be_a Hash
        expect(json_body[:offers]).to be_a Hash
      end
    end
  end
end
