require 'rails_helper'

RSpec.describe Commons::ConstantsController, type: :request do
  describe "GET #{api_path('constants', namespace: :common)}" do
    it 'renders JSON rep of constant models' do
      get api_path('constants', namespace: :common)
      expect(json_body).not_to be_nil
    end
  end
end
