require 'rails_helper'

RSpec.describe Commons::InquiriesController, type: :request do
  describe "POST #{api_path('inquiries', namespace: :common)}" do
    around { |example| perform_enqueued_jobs(&example) }

    let(:params) do
      {
        scope: 'Employer',
        first_name: '太郎',
        last_name: '山田',
        first_name_kana: 'ヤマダ',
        last_name_kana: 'タロ',
        company_name: 'テスト会社',
        department: 'テスト部署',
        email: 'test@test.com',
        title: 'テストタイトル',
        content: 'テストコンテンツ'
      }
    end

    it 'sends receipt mail and customer service mail' do
      expect do
        post api_path('inquiries', namespace: :common), params: { inquiry: params }
      end.to change { ActionMailer::Base.deliveries.count }.by 2
    end
  end
end
