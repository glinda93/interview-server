require 'rails_helper'

RSpec.describe Commons::SettingsController, type: :request do
  describe "GET #{commons_api_path('settings/privacy_policy')}" do
    before do
      create(:setting, key: :privacy_policy) unless Setting.privacy_policy
    end

    context 'when format is json' do
      it 'returns json' do
        get commons_api_path('settings/privacy_policy')
        expect(json_body[:value]).to match Setting.privacy_policy
      end
    end

    context 'when format is html' do
      it 'returns html' do
        get commons_api_path('settings/privacy_policy.html')
        expect(response.body).to match Setting.privacy_policy
      end
    end
  end

  describe "GET #{commons_api_path('settings/terms')}" do
    before do
      create(:setting, key: :terms) unless Setting.terms
    end

    context 'when format is json' do
      it 'returns json' do
        get commons_api_path('settings/terms')
        expect(json_body[:value]).to match Setting.terms
      end
    end

    context 'when format is html' do
      it 'returns html' do
        get commons_api_path('settings/terms.html')
        expect(response.body).to match Setting.terms
      end
    end
  end
end
