require 'rails_helper'

RSpec.describe Employees::ChatsController, type: :request do
  let(:chat_room) { create(:chat_room) }
  let(:employee) { chat_room.participants.find { |u| u.instance_of?(Employee) } }
  let(:headers) { auth_headers(user: employee) }
  let(:params) do
    {
      chat_message: {
        chat_room_id: chat_room.id,
        image: Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/image.png'), 'image/png')
      }
    }
  end

  describe "POST #{employees_api_path 'chats/upload_image'}" do
    it 'creates an image chat message' do
      expect do
        post employees_api_path('chats/upload_image'), params: params, headers: headers
      end.to change { ChatMessage.all.count }.by 1
      chat_message = ChatMessage.where(authorable: employee).first
      expect(chat_message.image).not_to be_nil
      expect(chat_message.message).to match "[image]*[/image]"
    end
  end

  describe "GET #{employees_api_path 'chats'}" do
    let(:chat_message) do
      chat_room
      employee
      create(:chat_message, chat_room: chat_room, authorable: employee)
    end

    it 'renders JSON rep of chat rooms' do
      chat_message
      get employees_api_path('chats'), headers: headers
      expect(json_body).not_to be_nil
    end
  end

  describe "GET #{employees_api_path('chats/:id')}" do
    it 'renders JSON rep of a chat room' do
      get employees_api_path("chats/#{chat_room.id}"), headers: headers
      expect(json_body).not_to be_nil
    end
  end

  describe "GET #{employers_api_path('chats/message')}" do
    let(:chat_message) do
      chat_room
      employee
      create(:chat_message, chat_room: chat_room, authorable: employee)
    end

    it 'renders JSON rep of chat messages' do
      get employees_api_path("chats/#{chat_room.id}"), headers: headers
      expect(json_body).not_to be_nil
    end
  end
end
