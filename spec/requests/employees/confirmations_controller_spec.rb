require 'rails_helper'

RSpec.describe Employees::ConfirmationsController, type: :request do
  let(:confirmation_token) { '123456' }
  let(:employee) { create(:employee, :unconfirmed, phone: '+19544182970', confirmation_token: confirmation_token) }
  let(:headers) { auth_headers(user: employee) }

  describe "GET #{employees_api_path('confirmation')}" do
    it 'confirms employee' do
      params = { confirmation_token: confirmation_token }
      get employees_api_path('confirmation'), params: params, headers: headers
      expect(json_body[:id]).to eq employee.id
      employee.reload
      expect(employee.confirmed?).to eq true
    end

    context 'when confirmation token is wrong' do
      it 'raises ApplicationError with confirmation_failed message' do
        params = { confirmation_token: 'wrong_token' }
        expect do
          get employees_api_path('confirmation'), params: params, headers: headers
        end.to raise_error ApplicationError, I18n.t('message.employee.confirmation_failed')
      end
    end

    context 'when employee is reconfirming' do
      let(:unconfirmed_phone) { '+1234567890' }
      let(:employee) { create(:employee, unconfirmed_phone: unconfirmed_phone, confirmation_token: confirmation_token) }

      it 'changes phone number' do
        params = { confirmation_token: confirmation_token }
        get employees_api_path('confirmation'), params: params, headers: headers
        expect(json_body[:id]).to eq employee.id
        employee.reload
        expect(employee.phone).to eq unconfirmed_phone.delete('+')
        expect(employee.unconfirmed_phone).to be_nil
        expect(employee.confirmation_token).to be_nil
        expect(employee.confirmed?).to eq true
      end
    end
  end

  describe "POST #{employees_api_path('confirmation')}" do
    around { |example| perform_enqueued_jobs(&example) }

    it 'sends confirmation token via SMS' do
      expect do
        post employees_api_path('confirmation'), headers: headers
      end.to change { ActionMailer::Base.deliveries.count }.by 1
    end
  end
end
