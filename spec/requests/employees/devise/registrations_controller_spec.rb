require 'rails_helper'

RSpec.describe Devise::RegistrationsController, type: :request do
  let(:email) { 'foo@bar.com' }
  let(:password) { 'password' }
  let(:params) { { employee: { email: email, password: password } } }

  describe "POST #{employees_api_path ''}" do
    it 'creates an employee' do
      expect do
        post employees_api_path(''), params: params, xhr: true
      end.to change { Employee.all.count }.by 1
    end
  end
end
