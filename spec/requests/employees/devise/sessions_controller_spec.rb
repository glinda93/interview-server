require 'rails_helper'

RSpec.describe Devise::SessionsController, type: :request do
  let(:password) { '123456' }
  let(:employee) { create(:employee, password: password, password_confirmation: password) }
  let(:params) { { employee: { email: employee.email, password: password } } }

  describe "POST #{employees_api_path 'sign_in'}" do
    it 'signs in an employee' do
      post employees_api_path('sign_in'), params: params, xhr: true
      expect(json_body[:id]).to eql employee.id
      expect(response.headers['Authorization']).to be_instance_of String
    end

    context 'when user is soft deleted' do
      before { employee.soft_destroy }

      it 'restores user' do
        post employees_api_path('sign_in'), params: params, xhr: true
        employee.reload
        expect(employee.deleted_at).to be_nil
      end
    end
  end
end
