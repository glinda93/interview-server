require 'rails_helper'

RSpec.describe Employees::EmployeesController, type: :request do
  let(:employee) { create(:employee) }
  let(:headers) { auth_headers(user: employee) }

  describe "GET #{employees_api_path('me')}" do
    it 'renders JSON rep of current user' do
      get employees_api_path("me"), headers: headers
      resp = json_body
      expect(resp).not_to be_nil
      expect(resp["id"]).to eq employee.id
    end

    context 'when missing or invalid token' do
      it 'responds with unauthorized status' do
        get employees_api_path("me")
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end

  describe "DELETE #{employees_api_path 'delete'}" do
    it 'softly destroys current user' do
      delete employees_api_path('delete'), headers: headers
      expect(json_body[:success]).to eq true
      employee.reload
      expect(employee.deleted_at).not_to be_nil
    end
  end
end
