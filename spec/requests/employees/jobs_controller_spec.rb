require 'rails_helper'

RSpec.describe Employees::JobsController, type: :request do
  let(:employee) { create(:employee) }
  let(:headers) { auth_headers(user: employee) }
  let(:unconfirmed_employee) { create(:employee, :unconfirmed) }
  let(:unconfirmed_headers) { auth_headers(user: unconfirmed_employee) }
  let(:job) { create(:job, pref: employee.pref, city: employee.city) }
  let(:recruitment) { job.recruitments.first }
  let(:job_id) { job.id }

  describe "GET #{employees_api_path('jobs')}" do
    before do
      job
    end

    it 'renders JSON rep of job search result' do
      get employees_api_path("jobs"), headers: headers
      resp = json_body
      expect(resp["rows"][0]["id"]).to eq job_id
      expect(resp[:rows][0][:wished]).to be_in [true, false]
    end

    it 'searches by recruit_at' do
      get employees_api_path('jobs'), headers: headers, params: { recruit_at: recruitment.recruit_at }, xhr: true
      resp = json_body
      expect(resp["rows"][0]["id"]).to eq job_id
    end

    context 'when user is unconfirmed' do
      let(:employee) { create(:employee, :unconfirmed) }

      it 'responds with unauthorized status' do
        get employees_api_path("jobs"), headers: headers
        expect(response).to have_http_status(:unauthorized)
        expect(json_body['message']).to eq I18n.t('message.employee.unconfirmed')
      end
    end
  end

  describe "GET #{employees_api_path 'jobs/:id'}" do
    let(:job) { create(:job) }
    let(:employee) { create(:employee) }
    let(:headers) { auth_headers(user: employee) }

    context 'when job is not accepted to employee' do
      it 'renders JSON rep of attributes_to_others' do
        get employees_api_path("jobs/#{job.id}"), headers: headers
        resp = json_body
        expect(resp).not_to be_nil
        expect(resp[:company_email]).to be_nil
        expect(resp[:phone]).to be_nil
      end
    end

    context 'when job is accepted to employee' do
      let(:offer) { create(:offer, :accepted, :from_employee, offerable_from: employee) }
      let(:job) { offer.job }

      it 'renders JSON rep of all attributes' do
        get employees_api_path("jobs/#{job.id}"), headers: headers
        resp = json_body
        expect(resp[:company_email]).to eql job.company_email
        expect(resp[:phone]).to eql job.phone
      end
    end
  end
end
