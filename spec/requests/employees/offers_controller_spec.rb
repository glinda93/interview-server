require 'rails_helper'

RSpec.describe Employees::OffersController, type: :request do
  let(:job) { create(:job) }
  let(:recruitment) { create(:recruitment) }
  let(:employee) { create(:employee) }
  let(:employer) { job.employer }
  let(:offer) { create(:offer, recruitment: recruitment, offerable_from: employee, offerable_to: employer) }
  let(:employer_offer) { create(:offer, :to_employee, offerable_to: employee) }
  let(:headers) { auth_headers(user: employee) }

  describe "GET #{employees_api_path 'offers'}" do
    it 'returns JSON rep of offers' do
      offer
      get employees_api_path('offers'), headers: headers
      resp = json_body
      expect(resp["rows"][0]["id"]).to eq offer.id
      expect(resp["rows"][0]["recruitment"]["id"]).to eq recruitment.id
      expect(resp["rows"][0]["offerable_from_id"]).to eq employee.id
      expect(resp["rows"][0]["offerable_to_id"]).to eq employer.id
    end

    it 'includes offers received' do
      employer_offer
      get employees_api_path('offers'), headers: headers
      resp = json_body
      expect(resp["rows"].pluck("offerable_from_id").include?(employer_offer.offerable_from.id)).to eq true
    end
  end

  describe "POST #{employees_api_path 'offers'}" do
    it 'creates an offer' do
      params = { offer: { offerable_to_id: employer.id, offerable_to_type: employer.class.name, recruitment_id: recruitment.id } }.to_json
      post employees_api_path('offers'), params: params, xhr: true, headers: headers
      expect(Offer.where(recruitment: recruitment, offerable_from: employee, offerable_to: employer).first).not_to be_nil
    end

    context 'when employer is nil' do
      it 'raises ApplicationError with employer_not_found message' do
        params = { offer: { offerable_to_id: -1, offerable_to_type: 'Employer', recruitment_id: recruitment.id } }.to_json
        expect { post employees_api_path('offers'), params: params, xhr: true, headers: headers }.to raise_error(ApplicationError, I18n.t('message.employee.employer_not_found'))
      end
    end

    context 'when job is nil' do
      it 'raises ApplicationError with job_not_found message' do
        employer.job.destroy

        params = { offer: { offerable_to_id: employer.id, offerable_to_type: employer.class.name, recruitment_id: recruitment.id } }.to_json
        expect { post employees_api_path('offers'), params: params, xhr: true, headers: headers }.to raise_error(ApplicationError, I18n.t('message.employee.job_not_found'))
      end
    end

    context 'when offer is duplicated' do
      it 'raises ApplicationError with offer_already_sent message' do
        params = { offer: { offerable_to_id: employer.id, offerable_to_type: employer.class.name, recruitment_id: recruitment.id } }.to_json
        post employees_api_path('offers'), params: params, xhr: true, headers: headers
        expect do
          post employees_api_path('offers'), params: params, xhr: true, headers: headers
        end.to raise_error ApplicationError, I18n.t('message.common.offer_already_sent')
      end
    end
  end

  describe "POST #{employees_api_path 'offers/:id/accept'}" do
    let(:offer) { create(:offer, recruitment: recruitment, offerable_from: employer, offerable_to: employee) }

    it 'accepts an offer' do
      post employees_api_path("offers/#{offer.id}/accept"), headers: headers
      expect(json_body[:success]).to eq true
      offer.reload
      expect(offer.responded?).to eq true
      expect(offer.response).to eq Offer::RESPONSES[:accepted]
    end

    context 'when offer does not belong to user' do
      let(:invalid_offer) { create(:offer) }

      it 'raises ApplicationError with offer_invalid message' do
        expect do
          post employees_api_path("offers/#{invalid_offer.id}/accept"), headers: headers
        end.to raise_error ApplicationError, I18n.t('message.common.offer_invalid')
      end
    end

    context 'when offer is already responded' do
      it 'raises ApplicationError with offer_already_responded message' do
        post employees_api_path("offers/#{offer.id}/accept"), headers: headers
        expect do
          post employees_api_path("offers/#{offer.id}/accept"), headers: headers
        end.to raise_error ApplicationError, I18n.t('message.common.offer_already_responded')
      end
    end
  end

  describe "POST #{employees_api_path 'offers/:id/decline'}" do
    let(:offer) { create(:offer, recruitment: recruitment, offerable_from: employer, offerable_to: employee) }

    it 'declines an offer' do
      post employees_api_path("offers/#{offer.id}/decline"), headers: headers
      expect(json_body[:success]).to eq true
      offer.reload
      expect(offer.responded?).to eq true
      expect(offer.response).to eq Offer::RESPONSES[:declined]
    end
  end

  describe "DELETE #{employees_api_path 'offers/:id'}" do
    before do
      offer
    end

    it 'destroys an offer' do
      expect do
        delete employees_api_path("offers/#{offer.id}"), headers: headers
      end.to change(Offer, :count).by(-1)
      expect(json_body[:success]).to eq true
    end

    context 'when offer is not found' do
      it 'returns false with message off_not_found' do
        delete employees_api_path("offers/-1"), headers: headers
        expect(json_body[:success]).to eq false
        expect(json_body[:message]).to eq I18n.t('message.common.offer_not_found')
      end
    end

    context 'when offer is not from the user' do
      let(:offer) { create(:offer) }

      it 'raises ApplicationError with offer_invalid message' do
        expect do
          delete employees_api_path("offers/#{offer.id}"), headers: headers
        end.to raise_error ApplicationError, I18n.t('message.common.offer_invalid')
      end
    end
  end
end
