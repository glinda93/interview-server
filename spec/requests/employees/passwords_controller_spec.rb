require 'rails_helper'

RSpec.describe Employees::PasswordsController, type: :request do
  before { clear_enqueued_jobs }

  let(:employee) { create(:employee) }

  describe "POST #{employees_api_path 'password'}" do
    let(:mail) { PasswordMailer.with(user: employee).password_instructions }

    context 'when email address is given' do
      let(:params) { { email_or_phone: employee.email } }

      it 'sends password reset instructions' do
        employee
        expect do
          post employees_api_path('password'), params: params
        end.to change(enqueued_jobs, :size).by 1
        expect(mail.to).to eq [employee.email]
        expect(mail.html_part.body.to_s).to match employee.otp
      end
    end

    context 'when phone number is given' do
      let(:employee) { create(:employee, phone: '81671607229') }
      let(:params) { { email_or_phone: employee.phone } }
      let(:mail) { PasswordTexter.password_instructions(employee) }

      it 'sends password reset instructions' do
        employee
        expect do
          post employees_api_path('password'), params: params
        end.to have_enqueued_job Textris::Delay::ActiveJob::Job
        expect(mail.to).to eq [employee.phone.delete('+')]
        expect(mail.content).to match employee.otp
      end
    end

    context 'when wrong email or phone is given' do
      let(:params) { { email_or_phone: 'wrong@email.com' } }

      it 'raises ApplicationError with no_such_registration message' do
        employee
        expect do
          post employees_api_path('password'), params: params
        end.to raise_error ApplicationError, I18n.t('message.employee.no_such_registration')
      end
    end
  end

  describe "POST #{employees_api_path('password/otp_verify')}" do
    let(:otp) { employee.generate_otp }
    let(:params) { { otp: otp, email_or_phone: employee.phone } }

    it 'returns reset_password_token' do
      post employees_api_path('password/otp_verify'), params: params
      employee.reload
      expect(json_body[:success]).to eq true
      expect(json_body[:reset_password_token]).to be_instance_of String
    end
  end

  describe "PUT #{employees_api_path('password')}" do
    let(:otp) { employee.generate_otp }
    let(:password) { Faker::Internet.password }

    it 'resets password' do
      params = { otp: otp, email_or_phone: employee.phone }
      post employees_api_path('password/otp_verify'), params: params
      reset_password_token = json_body[:reset_password_token]
      params = { employee: { reset_password_token: reset_password_token, password: password } }
      put employees_api_path('password'), params: params
      expect(json_body[:success]).to eq true
      post employees_api_path('sign_in'), params: { employee: { email: employee.email, password: password } }
      expect(json_body[:id]).to eq employee.id
      expect(response.headers['Authorization']).to be_instance_of String
    end
  end
end
