require 'rails_helper'

RSpec.describe Employees::ProfilesController, type: :request do
  describe "PUT #{employees_api_path 'profiles'}" do
    let(:employee) { create(:employee) }
    let(:headers) { auth_headers(user: employee) }
    let(:params) do
      {
        first_name: 'First Name',
        last_name: 'Last Name',
        first_name_kana: 'テキスト',
        last_name_kana: 'テキスト',
        phone: employee.phone,
        birthday: '2000-01-01',
        pref: employee.pref,
        city: employee.city,
        address: 'Address',
        profile_pref: employee.pref,
        profile_city: employee.city,
        profile: 'テキストテキスト',
        pn_enabled: false,
        gender_id: 1,
        skill_ids: {
          0 => 1
        }
      }
    end

    it 'updates profile' do
      put employees_api_path('profiles'), params: { profile: params }.to_json, headers: headers
      employee.reload
      expect(employee.first_name).to eq params[:first_name]
    end

    context 'when phone number is changed' do
      let(:phone) { '16462662535' }

      it 'sends reconfirmation sms' do
        params[:phone] = phone
        expect do
          put employees_api_path('profiles'), params: { profile: params }.to_json, headers: headers
        end.to have_enqueued_job Textris::Delay::ActiveJob::Job
        employee.reload
        expect(employee.unconfirmed_phone.delete('+')).to eq phone
      end
    end
  end
end
