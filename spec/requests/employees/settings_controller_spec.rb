require 'rails_helper'

RSpec.describe Employees::SettingsController, type: :request do
  let(:user) { create(:employee) }
  let(:headers) { auth_headers(user: user) }

  describe "GET #{employees_api_path('settings/intro')}" do
    before do
      create(:setting, key: :intro_employee) unless Setting.intro_employee
    end

    context 'when format is json' do
      it 'returns json' do
        get employees_api_path('settings/intro'), headers: headers
        expect(json_body[:value]).to match Setting.intro_employee
      end
    end

    context 'when format is html' do
      it 'returns html' do
        get employees_api_path('settings/intro.html'), headers: headers
        expect(response.body).to match Setting.intro_employee
      end
    end
  end
end
