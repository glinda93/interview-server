require 'rails_helper'

RSpec.describe Employees::WishesController, type: :request do
  let(:job) { create(:job) }
  let(:employee) { create(:employee) }
  let(:employer) { job.employer }
  let(:wish) { create(:wish, wishable_from: employee, wishable_to: job) }
  let(:headers) { auth_headers(user: employee) }

  describe("GET #{employees_api_path 'wishes'}") do
    it 'returns JSON rep of jobs in wishlist' do
      wish
      get employees_api_path('wishes'), headers: headers
      resp = json_body
      expect(resp["rows"][0]["id"]).to eq job.id
    end
  end

  describe("POST #{employees_api_path 'wishes'}") do
    it 'creates a wish' do
      params = { wish: { wishable_to_id: job.id, wishable_to_type: job.class.name } }.to_json
      post employees_api_path('wishes'), params: params, headers: headers
      expect(json_body[:success]).to eq true
      expect(Wish.where(wishable_from: employee, wishable_to: job).first).not_to be_nil
    end
  end

  describe("DELETE #{employees_api_path 'wishes'}") do
    it 'deletes a wish' do
      wish_id = wish.id
      delete employees_api_path('wishes'), params: { wishable_to_id: job.id, wishable_to_type: job.class.name }.to_json, headers: headers
      expect(json_body[:success]).to eq true
      expect { Wish.find wish_id }.to raise_error ActiveRecord::RecordNotFound
    end
  end
end
