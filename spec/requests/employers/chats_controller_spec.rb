require 'rails_helper'

RSpec.describe Employers::ChatsController, type: :request do
  let(:chat_room) { create(:chat_room) }
  let(:employer) do
    employer = chat_room.employers.first
    unless employer.subscribed?
      employer.subscription = create(:subscription, subscriber: employer)
      employer.save!
    end
    employer
  end
  let(:headers) { auth_headers(user: employer) }
  let(:params) do
    {
      chat_message: {
        chat_room_id: chat_room.id,
        image: Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/image.png'), 'image/png')
      }
    }
  end

  describe "POST #{employers_api_path 'chats/upload_image'}" do
    it 'creates an image chat message' do
      expect do
        post employers_api_path('chats/upload_image'), params: params, headers: headers
      end.to change { ChatMessage.all.count }.by 1
      chat_message = ChatMessage.where(authorable: employer).first
      expect(chat_message.image).not_to be_nil
      expect(chat_message.message).to match "[image]*[/image]"
    end
  end

  describe "GET #{employers_api_path 'chats'}" do
    let(:chat_message) do
      chat_room
      employer
      create(:chat_message, chat_room: chat_room, authorable: employer)
    end

    it 'renders JSON rep of chat rooms' do
      chat_message
      get employers_api_path('chats'), headers: headers
      expect(json_body).not_to be_nil
    end
  end

  describe "GET #{employers_api_path('chats/:id')}" do
    it 'renders JSON rep of a chat room' do
      get employers_api_path("chats/#{chat_room.id}"), headers: headers
      expect(json_body).not_to be_nil
    end
  end

  describe "GET #{employers_api_path('chats/message')}" do
    let(:chat_message) do
      chat_room
      employer
      create(:chat_message, chat_room: chat_room, authorable: employer)
    end

    it 'renders JSON rep of chat messages' do
      get employers_api_path("chats/#{chat_room.id}"), headers: headers
      expect(json_body).not_to be_nil
    end
  end

  describe "POST #{employers_api_path 'chats'}" do
    let(:job) { create(:job) }
    let(:employer) { job.employer }
    let(:employee) { create(:employee) }
    let(:params) { { chat_room: { chattable_id: employee.id, chattable_type: employee.class.name } }.to_json }
    let(:chat_room) { ChatRoom.where_participants([employee, employer]).first }

    it 'creates or updates chat room' do
      expect do
        post employers_api_path('chats'), headers: headers, params: params
      end.to change(ChatRoom, :count).by 1
      expect(chat_room.chat_room_infos.count).to eq 2

      expect do
        post employers_api_path('chats'), headers: headers, params: params
      end.to change(ChatRoom, :count).by 0
    end
  end
end
