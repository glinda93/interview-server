require 'rails_helper'

RSpec.describe Devise::ConfirmationsController, type: :request do
  before { clear_enqueued_jobs }

  describe "POST #{employers_api_path 'confirmation'}" do
    let(:employer) { create(:employer, :unconfirmed) }
    let(:params) { { employer: { email: employer.email } } }

    it 'sends a confirmation instruction' do
      employer
      expect do
        post employers_api_path('confirmation'), params: params
      end.to change { ActionMailer::Base.deliveries.length }.by 1
      employer.reload
      expect(employer.confirmation_token).not_to be_nil
    end
  end

  describe "GET #{employers_api_path 'confirmation'}" do
    let(:confirmation_token) { '123456' }
    let(:employer) { create(:employer, :unconfirmed, confirmation_token: confirmation_token) }
    let(:params) { { confirmation_token: confirmation_token, id: employer.id } }

    it 'confirms employer' do
      get employers_api_path('confirmation'), params: params, xhr: true
      employer.reload
      expect(employer.confirmed_at).not_to be_nil
    end

    context 'when invalid confirmation_token is given' do
      let(:params) { { confirmation_token: 'wrong', id: employer.id } }

      it 'does not confirm employer' do
        get employers_api_path('confirmation'), params: params, xhr: true
        expect(employer.confirmed_at).to be_nil
      end
    end
  end
end
