require 'rails_helper'

RSpec.describe Devise::RegistrationsController, type: :request do
  let(:email) { 'foo@bar.com' }
  let(:password) { 'password' }
  let(:params) { { employer: { email: email, password: password } } }

  describe "POST #{employers_api_path ''}" do
    let(:send_request) { post employers_api_path(''), params: params, xhr: true }

    it 'creates an employer' do
      expect do
        send_request
      end.to change { Employer.all.count }.by 1
    end

    it 'shows attributes for self' do
      send_request
      expect(json_body['confirmed']).not_to be_nil
    end

    it 'sends welcome email' do
      expect do
        send_request
      end.to change { ActionMailer::Base.deliveries.count }.by 1
    end
  end
end
