require 'rails_helper'

RSpec.describe Devise::SessionsController, type: :request do
  let(:password) { '123456' }
  let(:employer) { create(:employer, password: password, password_confirmation: password) }
  let(:params) { { employer: { email: employer.email, password: password } } }

  describe "POST #{employers_api_path 'sign_in'}" do
    it 'signs in an employer' do
      post employers_api_path('sign_in'), params: params, xhr: true
      expect(json_body[:id]).to eql employer.id
      expect(response.headers['Authorization']).to be_instance_of String
    end

    context 'when user is soft deleted' do
      before { employer.soft_destroy }

      it 'returns temporary token' do
        post employers_api_path('sign_in'), params: params, xhr: true
        expect(json_body[:token]).to be_instance_of String
      end

      it 'employer still remains soft destroyed' do
        post employers_api_path('sign_in'), params: params, xhr: true
        employer.reload
        expect(employer.soft_destroyed?).to eq true
      end
    end
  end

  describe "PUT #{employers_api_path 'resubscribe'}" do
    let(:employer) { create(:employer, :unsubscribed) }
    let(:stripe_helper) { StripeMock.create_test_helper }
    let(:card_token) { stripe_helper.generate_card_token }
    let(:product) { stripe_helper.create_product }
    let(:price) { stripe_helper.create_price({ product: product[:id] }) }
    let(:token) { '12345' }
    let(:params) { { token: token } }
    let(:stripe_customer) do
      Stripe::Customer.retrieve(employer.subscription.data['customer_id'])
    end
    let(:stripe_subscription) do
      Stripe::Subscription.retrieve(employer.subscription.data['subscription_id'])
    end
    let(:billing_cycle_anchor) { stripe_subscription.billing_cycle_anchor || stripe_subscription.trial_end }
    let(:backdate_start_date) { stripe_subscription.backdate_start_date }
    let(:trial_ends_at) { employer.subscription.trial_ends_at.to_i }

    before do
      StripeMock.start
      Rails.application.config.setting.stripe[:price_id] = price[:id]
      employer.create_subscription card_token
      employer.reload
      employer.soft_destroy
      Rails.cache.clear
      Rails.cache.write("auth/Employer/temporary_tokens/#{token}", employer)
    end

    after { StripeMock.stop }

    it 'resubscribes user' do
      put employers_api_path('resubscribe'), params: params, xhr: true
      expect(json_body[:id]).to eql employer.id
      expect(response.headers['Authorization']).to be_instance_of String
      employer.reload
      expect(employer.subscribed?).to eq true
      expect(employer.soft_destroyed?).to eq false
    end

    context 'when user trial periods not ended' do
      it 'billing cycle anchor should be equal to user trial periods' do
        put employers_api_path('resubscribe'), params: params, xhr: true
        expect(billing_cycle_anchor).to eq trial_ends_at
      end
    end

    context 'when user trial periods already ended' do
      let(:next_month_billing_cycle) { Time.now.utc.next_month.at_beginning_of_month.to_i }
      let(:this_month_billing_cycle) { Time.now.utc.at_beginning_of_month.to_i }

      context 'when the customer had not subscribed for current month' do
        it 'billing cycle anchor should be the first date of next month' do
          employer.subscription.suspended_at = 1.month.ago.utc.at_beginning_of_month
          employer.subscription.trial_ends_at = 2.months.ago.utc.at_beginning_of_month
          employer.subscription.save!
          put employers_api_path('resubscribe'), params: params, xhr: true
          employer.subscription.reload
          expect(billing_cycle_anchor).to eq next_month_billing_cycle
          expect(backdate_start_date).to eq this_month_billing_cycle
        end
      end
    end
  end
end
