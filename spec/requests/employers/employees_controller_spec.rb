require 'rails_helper'

RSpec.describe Employers::EmployeesController, type: :request do
  let(:job) { create(:job) }
  let(:employer) { job.employer }
  let(:headers) { auth_headers(user: employer) }
  let(:employee) { create(:employee, profile_pref: employer.pref, profile_city: employer.city) }

  describe "GET #{employers_api_path('employees')}" do
    it 'renders JSON rep of employee search result' do
      employee
      employer
      get employers_api_path('employees'), headers: headers
      resp = json_body
      expect(resp["rows"].pluck("id").include?(employee.id)).to eq true
      expect(resp[:rows][0][:wished]).to be_in([true, false])
    end

    context 'when job is missing' do
      it 'raises ApplicationError with job_not_found message' do
        employer.job.destroy
        expect { get employers_api_path('employees'), headers: headers }.to raise_error ApplicationError, I18n.t('message.employer.job_not_found')
      end
    end

    context 'when employer is unconfirmed' do
      let(:employer) do
        Employer.skip_callback(:create, :after, :send_welcome_email)
        create(:employer, :unconfirmed)
      end

      it 'raises UnconfirmedError' do
        expect do
          get employers_api_path('employees'), headers: headers
        end.to raise_error UnconfirmedError
      end
    end

    context 'when employer is unsubscribed' do
      let(:employer) { create(:employer, :unsubscribed) }

      it 'responds with unauthorized status' do
        expect do
          get employers_api_path('employees'), headers: headers
        end.to raise_error ApplicationError, I18n.t('message.employer.not_subscribed')
      end
    end
  end

  describe "GET #{employers_api_path('employees/:id')}" do
    context 'when job is not accepted by employee' do
      it 'renders JSON rep of attributes_for_employers' do
        get employers_api_path("employees/#{employee.id}"), headers: headers
        expect(json_body).not_to be_nil
        expect(json_body[:email]).to be_nil
        expect(json_body[:phone]).to be_nil
        expect(json_body[:address]).to be_nil
        expect(json_body[:wished]).to be_in([true, false])
      end
    end

    context 'when job is accepted by employee' do
      let(:offer) { create(:offer, :accepted, :from_employee) }
      let(:employer) { offer.offerable_to }
      let(:employee) { offer.offerable_from }

      it 'renders JSON rep of attributes_for_accepted' do
        get employers_api_path("employees/#{employee.id}"), headers: headers
        expect(json_body).not_to be_nil
        expect(json_body[:email]).to eql employee.email
        expect(json_body[:phone]).to eql employee.phone
        expect(json_body[:address]).to eql employee.address
        expect(json_body[:wished]).to be_in([true, false])
      end
    end
  end

  describe "GET #{employers_api_path 'employees/:id/reviews'}" do
    let(:employee) { create(:employee) }
    let(:employer) { create(:employer, :with_job) }
    let(:offer) { create(:offer, offerable_from: employee, offerable_to: employer) }
    let(:review) { create(:review, reviewable: offer, reviewable_from: employer, reviewable_to: employee) }

    before do
      review
    end

    it 'renders JSON rep of employee reviews' do
      get employers_api_path("employees/#{employee.id}/reviews"), headers: headers
      expect(json_body[:rows][0][:id]).to eq review.id
    end
  end
end
