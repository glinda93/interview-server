require 'rails_helper'

RSpec.describe Employers::EmployersController, type: :request do
  let(:employer) { create(:employer) }
  let(:headers) { auth_headers(user: employer) }

  describe "GET #{employers_api_path('me')}" do
    it 'renders JSON rep of current user' do
      get employers_api_path("me"), headers: headers
      resp = json_body
      expect(resp).not_to be_nil
      expect(resp["id"]).to eq employer.id
    end

    context 'when missing or invalid token' do
      it 'responds with unauthorized status' do
        get employers_api_path("me")
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end

  describe "DELETE #{employers_api_path 'delete'}" do
    it 'softly destroys current user' do
      delete employers_api_path('delete'), headers: headers
      expect(json_body[:success]).to eq true
      employer.reload
      expect(employer.deleted_at).not_to be_nil
    end
  end
end
