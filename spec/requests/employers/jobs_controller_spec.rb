require 'rails_helper'

RSpec.describe Employers::JobsController, type: :request do
  let(:employer) { create(:employer) }
  let(:headers) { auth_headers(user: employer, headers: { 'Accept' => 'application/json' }) }

  describe "GET #{employers_api_path 'jobs'}" do
    context 'when job is not created' do
      it 'renders nil' do
        get employers_api_path('jobs'), headers: headers
      end
    end

    context 'when job is created' do
      let(:employer) { create(:employer, :with_job) }
      let(:job) { employer.job }

      it 'renders JSON rep of employer job' do
        get employers_api_path('jobs'), headers: headers
        expect(json_body[:id]).to eq job.id
      end
    end
  end

  describe "PUT #{employers_api_path('jobs')}" do
    let(:job_params) do
      {
        company_name: 'Test Company',
        pref: 0,
        city: 0,
        address: 'Address',
        phone: '1234567890',
        company_email: 'test@test.com',
        price_type_id: 1,
        payment_method_id: 1,
        price: 1217,
        desc_main: 'Desc Main',
        desc_skill: 'Desc Skill',
        desc_extra: 'Desc Extra',
        desc_payment: 'Desc Payment',
        incl_commuting_fee: false,
        child_allowed: false,
        work_hour_from: "08:00",
        work_hour_to: "10:00",
        skill_ids: {
          0 => 1,
          1 => 2
        },
        images: [
          {
            rank: 1,
            data: Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/image.png'), 'image/png')
          }
        ],
        recruitments: [
          {
            number: 5,
            recruit_at: Time.now.utc,
            status: Recruitment::STATUSES[:active]
          }
        ],
        status: Job::STATUSES[:active]
      }
    end

    it 'creates or updates job' do
      # create
      put employers_api_path('jobs').to_s, params: { job: job_params }, headers: headers
      # update
      put employers_api_path('jobs').to_s, params: { job: job_params }, headers: headers
      employer.reload
      job = employer.job
      expect(job).not_to be_nil
      expect(job.company_name).to eq job_params[:company_name]
      expect(job.job_skills.count).to eq 2
      expect(job.images.count).to eq 1
    end

    context 'when work_hour_from is behind work_hour_to' do
      let(:invalid_job_params) do
        job_params[:work_hour_from] = "12:00"
        job_params[:work_hour_to] = "11:00"
        job_params
      end

      it 'raises RecordInvalid error' do
        expect do
          put employers_api_path('jobs').to_s, params: { job: invalid_job_params }, headers: headers
        end.to raise_error ActiveRecord::RecordInvalid
      end
    end
  end

  describe "PUT #{employers_api_path 'jobs/update_images'}" do
    let(:job) { create(:job, images_count: 3) }
    let(:employer) { job.employer }
    let(:params) do
      {
        job: {
          images: [
            {
              id: job.images[0].id,
              rank: 1
            },
            {
              rank: 2,
              data: Rack::Test::UploadedFile.new(Rails.root.join('spec/fixtures/image.png'), 'image/png')
            }
          ]
        }
      }
    end

    it 'updates job images' do
      put employers_api_path('jobs/update_images'), headers: headers, params: params
      job.reload
      expect(job.images.count).to eq 2
    end
  end

  describe "PUT #{employers_api_path('jobs/update_recruitments')}" do
    let(:employer) { create(:employer, :with_job) }
    let(:recruitment_params) do
      {
        status: Job::STATUSES[:inactive],
        recruitments: [{ number: 1, recruit_at: Time.zone.now.strftime("%Y-%m-%d"), status: Recruitment::STATUSES[:active] }]
      }
    end

    it 'updates job status and recruitments' do
      expect do
        put employers_api_path('jobs/update_recruitments'), params: recruitment_params, headers: headers
      end.to change { employer.job.recruitments.count }.by 1
      employer.job.reload
      expect(employer.job.status).to eq recruitment_params[:status]
    end
  end
end
