require 'rails_helper'

RSpec.describe Employers::OffersController, type: :request do
  let(:job) { create(:job) }
  let(:recruitment) { job.recruitments.first }
  let(:employee) { create(:employee) }
  let(:employer) { job.employer }
  let(:offer) { create(:offer, recruitment: recruitment, offerable_from: employer, offerable_to: employee) }
  let(:employee_offer) { create(:offer, :to_employer, recruitment: recruitment) }
  let(:headers) { auth_headers(user: employer) }

  describe "GET #{employers_api_path 'offers'}" do
    it 'returns JSON rep of offers' do
      offer
      get employers_api_path('offers'), headers: headers
      resp = json_body
      expect(resp["rows"][0]["id"]).to eq offer.id
      expect(resp["rows"][0]["recruitment"]["id"]).to eq recruitment.id
      expect(resp["rows"][0]["offerable_from_id"]).to eq employer.id
      expect(resp["rows"][0]["offerable_to_id"]).to eq employee.id
    end

    it 'includes offers received' do
      employee_offer
      get employers_api_path('offers'), headers: headers
      resp = json_body
      expect(resp["rows"].pluck("offerable_from_id").include?(employee_offer.offerable_from.id)).to eq true
    end
  end

  describe "GET #{employers_api_path('offers/:id')}" do
    it 'returns JSON rep of offer' do
      offer
      get employers_api_path("offers/#{offer.id}"), headers: headers
      resp = json_body
      expect(resp[:id]).to eq offer.id
    end
  end

  describe "POST #{employers_api_path 'offers'}" do
    let(:params) { { offer: { offerable_to_id: employee.id, offerable_to_type: employee.class.name, recruitment_id: recruitment.id } }.to_json }

    it 'creates an offer' do
      post employers_api_path('offers'), params: params, headers: headers
      expect(Offer.where(recruitment: recruitment, offerable_from: employer, offerable_to: employee).first).not_to be_nil
    end

    context 'when offer is already sent' do
      it 'raises ApplicationError with offer_already_sent message' do
        post employers_api_path('offers'), params: params, headers: headers
        expect do
          post employers_api_path('offers'), params: params, headers: headers
        end.to raise_error ApplicationError, I18n.t('message.common.offer_already_sent')
      end
    end

    context 'when offer is already received' do
      before do
        recruitment.recruit_at = 1.day.ago
        recruitment.save
        offer.respond(Offer::RESPONSES[:accepted])
      end

      it 'return false with review missing offer' do
        post employers_api_path('offers'), params: params, headers: headers

        resp = json_body
        expect(resp[:success]).to eq false
        expect(resp[:message]).to eq I18n.t('message.common.offer_not_allowed_before_review')
        expect(resp[:code]).to eq 'offer_not_allowed_before_review'
        expect(resp[:data]).to eq offer.id
      end
    end
  end

  describe "POST #{employers_api_path 'offers/:id/accept'}" do
    let(:offer) { create(:offer, recruitment: recruitment, offerable_from: employee, offerable_to: employer) }

    it 'accepts an offer' do
      post employers_api_path("offers/#{offer.id}/accept"), headers: headers
      expect(json_body[:success]).to eq true
      offer.reload
      expect(offer.responded?).to eq true
      expect(offer.response).to eq Offer::RESPONSES[:accepted]
    end

    context 'when offer does not belong to user' do
      let(:invalid_offer) { create(:offer) }

      it 'raises ApplicationError with offer_invalid message' do
        expect do
          post employers_api_path("offers/#{invalid_offer.id}/accept"), headers: headers
        end.to raise_error ApplicationError, I18n.t('message.common.offer_invalid')
      end
    end

    context 'when offer is already responded' do
      it 'raises ApplicationError with offer_already_responded message' do
        post employers_api_path("offers/#{offer.id}/accept"), headers: headers
        expect do
          post employers_api_path("offers/#{offer.id}/accept"), headers: headers
        end.to raise_error ApplicationError, I18n.t('message.common.offer_already_responded')
      end
    end
  end

  describe "POST #{employers_api_path 'offers/:id/decline'}" do
    let(:offer) { create(:offer, recruitment: recruitment, offerable_from: employee, offerable_to: employer) }

    it 'declines an offer' do
      post employers_api_path("offers/#{offer.id}/decline"), headers: headers
      expect(json_body[:success]).to eq true
      offer.reload
      expect(offer.responded?).to eq true
      expect(offer.response).to eq Offer::RESPONSES[:declined]
    end
  end

  describe "DELETE #{employers_api_path 'offers/:id'}" do
    before do
      offer
    end

    it 'destroys an offer' do
      expect do
        delete employers_api_path("offers/#{offer.id}"), headers: headers
      end.to change(Offer, :count).by(-1)
      expect(json_body[:success]).to eq true
    end

    context 'when offer is not found' do
      it 'returns false with message off_not_found' do
        delete employers_api_path("offers/-1"), headers: headers
        expect(json_body[:success]).to eq false
        expect(json_body[:message]).to eq I18n.t('message.common.offer_not_found')
      end
    end

    context 'when offer is not from the user' do
      let(:offer) { create(:offer) }

      it 'raises ApplicationError with offer_invalid message' do
        expect do
          delete employers_api_path("offers/#{offer.id}"), headers: headers
        end.to raise_error ApplicationError, I18n.t('message.common.offer_invalid')
      end
    end
  end
end
