require 'rails_helper'

RSpec.describe Employers::PasswordsController, type: :request do
  let(:employer) { create(:employer) }

  describe "POST #{employers_api_path 'password'}" do
    let(:mail) { PasswordMailer.with(user: employer).password_instructions }

    context 'when email address is given' do
      let(:params) { { email: employer.email } }

      it 'sends password reset instruction' do
        employer
        expect do
          post employers_api_path('password'), params: params
        end.to change { ActionMailer::Base.deliveries.count }.by 1
        expect(mail.html_part.body.decoded).to match employer.otp
      end
    end
  end

  describe "POST #{employers_api_path 'password/otp_verify'}" do
    let(:otp) { employer.generate_otp }
    let(:params) { { otp: otp, email: employer.email } }

    it 'returns reset_password_token' do
      post employers_api_path('password/otp_verify'), params: params
      employer.reload
      expect(json_body[:success]).to eq true
      expect(json_body[:reset_password_token]).to be_instance_of String
    end
  end

  describe "PUT #{employers_api_path 'password'}" do
    let(:otp) { employer.generate_otp }
    let(:password) { Faker::Internet.password }
    let(:email) { employer.email }

    it 'resets password' do
      params = { otp: otp, email: email }
      post employers_api_path('password/otp_verify'), params: params
      reset_password_token = json_body[:reset_password_token]
      params = { employer: { reset_password_token: reset_password_token, password: password } }
      put employers_api_path('password'), params: params
      expect(json_body[:success]).to eq true
      post employers_api_path('sign_in'), params: { employer: { email: employer.email, password: password } }
      expect(json_body[:id]).to eq employer.id
      expect(response.headers['Authorization']).to be_instance_of String
    end
  end
end
