require 'rails_helper'

RSpec.describe Employers::ProfilesController, type: :request do
  describe "PUT #{employers_api_path 'profiles'}" do
    let(:employer) { create(:employer) }
    let(:headers) { auth_headers(user: employer) }
    let(:params) do
      {
        company_name: 'Test Company',
        pref: 0,
        city: 0,
        address: 'Address',
        phone: '1234567890',
        company_email: 'email@email.com',
        desc_manager: 'Manager Description'
      }
    end

    it 'updates employer info' do
      put employers_api_path('profiles'), params: { profile: params }.to_json, headers: headers
      employer.reload
      expect(employer.desc_manager).to eq params[:desc_manager]
    end
  end
end
