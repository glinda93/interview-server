require 'rails_helper'

RSpec.describe Employers::ReviewsController, type: :request do
  let(:job) { create(:job) }
  let(:employee) { create(:employee) }
  let(:employer) { job.employer }
  let(:offer) { create(:offer, :accepted, recruitment: job.recruitments.first, offerable_from: employee, offerable_to: employer) }
  let(:review) { create(:review, reviewable: offer, reviewable_to: employer, reviewable_from: employee) }
  let(:headers) { auth_headers(user: employer) }

  describe "GET #{employers_api_path 'reviews'}" do
    it 'returns JSON rep of reviews received' do
      review
      get employers_api_path('reviews'), headers: headers
      resp = json_body
      expect(resp['rows'].pluck('id')).to include review.id
      expect(resp['rows'].pluck('gender_id')).to include review.employee.gender_id
    end
  end

  describe "POST #{employers_api_path 'reviews'}" do
    let(:params) do
      {
        review: {
          reviewable_id: offer.id,
          reviewable_type: offer.class.name,
          reviewable_from_id: employer.id,
          reviewable_from_type: employer.class.name,
          reviewable_to_id: employee.id,
          reviewable_to_type: employee.class.name,
          rating: 5,
          comment: 'Test'
        }
      }.to_json
    end

    it 'creates or updates a review' do
      offer
      expect do
        post employers_api_path('reviews'), headers: headers, params: params
      end.to change { Review.all.count }.from(0).to(1)
    end

    it 'updates offer visible' do
      offer
      expect(offer.visible_to_offerable_to).to eq true
      expect(offer.visible_to_offerable_from).to eq true
      post employers_api_path('reviews'), headers: headers, params: params
      offer.reload
      expect(offer.visible_to_offerable_to).to eq false
      expect(offer.visible_to_offerable_from).to eq true
    end

    context 'when reviewable is invalid' do
      it 'raises ApplicationError with offer_invalid message' do
        offer.responded_at = nil
        offer.response = nil
        offer.save!
        expect do
          post employers_api_path('reviews'), headers: headers, params: params
        end.to raise_error ApplicationError, I18n.t('message.common.offer_invalid')
      end
    end
  end
end
