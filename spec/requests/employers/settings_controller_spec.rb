require 'rails_helper'

RSpec.describe Employers::SettingsController, type: :request do
  let(:user) { create(:employer) }
  let(:headers) { auth_headers(user: user) }

  describe "GET #{employers_api_path('settings/intro')}" do
    before do
      create(:setting, key: :intro_employer) unless Setting.intro_employer
    end

    context 'when format is json' do
      it 'returns json' do
        get employers_api_path('settings/intro'), headers: headers
        expect(json_body[:value]).to match Setting.intro_employer
      end
    end

    context 'when format is html' do
      it 'returns html' do
        get employers_api_path('settings/intro.html'), headers: headers
        expect(response.body).to match Setting.intro_employer
      end
    end
  end
end
