require 'rails_helper'
require 'stripe_mock'

RSpec.describe Employers::SubscriptionsController, type: :request do
  before do
    StripeMock.start
    Rails.application.config.setting.stripe[:price_id] = price[:id]
  end

  after { StripeMock.stop }

  let(:stripe_helper) { StripeMock.create_test_helper }
  let(:product) { stripe_helper.create_product }
  let(:price) { stripe_helper.create_price({ product: product[:id] }) }
  let(:employer) { create(:employer, :unsubscribed) }
  let(:headers) { auth_headers(user: employer) }
  let(:params) do
    {
      subscription: {
        card: {
          number: '4242424242424242',
          exp_month: '1',
          exp_year: (Time.now.strftime("%Y").to_i + 1).to_s,
          cvc: '123'
        }
      }
    }.to_json
  end

  describe "GET #{employers_api_path 'subscriptions'}" do
    let(:card_token) { stripe_helper.generate_card_token }

    before do
      employer.create_subscription card_token
      employer.reload
      get employers_api_path('subscriptions'), headers: headers
    end

    it 'renders JSON rep of employer subscription' do
      expect(json_body['subscription']).not_to be_nil
      expect(json_body['stripe_customer']).not_to be_nil
    end

    context 'when employer is not subscribed' do
      before do
        employer.subscription.destroy
        employer.reload
        get employers_api_path('subscriptions'), headers: headers
      end

      it 'subscription and stripe_customer should be nil' do
        get employers_api_path('subscriptions'), headers: headers
        expect(json_body['subscription']).to be_nil
        expect(json_body['stripe_customer']).to be_nil
      end
    end

    context 'when employer has suspended subscription' do
      before do
        employer.suspend_subscription
        employer.reload
        get employers_api_path('subscriptions'), headers: headers
      end

      it 'renders JSON' do
        expect(json_body).not_to be_nil
      end
    end
  end

  describe "POST #{employers_api_path 'subscriptions'}" do
    it 'creates a subscription' do
      expect do
        post employers_api_path('subscriptions'), headers: headers, params: params
      end.to change { Subscription.all.count }.by 1
      employer.reload
      expect(employer.subscribed?).to eq true
    end

    context 'when employer is already subscribed' do
      let(:employer) { create(:employer) }

      it 'raises ApplicationError with already_subscribed message' do
        post employers_api_path('subscriptions'), headers: headers, params: params
        expect(json_body[:success]).to eq false
        expect(json_body[:message]).to eq I18n.t('message.employer.already_subscribed')
      end
    end

    context 'when card token is given' do
      let(:card_token) do
        stripe_helper.generate_card_token
      end

      let(:params) do
        {
          subscription: {
            token: {
              id: card_token['id']
            }
          }
        }.to_json
      end

      it 'creates a subscription' do
        expect do
          post employers_api_path('subscriptions'), headers: headers, params: params
        end.to change { Subscription.all.count }.by 1
        employer.reload
        expect(employer.subscribed?).to eq true
      end
    end
  end
end
