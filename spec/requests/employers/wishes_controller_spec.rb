require 'rails_helper'

RSpec.describe Employers::WishesController, type: :request do
  let(:job) { create(:job) }
  let(:employee) { create(:employee) }
  let(:employer) { job.employer }
  let(:wish) { create(:wish, wishable_from: employer, wishable_to: employee) }
  let(:headers) { auth_headers(user: employer) }

  describe("GET #{employers_api_path 'wishes'}") do
    it 'returns JSON rep of employees in wishlist' do
      wish
      get employers_api_path('wishes'), headers: headers
      resp = json_body
      expect(resp["rows"][0]["id"]).to eq employee.id
    end
  end

  describe("POST #{employers_api_path 'wishes'}") do
    it 'creates a wish' do
      params = { wish: { wishable_to_id: employee.id, wishable_to_type: employee.class.name } }.to_json
      post employers_api_path('wishes'), params: params, headers: headers
      expect(json_body[:success]).to eq true
      expect(Wish.where(wishable_from: employer, wishable_to: employee).first).not_to be_nil
    end
  end

  describe("DELETE #{employers_api_path 'wishes'}") do
    it 'deletes a wish' do
      wish_id = wish.id
      delete employers_api_path('wishes'), params: { wishable_to_id: employee.id, wishable_to_type: employee.class.name }.to_json, headers: headers
      expect(json_body[:success]).to eq true
      expect { Wish.find wish_id }.to raise_error ActiveRecord::RecordNotFound
    end
  end
end
