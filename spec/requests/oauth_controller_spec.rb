require 'rails_helper'

RSpec.describe OauthController, type: :request do
  let(:user) { create(:employee) }
  let(:provider) { Oauth::Twitter }
  let(:social_user) { { id: user.social_auth.uid, email: user.email, provider: provider.class.name.underscore } }

  before do
    allow(provider).to receive(:get_user!).and_return(social_user)
  end

  describe "POST #{api_path('oauth/twitter')}" do
    let(:params) { { auth_token: 'auth_token', auth_token_secret: 'auth_token_secret', scope: 'Employee' } }

    it 'signs in by twitter' do
      post api_path('oauth/twitter'), params: params, xhr: true
      expect(json_body[:id]).to eql user.id
      expect(response.headers['Authorization']).to be_instance_of String
    end

    context 'when social_auth is empty' do
      let(:user) { create(:employee, with_social_auth: false) }
      let(:social_user) { { id: 71_213_991, email: user.email, provider: provider.class.name.underscore } }

      it 'creates a social_auth' do
        post api_path('oauth/twitter'), params: params, xhr: true
        expect(json_body[:id]).to eql user.id
        user.reload
        expect(user.social_auth).not_to be_nil
      end
    end

    context 'when social_user email is empty' do
      let(:user) { create(:employee, with_social_auth: false) }
      let(:social_user) { { id: 71_213_991, email: '', provider: provider.class.name.underscore } }

      context 'when device param is given' do
        let(:device) { user.devices.first }
        let(:params) do
          {
            auth_token: 'auth_token',
            auth_token_secret: 'auth_token_secret',
            scope: 'Employee',
            device: {
              uuid: device.uuid,
              model: device.model,
              platform: device.platform,
              environment: device.environment
            }
          }
        end

        it 'finds user by device' do
          post api_path('oauth/twitter'), params: params, xhr: true
          expect(json_body[:id]).to eql user.id
          user.reload
          expect(user.social_auth).not_to be_nil
        end
      end

      context 'when device is not given' do
        it 'raises ApplicationError with email_empty error message' do
          expect do
            post api_path('oauth/twitter'), params: params, xhr: true
          end.to raise_error Oauth::EmailEmptyError, I18n.t('message.common.social_auth_email_empty', provider: social_user[:provider])
        end
      end
    end

    context 'when social user is soft destroyed' do
      context 'when user is Employee' do
        before do
          user.soft_destroy
        end

        it 'restores user' do
          post api_path('oauth/twitter'), params: params, xhr: true
          user.reload
          expect(user.soft_destroyed?).to eq false
        end
      end

      context 'when user is Employer and user has cancelled subscrition' do
        let(:user) { create(:employer, :unsubscribed) }
        let(:params) { { auth_token: 'auth_token', auth_token_secret: 'auth_token_secret', scope: 'Employer' } }
        let(:stripe_helper) { StripeMock.create_test_helper }
        let(:card_token) { stripe_helper.generate_card_token }
        let(:product) { stripe_helper.create_product }
        let(:price) { stripe_helper.create_price({ product: product[:id] }) }

        before do
          StripeMock.start
          Rails.application.config.setting.stripe[:price_id] = price[:id]
          user.create_subscription card_token
          user.reload
          user.soft_destroy
          user.reload
        end

        it 'responds with temporary token' do
          post api_path('oauth/twitter'), params: params, xhr: true
          expect(json_body[:token]).to be_instance_of String
        end
      end
    end
  end
end
