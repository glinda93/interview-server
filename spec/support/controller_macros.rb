require 'devise/jwt/test_helpers'

module ControllerMacros
  def json_body
    parsed = JSON.parse(response.body)
    return parsed.with_indifferent_access if parsed.respond_to?(:with_indifferent_access)

    parsed
  end

  def auth_headers(user:, headers: { 'Accept' => 'application/json', 'Content-Type' => 'application/json' })
    Devise::JWT::TestHelpers.auth_headers(headers, user)
  end

  def api_path(path, namespace: nil)
    url = "/api/v#{ENV['API_VERSION']}"
    url = "#{url}/#{namespace.to_s.pluralize}" if namespace.present?
    "#{url}/#{path}"
  end

  def employees_api_path(path)
    api_path(path, namespace: :employee)
  end

  def employers_api_path(path)
    api_path(path, namespace: :employer)
  end

  def admins_api_path(path)
    api_path(path, namespace: :admin)
  end

  def commons_api_path(path)
    api_path(path, namespace: :common)
  end
end
