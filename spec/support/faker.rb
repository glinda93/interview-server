RSpec.configure do |config|
  config.before(:all) do
    Faker::Config.random = Random.new(config.seed)
    Faker::Config.locale = 'ja'
  end
end
