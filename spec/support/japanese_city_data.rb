module MockUp
  module JapaneseCityData
    def self.included(base)
      base.instance_eval do
        def fetch_japanese_city_data
          Rails.cache.fetch('japanese_city_data') do
            json = File.read Rails.root.join("spec/fixtures/japanese-city-data.json")
            JSON.parse(json, symbolize_names: true)
          end
        end
      end
    end
  end
end

RSpec.configure do
  JapaneseCityData.include MockUp::JapaneseCityData
end
