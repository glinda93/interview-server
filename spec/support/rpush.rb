require 'rpush'

module RpushMacro
  def prepare_test_app
    PushNotification::Service.create_android_app
    PushNotification::Service.create_ios_apps
  end
end

RSpec.configure do |config|
  config.before(:all) do
    config.include RpushMacro
    config.extend RpushMacro
    prepare_test_app
  end
  config.before do
    allow(Rpush).to receive(:push)
  end
end
