RSpec.configure do |config|
  config.around(:each, use_transactional_fixtures: false) do |example|
    self.use_transactional_tests = false
    example.run
    self.use_transactional_tests = true

    DatabaseCleaner.clean_with(:truncation, except: %w[skills genders price_types payment_methods])
  end
end
