require 'rails_helper'

RSpec.describe DestroyObsoleteNotificationsWorker, type: :worker do
  let(:worker) { subject }
  let(:user) { create(:employee) }

  before do
    PushNotification::Service.create(user: user, notification: { body: 'test' })
    notification = Rpush::Notification.first
    notification.created_at = 24.hours.ago
    notification.save!
  end

  describe 'perform' do
    it 'deletes notifications older than 24 hours' do
      expect do
        worker.perform
      end.to change { Rpush::Notification.count }.by(-1)
    end
  end
end
