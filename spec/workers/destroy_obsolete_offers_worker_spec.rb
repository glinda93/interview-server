require 'rails_helper'
RSpec.describe DestroyObsoleteOffersWorker, type: :worker do
  let(:worker) { subject }
  let(:expired_offer) { create(:offer, recruitment: create(:recruitment, recruit_at: 24.hours.ago.utc)) }
  let(:expired_but_accepted_offer) { create(:offer, :accepted, created_at: 24.hours.ago.utc) }
  let(:new_offer) { create(:offer) }

  describe "perform" do
    it "destroys obsolete offers" do
      expired_offer_id = expired_offer.id
      expired_but_accepted_offer_id = expired_but_accepted_offer.id
      new_offer_id = new_offer.id
      worker.perform
      expect(Offer.find_by(id: expired_offer_id)).to be_nil
      expect(Offer.find_by(id: expired_but_accepted_offer_id)).not_to be_nil
      expect(Offer.find_by(id: new_offer_id)).not_to be_nil
    end
  end
end
