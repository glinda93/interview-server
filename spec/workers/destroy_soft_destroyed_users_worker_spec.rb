require 'rails_helper'
RSpec.describe DestroySoftDestroyedUsersWorker, type: :worker do
  let(:worker) { subject }
  let(:expiry) { (Rails.application.config.setting[:soft_destroy_expires_in].to_i + 1).days.ago.utc }
  let(:expired_employee) { create(:employee, :soft_destroyed, deleted_at: expiry) }
  let(:expired_employer) { create(:employer, :soft_destroyed, deleted_at: expiry) }
  let(:unexpired_employee) { create(:employee) }
  let(:unexpired_employer) { create(:employer, :soft_destroyed) }

  before do
    expired_employee
    expired_employer
    unexpired_employee
    unexpired_employer
  end

  describe 'perform' do
    it 'destroys soft destroyed users' do
      expect(Employee.soft_destroy_expired.count).to eq 1
      expect(Employer.soft_destroy_expired.count).to eq 1
      expect do
        worker.perform
      end.to change { Employer.count + Employee.count }.by(-2)
    end
  end
end
