require 'rails_helper'
RSpec.describe NotifyReviewRemindsWorker, type: :worker do
  let(:worker) { subject }
  let(:offer) { create(:offer, :accepted, responded_at: 24.hours.ago) }
  let(:offer_not_to_remind) { create(:offer) }
  let(:review) { create(:review, reviewable: offer, reviewable_from: offer.offerable_from, reviewable_to: offer.offerable_to) }

  describe 'perform' do
    before do
      review
      offer_not_to_remind
    end

    it 'sends push notifications to users who have not left a review' do
      expect do
        worker.perform
      end.to change { Rpush::Notification.count }.by offer.offerable_to.devices.count
    end
  end
end
