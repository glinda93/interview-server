require 'rails_helper'

RSpec.describe NotifyUnreadMessagesWorker, type: :worker do
  include ActiveJob::TestHelper
  let(:worker) { subject }

  around { |example| perform_enqueued_jobs(&example) }

  before do
    create(:chat_message)
  end

  describe 'perform' do
    it 'destroys all unread messages' do
      expect do
        worker.perform
      end.to change(ChatUnreadMessage, :count).from(1).to(0)
    end
  end
end
