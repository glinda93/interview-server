require 'rails_helper'

RSpec.describe RecalculateStatsWorker, type: :worker do
  let(:worker) { subject }

  describe 'perform' do
    it 'works' do
      expect(worker.perform).to be_a Hash
    end
  end
end
